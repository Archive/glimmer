/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  test-control.c
 *
 *  Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <libbonobo.h>
#include <libbonoboui.h>
#include <libgnome/libgnome.h>
#include <libgnomeui/libgnomeui.h>
#include <libgnomevfs/gnome-vfs-init.h>
#include <libgnomevfs/gnome-vfs-mime-utils.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#include "bonobo-stream-vfs.h"

static gboolean use_stream = FALSE;

static void
open_file (GtkWidget *window, const gchar *filename)
{
	GtkWidget *widget;
	Bonobo_Unknown object;
	Bonobo_PersistStream persist_stream;
	Bonobo_PersistFile persist_file;
	gchar *uri, *mime_type;

	widget = g_object_get_data (G_OBJECT (window), "control");
	object = bonobo_widget_get_objref (BONOBO_WIDGET (widget));
	if (!object) {
		g_warning ("Object reference for glimmer control is NULL");
		return;
	}

	if (use_stream) {
		CORBA_Environment ev;
		BonoboStreamVfs *stream;

		persist_stream = Bonobo_Unknown_queryInterface (object, "IDL:Bonobo/PersistStream:1.0", NULL);
		if (!persist_stream) {
			g_warning ("PersistStream reference for glimmer control is NULL");
			return;
		}

		if (g_path_is_absolute (filename)) {
			uri = gnome_vfs_get_uri_from_local_path (filename);
		} else {
			gchar *curdir, *path;

			curdir = g_get_current_dir ();
			path = g_strconcat (curdir, "/", filename, NULL);
			g_free (curdir);
			uri = gnome_vfs_get_uri_from_local_path (path);
			g_free (path);
		}

		mime_type = gnome_vfs_get_mime_type (uri);
		g_free (uri);

		CORBA_exception_init (&ev);

		stream = bonobo_stream_vfs_open (filename,
						 Bonobo_Storage_READ,
		 				 &ev);
		if (!BONOBO_EX (&ev)) {
			Bonobo_PersistStream_load (persist_stream,
						   BONOBO_OBJREF (stream), 
						   mime_type, &ev);
			bonobo_object_unref (stream);
		}
		if (mime_type)
			g_free (mime_type);
		CORBA_exception_free (&ev);
		use_stream = FALSE;
	} else {
		persist_file = Bonobo_Unknown_queryInterface (object, "IDL:Bonobo/PersistFile:1.0", NULL);
		if (!persist_file) {
			g_warning ("PersistFile reference for glimmer control is NULL");
			return;
		}

		Bonobo_PersistFile_load (persist_file, filename, NULL);
	}
}

static void
file_selected_cb (GtkWidget *widget, GtkFileSelection *file_sel)
{
	GtkWidget *window;
	const gchar *filename;

	window = g_object_get_data (G_OBJECT (file_sel), "window");
	filename = gtk_file_selection_get_filename (file_sel);
	open_file (window, filename);
}

static void
open_cb (GtkWidget *widget, gpointer data)
{
	GtkWidget *file_sel;

	file_sel = gtk_file_selection_new ("Open File");
	g_object_set_data (G_OBJECT (file_sel), "window", data);

	g_signal_connect (GTK_FILE_SELECTION (file_sel)->ok_button,
			  "clicked",
			  G_CALLBACK (file_selected_cb),
			  file_sel);

	g_signal_connect_swapped (GTK_FILE_SELECTION (file_sel)->ok_button, 
				  "clicked", G_CALLBACK (gtk_widget_destroy),
				  file_sel);
	g_signal_connect_swapped (GTK_FILE_SELECTION (file_sel)->cancel_button, 
				  "clicked", G_CALLBACK (gtk_widget_destroy),
				  file_sel);

	gtk_widget_show (file_sel);
}

static void
open_stream_cb (GtkWidget *widget, gpointer data)
{
	use_stream = TRUE;
	open_cb (widget, data);
}

static void
save_cb (GtkWidget *widget, gpointer data)
{
}

static void
save_stream_cb (GtkWidget *widget, gpointer data)
{
}

static void
exit_cb (GtkWidget *widget, gpointer data)
{
	bonobo_main_quit ();
}

static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB ("FileOpen", open_cb),
	BONOBO_UI_UNSAFE_VERB ("FileOpenStream", open_stream_cb),
	BONOBO_UI_UNSAFE_VERB ("FileSave", save_cb),
	BONOBO_UI_UNSAFE_VERB ("FileSaveStream", save_stream_cb),
	BONOBO_UI_UNSAFE_VERB ("FileExit", exit_cb),
	BONOBO_UI_VERB_END
};

static GtkWidget *
create_main_window (void)
{
	GtkWidget *window;
	BonoboUIContainer *container;
	Bonobo_UIContainer corba_container;
	BonoboUIComponent *component;
	BonoboControlFrame *frame;
	GtkWidget *widget;
	CORBA_Environment ev;

	bonobo_activate ();

	window = bonobo_window_new ("test-control", "test-control");
	container = bonobo_window_get_ui_container (BONOBO_WINDOW (window));
	component = bonobo_ui_component_new ("test-ui");
	bonobo_ui_component_set_container (component, BONOBO_OBJREF (container), NULL);
	bonobo_ui_util_set_ui (component, ".", "test-ui.xml", "test-ui", NULL);
	bonobo_ui_component_add_verb_list_with_data (component, verbs, window);

	corba_container = bonobo_object_corba_objref (BONOBO_OBJECT (container));
	widget = bonobo_widget_new_control ("OAFIID:GNOME_Development_GlimmerView", corba_container);
	if (!widget) {
		g_error ("Unable to create new control");
		return NULL;
	}
	g_object_set_data (G_OBJECT (window), "control", widget);
	frame = bonobo_widget_get_control_frame (BONOBO_WIDGET (widget));

	bonobo_window_set_contents (BONOBO_WINDOW (window), widget);
	gtk_widget_show_all (widget);
	bonobo_control_frame_control_activate (frame);

	g_signal_connect (G_OBJECT (window), "delete_event", 
			  G_CALLBACK (exit_cb), window);

	CORBA_exception_init (&ev);

	component = bonobo_control_frame_get_popup_component (frame, &ev);
	if (component == NULL || BONOBO_EX (&ev)) {
		g_warning ("Failed to get popup component");
		return window;
	}

	bonobo_ui_component_freeze (component, NULL);

	bonobo_ui_util_set_ui (component, ".", "test-ui.xml", "test-ui-popup", &ev);
	if (BONOBO_EX (&ev)) {
		g_message ("error setting the ui");
		return window;
	}

	bonobo_ui_component_add_verb_list_with_data (component, verbs, window);

	bonobo_ui_component_thaw (component, NULL);

	return window;
}

int
main (int argc, char *argv[])
{
	GtkWidget *window;

	/* Initialization */
	gtk_init (&argc, &argv);
	gnome_vfs_init ();

	if (!bonobo_init (NULL, NULL)) {
		g_error (_("Can't initialize bonobo"));
	}

	window = create_main_window ();

	gtk_window_set_default_size (GTK_WINDOW (window), 500, 500);
	gtk_widget_show (window);

	/* ... and action! */
	bonobo_main ();

	gnome_vfs_shutdown ();

	return 0;
}
