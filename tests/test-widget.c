/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  test-widget.c
 *
 *  Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gconf/gconf-client.h>
#include <libgnome/libgnome.h>
#include <libgnomeui/libgnomeui.h>
#include <libgnomevfs/gnome-vfs-init.h>
#include <libgnomevfs/gnome-vfs-mime-utils.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#include <libglimmerfile/glimmer-view.h>

#define READ_BUFFER_SIZE 4096

/* Private prototypes */
static void       open_file_cb                   (GtkWidget *window,
						  guint      callback_action,
						  GtkWidget *widget);
static void       new_view_cb                    (GtkWidget *window,
						  guint      callback_action,
						  GtkWidget *widget);
static void       view_toggled_cb                (GtkWidget *window,
						  guint      callback_action,
						  GtkWidget *widget);
static void       tabs_toggled_cb                (GtkWidget *window,
						  guint      callback_action,
						  GtkWidget *widget);

/* Menu definition */
#define SHOW_NUMBERS_PATH "/View/Show _Line Numbers"
#define SHOW_MARKERS_PATH "/View/Show _Markers"
#define SHOW_MARGIN_PATH "/View/Show M_argin"

#define ENABLE_AUTO_INDENT_PATH "/View/Enable _Auto Indent"
#define INSERT_SPACES_PATH "/View/Insert _Spaces Instead of Tabs"

static GtkItemFactoryEntry menu_items[] = {
	{ "/_File",                   NULL,         0,               0, "<Branch>" },
	{ "/File/_Open",              "<control>O", open_file_cb,    0, "<StockItem>", GTK_STOCK_OPEN },
	{ "/File/sep1",               NULL,         0,               0, "<Separator>" },
	{ "/File/_Quit",              "<control>Q", gtk_main_quit,   0, "<StockItem>", GTK_STOCK_QUIT },
	
	{ "/_View",                   NULL,         0,               0, "<Branch>" },
	{ "/View/_New View",          NULL,         new_view_cb,     0, "<StockItem>", GTK_STOCK_NEW },
	{ "/View/sep1",               NULL,         0,               0, "<Separator>" },
	{ SHOW_NUMBERS_PATH,          NULL,         view_toggled_cb, 1, "<CheckItem>" },
	{ SHOW_MARKERS_PATH,          NULL,         view_toggled_cb, 2, "<CheckItem>" },
	{ SHOW_MARGIN_PATH,           NULL,         view_toggled_cb, 5, "<CheckItem>" },

	{ "/View/sep2",               NULL,         0,               0, "<Separator>" },
	{ ENABLE_AUTO_INDENT_PATH,    NULL,         view_toggled_cb, 3, "<CheckItem>" },
	{ INSERT_SPACES_PATH,         NULL,         view_toggled_cb, 4, "<CheckItem>" },

	{ "/View/sep3",               NULL,         0,               0, "<Separator>" },
	{ "/_View/_Tabs Width",	      NULL,         0,	             0, "<Branch>" },
	{ "/View/Tabs Width/4",	      NULL,         tabs_toggled_cb, 4, "<RadioItem>" },
	{ "/View/Tabs Width/6",	      NULL,         tabs_toggled_cb, 6, "/View/Tabs Width/4" },
	{ "/View/Tabs Width/8",	      NULL,         tabs_toggled_cb, 8, "/View/Tabs Width/4" },
	{ "/View/Tabs Width/10",      NULL,         tabs_toggled_cb, 10, "/View/Tabs Width/4" },
	{ "/View/Tabs Width/12",      NULL,         tabs_toggled_cb, 12, "/View/Tabs Width/4" }
};

static void
error_dialog (GtkWindow *parent, const gchar *msg, ...)
{
	va_list ap;
	gchar *tmp;
	GtkWidget *dialog;

	va_start (ap, msg);
	tmp = g_strdup_vprintf (msg, ap);
	va_end (ap);

	dialog = gtk_message_dialog_new (parent,
					 GTK_DIALOG_DESTROY_WITH_PARENT,
					 GTK_MESSAGE_ERROR,
					 GTK_BUTTONS_OK,
					 tmp);
	g_free (tmp);

	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

static gboolean
gtk_source_buffer_load_with_encoding (GtkSourceBuffer *source_buffer,
				      const gchar     *filename,
				      const gchar     *encoding,
				      GError         **error)
{
	GIOChannel *io;
	GtkTextIter iter;
	gchar *buffer;
	gboolean reading;
	
	g_return_val_if_fail (source_buffer != NULL, FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_SOURCE_BUFFER (source_buffer), FALSE);

	*error = NULL;

	io = g_io_channel_new_file (filename, "r", error);
	if (!io)
	{
		error_dialog (NULL, "%s\nFile %s", (*error)->message, filename);
		return FALSE;
	}

	if (g_io_channel_set_encoding (io, encoding, error) != G_IO_STATUS_NORMAL)
	{
		error_dialog (NULL, "Failed to set encoding:\n%s\n%s",
			      filename, (*error)->message);
		return FALSE;
	}

	gtk_source_buffer_begin_not_undoable_action (source_buffer);

	gtk_text_buffer_set_text (GTK_TEXT_BUFFER (source_buffer), "", 0);
	buffer = g_malloc (READ_BUFFER_SIZE);
	reading = TRUE;
	while (reading)
	{
		gsize bytes_read;
		GIOStatus status;
		
		status = g_io_channel_read_chars (io, buffer,
						  READ_BUFFER_SIZE, &bytes_read,
						  error);
		switch (status)
		{
			case G_IO_STATUS_EOF:
				reading = FALSE;
				/* fall through */
				
			case G_IO_STATUS_NORMAL:
				if (bytes_read == 0)
				{
					continue;
				}
				
				gtk_text_buffer_get_end_iter (
					GTK_TEXT_BUFFER (source_buffer), &iter);
				gtk_text_buffer_insert (GTK_TEXT_BUFFER (source_buffer),
							&iter, buffer, bytes_read);
				break;
				
			case G_IO_STATUS_AGAIN:
				continue;

			case G_IO_STATUS_ERROR:
			default:
				error_dialog (NULL, "%s\nFile %s", (*error)->message, filename);

				/* because of error in input we clear already loaded text */
				gtk_text_buffer_set_text (GTK_TEXT_BUFFER (source_buffer), "", 0);
				
				reading = FALSE;
				break;
		}
	}
	g_free (buffer);
	
	gtk_source_buffer_end_not_undoable_action (source_buffer);

	g_io_channel_unref (io);

	if (*error)
		return FALSE;

	gtk_text_buffer_set_modified (GTK_TEXT_BUFFER (source_buffer), FALSE);

	/* move cursor to the beginning */
	gtk_text_buffer_get_start_iter (GTK_TEXT_BUFFER (source_buffer), &iter);
	gtk_text_buffer_place_cursor (GTK_TEXT_BUFFER (source_buffer), &iter);

	return TRUE;
}

static void
open_file (GtkWidget *window, const gchar *filename)
{
	GlimmerView *view;
	GtkSourceBuffer *buffer;
	gchar *mime_type;
	GError *err = NULL;
	gchar *uri;

	view = g_object_get_data (G_OBJECT (window), "view");
	buffer = GTK_SOURCE_BUFFER (GTK_TEXT_VIEW (view)->buffer);

	/* I hate this! */
	if (g_path_is_absolute (filename)) {
		uri = gnome_vfs_get_uri_from_local_path (filename);
	} else {
		gchar *curdir, *path;

		curdir = g_get_current_dir ();
		path = g_strconcat (curdir, "/", filename, NULL);
		g_free (curdir);
		uri = gnome_vfs_get_uri_from_local_path (path);
		g_free (path);
	}

	mime_type = gnome_vfs_get_mime_type (uri);
	g_free (uri);
	if (mime_type) {
		glimmer_view_set_mime_type (view, mime_type);
		g_free (mime_type);
	} else {
		g_warning ("Couldn't get mime type for file `%s'", filename);
	}

	gtk_source_buffer_load_with_encoding (buffer, filename, "utf-8", &err);

	if (err != NULL) {
		g_error_free (err);
	}
}

static void
file_selected_cb (GtkWidget *widget, GtkFileSelection *file_sel)
{
	GtkWidget *window;
	const gchar *filename;

	window = g_object_get_data (G_OBJECT (file_sel), "window");
	filename = gtk_file_selection_get_filename (file_sel);
	open_file (window, filename);
}

static void 
open_file_cb (GtkWidget *window,
	      guint      callback_action,
	      GtkWidget *widget)
{
	GtkWidget *file_sel;

	file_sel = gtk_file_selection_new ("Open File");
	g_object_set_data (G_OBJECT (file_sel), "window", window);

	g_signal_connect (GTK_FILE_SELECTION (file_sel)->ok_button,
			  "clicked",
			  G_CALLBACK (file_selected_cb),
			  file_sel);

	g_signal_connect_swapped (GTK_FILE_SELECTION (file_sel)->ok_button, 
				  "clicked", G_CALLBACK (gtk_widget_destroy),
				  file_sel);
	g_signal_connect_swapped (GTK_FILE_SELECTION (file_sel)->cancel_button, 
				  "clicked", G_CALLBACK (gtk_widget_destroy),
				  file_sel);

	gtk_widget_show (file_sel);
}

static void
new_view_cb (GtkWidget *window,
	     guint callback_action,
	     GtkWidget *widget)
{
}

static void
view_toggled_cb (GtkWidget *window,
		 guint callback_action,
		 GtkWidget *widget)
{
}

static void
tabs_toggled_cb (GtkWidget *window,
		 guint callback_action,
		 GtkWidget *widget)
{
}

static gboolean
window_deleted_cb (GtkWidget *widget, GdkEvent *ev, gpointer data)
{
	gtk_main_quit ();

	return FALSE;
}

static GtkWidget *
create_window (void)
{
	GtkWidget *window, *sw, *view, *vbox;
	GConfClient *conf_client;
	char *monospace_font;
	PangoFontDescription *monospace_font_desc;

	/* window */
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_container_set_border_width (GTK_CONTAINER (window), 0);
	gtk_window_set_title (GTK_WINDOW (window), "Glimmer Demo");
	g_signal_connect (window, "delete-event", G_CALLBACK (window_deleted_cb), window);

	/* vbox */
	vbox = gtk_vbox_new (0, FALSE);
	gtk_container_add (GTK_CONTAINER (window), vbox);
	g_object_set_data (G_OBJECT (window), "vbox", vbox);
	gtk_widget_show (vbox);

	/* scrolled window */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_box_pack_end (GTK_BOX (vbox), sw, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw),
					     GTK_SHADOW_IN);
	gtk_widget_show (sw);

	/* view */
	view = glimmer_view_new ();
	g_object_set_data (G_OBJECT (window), "view", view);
	gtk_container_add (GTK_CONTAINER (sw), view);
	gtk_widget_show (view);

	/* Pick up the monospace font from desktop preferences */
	conf_client = gconf_client_get_default ();
	monospace_font = gconf_client_get_string (conf_client, "/desktop/gnome/interface/monospace_font_name", NULL);
	if (monospace_font) {
		monospace_font_desc = pango_font_description_from_string (monospace_font);
		gtk_widget_modify_font (view, monospace_font_desc);
		pango_font_description_free (monospace_font_desc);
	}
	g_object_unref (conf_client);

	return window;
}

static GtkWidget *
create_main_window (void)
{
	GtkWidget *window;
	GtkAccelGroup *accel_group;
	GtkWidget *vbox;
	GtkWidget *menu;
	GtkWidget *radio_item;
	gchar *radio_path;
	GtkItemFactory *item_factory;

	window = create_window ();
	vbox = g_object_get_data (G_OBJECT (window), "vbox");

	/* item factory/menu */
	accel_group = gtk_accel_group_new ();
	item_factory = gtk_item_factory_new (GTK_TYPE_MENU_BAR, "<main>", accel_group);
	gtk_window_add_accel_group (GTK_WINDOW (window), accel_group);
	g_object_unref (accel_group);
	gtk_item_factory_create_items (item_factory,
				       G_N_ELEMENTS (menu_items),
				       menu_items,
				       window);
	menu = gtk_item_factory_get_widget (item_factory, "<main>");
	gtk_box_pack_start (GTK_BOX (vbox), menu, FALSE, FALSE, 0);
	gtk_widget_show (menu);

	/* preselect menu checkitems */
	gtk_check_menu_item_set_active (
		GTK_CHECK_MENU_ITEM (gtk_item_factory_get_item (item_factory,
								"/View/Show Line Numbers")),
		TRUE);
	gtk_check_menu_item_set_active (
		GTK_CHECK_MENU_ITEM (gtk_item_factory_get_item (item_factory,
								"/View/Show Markers")),
		TRUE);
	gtk_check_menu_item_set_active (
		GTK_CHECK_MENU_ITEM (gtk_item_factory_get_item (item_factory,
								"/View/Show Margin")),
		TRUE);

	gtk_check_menu_item_set_active (
		GTK_CHECK_MENU_ITEM (gtk_item_factory_get_item (item_factory,
								"/View/Enable Auto Indent")),
		TRUE);
	gtk_check_menu_item_set_active (
		GTK_CHECK_MENU_ITEM (gtk_item_factory_get_item (item_factory,
								"/View/Insert Spaces Instead of Tabs")),
		FALSE);

	radio_path = g_strdup_printf ("/View/Tabs Width/%d", 8);
	radio_item = gtk_item_factory_get_item (item_factory, radio_path);
	if (radio_item)
		gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (radio_item), TRUE);
	g_free (radio_path);

	return window;
}

int
main (int argc, char *argv[])
{
	GtkWidget *window;

	/* Initialization */
	gtk_init (&argc, &argv);
	gnome_vfs_init ();

	window = create_main_window ();

	gtk_window_set_default_size (GTK_WINDOW (window), 500, 500);
	gtk_widget_show (window);

	/* ... and action! */
	gtk_main ();

	gnome_vfs_shutdown ();

	return 0;
}
