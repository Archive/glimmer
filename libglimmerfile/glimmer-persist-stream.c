/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  glimmer-persist-stream.c
 *
 *  Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.  
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <libbonobo.h>
#include <gtksourceview/gtksourcebuffer.h>
#include <gtksourceview/gtksourceview.h>
#include "glimmer-persist-stream.h"
#include "glimmer-view.h"
#include "glimmer-convert.h"

static int
impl_save (BonoboPersistStream       *ps,
	   const Bonobo_Stream        stream,
	   Bonobo_Persist_ContentType type,
	   void                      *closure,
	   CORBA_Environment         *ev)
{
	GtkTextBuffer *buffer = GTK_TEXT_VIEW (closure)->buffer;
	GtkTextIter start;
	GtkTextIter end;
	char *data;
	Bonobo_Stream_iobuf buf;
	int i;
	int read_size = 16 *1024;
	gint length;

	gtk_text_buffer_get_start_iter (buffer, &start);
	length = gtk_text_buffer_get_char_count (buffer);

	for (i = 0; i < length; i += read_size) {
		if ((i + read_size) >= length)
			read_size = length - i;
		end = start;
		gtk_text_iter_forward_chars (&end, read_size);
		data = gtk_text_iter_get_slice (&start, &end);
		/* _length is in bytes, not chars so use strlen to get the bytecount. */
		buf._length = strlen (data);
		buf._buffer = CORBA_string_dup (data);
		g_free (data);
		Bonobo_Stream_write (stream, &buf, ev);
		start = end;
	}

	gtk_text_buffer_set_modified (buffer, FALSE);

	return 0;
}

static int
impl_load (BonoboPersistStream       *ps,
	   Bonobo_Stream              stream,
	   Bonobo_Persist_ContentType type,
	   void                      *closure,
	   CORBA_Environment         *ev)
{
	GtkTextView *view = GTK_TEXT_VIEW (closure);
	GtkTextBuffer *buffer = view->buffer;
	GtkTextIter start;
	GtkTextIter end;
	GString *text_buf;
	gint read_size = 16 *1024;

	gtk_source_buffer_begin_not_undoable_action (GTK_SOURCE_BUFFER (buffer));

	gtk_text_buffer_get_start_iter (buffer, &start);
	gtk_text_buffer_get_end_iter (buffer, &end);
	gtk_text_buffer_delete (buffer, &start, &end);

	glimmer_view_set_mime_type (GLIMMER_VIEW (view), (const gchar *)type);

	text_buf = g_string_new ("");

	/* Read the complete stream first. */
	while (TRUE) {
		Bonobo_Stream_iobuf *buf;

		Bonobo_Stream_read (stream, read_size, &buf, ev);

		if (ev->_major != CORBA_NO_EXCEPTION)
			break;

		if (buf->_length)
			text_buf = g_string_append_len (text_buf, buf->_buffer,
							buf->_length);

		if (buf->_length < read_size)
			break;
	}

	if (text_buf->len > 0) {
		gchar *converted_text;
		int len;

		if (g_utf8_validate (text_buf->str, text_buf->len, NULL)) {
			converted_text = text_buf->str;
			len = text_buf->len;
		} else {
			converted_text = glimmer_convert_to_utf8 (text_buf->str,
								  text_buf->len,
								  NULL);
			len = strlen (converted_text);
			g_free (text_buf->str);
		}

		if (converted_text == NULL) {
			g_warning ("Invalid UTF-8 data");
			g_string_free (text_buf, FALSE);
			return FALSE;
		}

		gtk_text_buffer_get_end_iter (buffer, &end);
		gtk_text_buffer_insert (buffer,
					&end,
					converted_text,
					len);
		g_free (converted_text);
	}

	g_string_free (text_buf, FALSE);

	gtk_text_buffer_get_start_iter (buffer, &start);
	gtk_text_buffer_place_cursor (buffer, &start);
	gtk_text_view_place_cursor_onscreen (view);
	gtk_text_buffer_set_modified (buffer, FALSE);

	gtk_source_buffer_end_not_undoable_action (GTK_SOURCE_BUFFER (buffer));

	return 0;
}

static Bonobo_Persist_ContentTypeList *
impl_get_content_types (BonoboPersistStream *ps,
			gpointer             data,
			CORBA_Environment   *ev)
{
	return bonobo_persist_generate_content_types (1, "text/*");
}

BonoboPersistStream *
glimmer_persist_stream_new (GlimmerView *view)
{
	BonoboPersistStream *stream;

	stream = bonobo_persist_stream_new ((BonoboPersistStreamIOFn) impl_load,
					    (BonoboPersistStreamIOFn) impl_save,
					    (BonoboPersistStreamTypesFn) impl_get_content_types,
					    "GlimmerPersistStream",
					    view);

	g_object_set_data (G_OBJECT (view), "PersistStream", stream);

	return stream;
}
