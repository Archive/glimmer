/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  glimmer-themes.c
 *
 *  Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <gconf/gconf-client.h>
#include <gtksourceview/gtksourcetagstyle.h>
#include <libgnome/gnome-macros.h>
#include <libxml/tree.h>
#include "glimmer-themes.h"
#include "glimmer-settings.h"

struct _GlimmerThemesPrivate {
	gchar *translation_domain;
	GSList *available_themes;
};

GNOME_CLASS_BOILERPLATE (GlimmerThemes, glimmer_themes, GObject, G_TYPE_OBJECT);

static void
glimmer_themes_finalize (GObject *object)
{
	GlimmerThemes *themes = GLIMMER_THEMES (object);

	if (themes->priv) {
		g_free (themes->priv->translation_domain);
		g_slist_foreach (themes->priv->available_themes, (GFunc)g_free, NULL);
		g_slist_free (themes->priv->available_themes);
		g_free (themes->priv);
		themes->priv = NULL;
	}
}

static GSList *
build_file_list (const char *dirname, GSList *files)
{
	GDir *dir;
	const char *filename;

	dir = g_dir_open (dirname, 0, NULL);
	if (!dir)
		return files;

	while ((filename = g_dir_read_name (dir))) {
		files = g_slist_append (files, g_strconcat (dirname, "/",
							    filename, NULL));
	}

	return files;
}

static gboolean
parse_style (const char *folder,
	     xmlNodePtr node,
	     GConfClient *client)
{
	xmlChar *unesc, *content;
	gboolean val;
	char *name, *key;
	
	unesc = xmlGetProp (node, "name");
	name = gconf_escape_key (unesc, xmlStrlen (unesc));
	xmlFree (unesc);

	node = node->xmlChildrenNode;
	while (node) {
		content = xmlNodeGetContent (node);
		if (!content) {
			g_warning ("Empty element in style '%s'", name);
			g_free (name);
			return FALSE;
		}
		
		if (!xmlStrcmp (node->name, (const xmlChar *) "bold")) {
			val = !xmlStrcasecmp (content, (const xmlChar *) "TRUE");
			key = g_strconcat (folder, "/", name, "_bold", NULL);
			gconf_client_set_bool (client, key, val, NULL);
			g_free (key);
		} else if (!xmlStrcmp (node->name, (const xmlChar *) "italic")) {
			val = !xmlStrcasecmp (content, (const xmlChar *) "TRUE");
			key = g_strconcat (folder, "/", name, "_italic", NULL);
			gconf_client_set_bool (client, key, val, NULL);
			g_free (key);
		} else if (!xmlStrcmp (node->name, (const xmlChar *) "background")) {
			key = g_strconcat (folder, "/", name, "_background", NULL);
			gconf_client_set_string (client, key, content, NULL);
			g_free (key);
			key = g_strconcat (folder, "/", name, "_usebackground", NULL);
			gconf_client_set_bool (client, key, TRUE, NULL);
			g_free (key);
		} else if (!xmlStrcmp (node->name, (const xmlChar *) "foreground")) {
			key = g_strconcat (folder, "/", name, "_foreground", NULL);
			gconf_client_set_string (client, key, content, NULL);
			g_free (key);
			key = g_strconcat (folder, "/", name, "_useforeground", NULL);
			gconf_client_set_bool (client, key, TRUE, NULL);
			g_free (key);
		}
		xmlFree (content);
		node = node->next;
	}
	
	g_free (name);

	return TRUE;
}

static gboolean
parse_theme (const char *theme_name,
	     xmlNodePtr root,
	     GConfClient *client)
{
	char *folder;
	xmlNodePtr node;

	/* Check if theme is already installed. */
	folder = g_strconcat (GLIMMER_BASE_KEY, GLIMMER_THEMES_KEY, "/",
			      theme_name, NULL);
	if (gconf_client_dir_exists (client, folder, NULL)) {
		g_free (folder);
		return TRUE;
	}

	node = root->xmlChildrenNode;
	while (node) {
		if (!xmlStrcmp (node->name, (const xmlChar *) "style")) {
			if (!parse_style (folder, node, client)) {
				g_free (folder);
				return FALSE;
			} 
		}
	
		node = node->next;
	}
	
	g_free (folder);

	return TRUE;
}

static void
install_themes (GlimmerThemes *themes)
{
	const char *installdir = DATADIR "/glimmer/themes";
	char *homedir;
	GSList *l, *theme_list = NULL;
	GConfClient *client;

	/* Get themes list. */
	theme_list = build_file_list (installdir, theme_list);
	homedir = g_strdup_printf ("%s/.glimmer/themes", g_get_home_dir ());
	theme_list = build_file_list (homedir, theme_list);
	g_free (homedir);

	client = gconf_client_get_default ();

	for (l = theme_list; l; l = l->next) {
		char *filename = l->data;
		xmlDocPtr doc;
		xmlNodePtr root;
		xmlChar *name, *id_temp;
		char *id;

		doc = xmlParseFile (filename);
		if (!doc) {
			g_warning ("Impossible to parse file '%s'", filename);
			g_free (filename);
			continue;
		}
		
		root = xmlDocGetRootElement (doc);
		if (!root) {
			g_warning ("The theme file '%s' is empty", filename);
			xmlFreeDoc (doc);
			g_free (filename);
			continue;
		}
			
		if (xmlStrcmp (root->name, (const xmlChar *) "glimmer-theme")) {
			g_warning ("File '%s' is of the wrong type", filename);
			xmlFreeDoc (doc);
			g_free (filename);
			continue;
		}
		
		name = xmlGetProp (root, "_name");
		if (name == NULL) {
			name = xmlGetProp (root, "name");
			id_temp = xmlStrdup (name);
		} else {
			/* FIXME: Add i18n stuff. */
			xmlChar *tmp = xmlStrdup (/*dgettext (themes->priv->translation_domain,*/ name/*)*/);
			id_temp = xmlStrdup (name);
			xmlFree (name);
			name = tmp;
		}
		
		id = gconf_escape_key (id_temp, xmlStrlen (id_temp));
		xmlFree (id_temp);
	
		if (parse_theme (id, root, client)) {
			themes->priv->available_themes = g_slist_append (
				themes->priv->available_themes, xmlStrdup (name));
		}
		
		g_free (id);
		xmlFree (name);
		xmlFreeDoc (doc);
		g_free (filename);
	}
	
	g_slist_free (theme_list);
	g_object_unref (G_OBJECT (client));
}

static void
glimmer_themes_class_init (GlimmerThemesClass *klass) 
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = glimmer_themes_finalize;
}

static void
glimmer_themes_instance_init (GlimmerThemes *themes)
{
	themes->priv = g_new0 (GlimmerThemesPrivate, 1);
	themes->priv->translation_domain = g_strdup (GETTEXT_PACKAGE);
	themes->priv->available_themes = NULL;
	
	install_themes (themes);
}

GlimmerThemes *
glimmer_themes_new (void)
{
	return GLIMMER_THEMES (g_object_new (GLIMMER_TYPE_THEMES, NULL));
}

GSList *
glimmer_themes_get_available_themes (GlimmerThemes *themes)
{
	g_return_val_if_fail (GLIMMER_IS_THEMES (themes), NULL);

	return themes->priv->available_themes;
}

static void
set_style_color (const gchar *value,
		 GdkColor    *color)
{
	unsigned int r = 0, g = 0, b = 0;

	g_return_if_fail (color != NULL);

	if (value && (strlen (value) == 8) &&
	    (sscanf (value, "0x%02X%02X%02X", &r, &g, &b) == 3)) {
		color->red = r * 256;
		color->green = g * 256;
		color->blue = b * 256;
	}
}

GHashTable *
glimmer_themes_get_style_tags (GlimmerThemes *themes,
			       const gchar   *theme,
			       const gchar   *language_id)
{
	GHashTable *tags;
	GConfClient *client;
	gint prefix;
	gchar *folder, *key, *style_class, *style_pref, *style_name;
	GSList *entries, *l;
	GError *err = NULL;
	GtkSourceTagStyle *style;

	g_return_val_if_fail (GLIMMER_IS_THEMES (themes), NULL);
	g_return_val_if_fail (theme != NULL, NULL);

	tags = g_hash_table_new_full ((GHashFunc)g_str_hash,
				      (GEqualFunc)g_str_equal,
				      (GDestroyNotify)g_free,
				      (GDestroyNotify)gtk_source_tag_style_free);

	client = gconf_client_get_default ();
	
	if (language_id)
		folder = g_strconcat (GLIMMER_BASE_KEY, GLIMMER_THEMES_KEY, "/",
				      theme, "/", language_id, NULL);
	else
		folder = g_strconcat (GLIMMER_BASE_KEY, GLIMMER_THEMES_KEY, "/",
				      theme, NULL);

	entries = gconf_client_all_entries (client, folder, &err);
	if (err) {
		g_warning ("Error retrieving styles from gconf: '%s'", err->message);
		g_error_free (err);
		g_hash_table_destroy (tags);
		g_object_unref (G_OBJECT (client));
		return NULL;
	}
	
	prefix = strlen (folder) + 1;

	for (l = entries; l != NULL; l = l->next) {
		GConfEntry *entry = l->data;
		
		key = g_malloc (strlen (entry->key) - prefix + 1);
		strcpy (key, entry->key + prefix);
		key[strlen (entry->key) - prefix] = '\0';

		/* Do some string magic to decode the style class and preference. */
		style_class = strchr (key, '_');
		style_pref = style_class + 1;
		style_class = g_strndup (key, style_class - key);
		style_name = g_strdup (style_class);
		
		style = g_hash_table_lookup (tags, style_name);
		if (!style) {
			style = gtk_source_tag_style_new ();
			g_hash_table_insert (tags, style_name, style);
		}
		
		if (!strcmp (style_pref, "background")) {
			set_style_color (gconf_value_get_string (entry->value),
					 &style->background);
		} else if (!strcmp (style_pref, "usebackground")) {
			if (gconf_value_get_bool (entry->value)) 
				style->mask |= GTK_SOURCE_TAG_STYLE_USE_BACKGROUND;
			else
				style->mask &= ~GTK_SOURCE_TAG_STYLE_USE_BACKGROUND;
		} else if (!strcmp (style_pref, "foreground")) {
			set_style_color (gconf_value_get_string (entry->value),
					 &style->foreground);
		} else if (!strcmp (style_pref, "useforeground")) {
			if (gconf_value_get_bool (entry->value)) 
				style->mask |= GTK_SOURCE_TAG_STYLE_USE_FOREGROUND;
			else
				style->mask &= ~GTK_SOURCE_TAG_STYLE_USE_FOREGROUND;
		} else if (!strcmp (style_pref, "bold")) {
			style->bold = gconf_value_get_bool (entry->value);
		} else if (!strcmp (style_pref, "italic")) {
			style->italic = gconf_value_get_bool (entry->value);
		} else {
			g_warning ("Unknown style pref '%s'", style_pref);
		}

		g_free (style_class);
		g_free (key);
	}

	g_free (folder);
	g_object_unref (G_OBJECT (client));

	return tags;
}
