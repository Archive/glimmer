/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *  glimmer-encodings.c
 *
 *  Copyright (C) 2003 - Paolo Maggi
 *                       Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
/*
 * Copied from the gedit project. Original author Paolo Maggi.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <bonobo/bonobo-i18n.h>
#include <string.h>
#include "glimmer-encodings.h"

struct _GlimmerEncoding {
	gint   index;
	gchar *charset;
	gchar *name;
};

/* 
 * The original versions of the following tables are taken from profterm 
 *
 * Copyright (C) 2002 Red Hat, Inc.
 */
typedef enum {
	GLIMMER_ENCODING_ISO_8859_1,
	GLIMMER_ENCODING_ISO_8859_2,
	GLIMMER_ENCODING_ISO_8859_3,
	GLIMMER_ENCODING_ISO_8859_4,
	GLIMMER_ENCODING_ISO_8859_5,
	GLIMMER_ENCODING_ISO_8859_6,
	GLIMMER_ENCODING_ISO_8859_7,
	GLIMMER_ENCODING_ISO_8859_8,
	GLIMMER_ENCODING_ISO_8859_8_I,
	GLIMMER_ENCODING_ISO_8859_9,
	GLIMMER_ENCODING_ISO_8859_10,
	GLIMMER_ENCODING_ISO_8859_13,
	GLIMMER_ENCODING_ISO_8859_14,
	GLIMMER_ENCODING_ISO_8859_15,
	GLIMMER_ENCODING_ISO_8859_16,

	GLIMMER_ENCODING_UTF_7,
	GLIMMER_ENCODING_UTF_16,
	GLIMMER_ENCODING_UCS_2,
	GLIMMER_ENCODING_UCS_4,

	GLIMMER_ENCODING_ARMSCII_8,
	GLIMMER_ENCODING_BIG5,
	GLIMMER_ENCODING_BIG5_HKSCS,
	GLIMMER_ENCODING_CP_866,

	GLIMMER_ENCODING_EUC_JP,
	GLIMMER_ENCODING_EUC_KR,
	GLIMMER_ENCODING_EUC_TW,

	GLIMMER_ENCODING_GB18030,
	GLIMMER_ENCODING_GB2312,
	GLIMMER_ENCODING_GBK,
	GLIMMER_ENCODING_GEOSTD8,
	GLIMMER_ENCODING_HZ,

	GLIMMER_ENCODING_IBM_850,
	GLIMMER_ENCODING_IBM_852,
	GLIMMER_ENCODING_IBM_855,
	GLIMMER_ENCODING_IBM_857,
	GLIMMER_ENCODING_IBM_862,
	GLIMMER_ENCODING_IBM_864,

	GLIMMER_ENCODING_ISO_2022_JP,
	GLIMMER_ENCODING_ISO_2022_KR,
	GLIMMER_ENCODING_ISO_IR_111,
	GLIMMER_ENCODING_JOHAB,
	GLIMMER_ENCODING_KOI8_R,
	GLIMMER_ENCODING_KOI8_U,

	GLIMMER_ENCODING_SHIFT_JIS,
	GLIMMER_ENCODING_TCVN,
	GLIMMER_ENCODING_TIS_620,
	GLIMMER_ENCODING_UHC,
	GLIMMER_ENCODING_VISCII,

	GLIMMER_ENCODING_WINDOWS_1250,
	GLIMMER_ENCODING_WINDOWS_1251,
	GLIMMER_ENCODING_WINDOWS_1252,
	GLIMMER_ENCODING_WINDOWS_1253,
	GLIMMER_ENCODING_WINDOWS_1254,
	GLIMMER_ENCODING_WINDOWS_1255,
	GLIMMER_ENCODING_WINDOWS_1256,
	GLIMMER_ENCODING_WINDOWS_1257,
	GLIMMER_ENCODING_WINDOWS_1258,

	GLIMMER_ENCODING_LAST
} GlimmerEncodingIndex;

static GlimmerEncoding encodings [] = {
	{ GLIMMER_ENCODING_ISO_8859_1,
	  "ISO-8859-1", N_("Western") },
	{ GLIMMER_ENCODING_ISO_8859_2,
	  "ISO-8859-2", N_("Central European") },
	{ GLIMMER_ENCODING_ISO_8859_3,
	  "ISO-8859-3", N_("South European") },
	{ GLIMMER_ENCODING_ISO_8859_4,
	  "ISO-8859-4", N_("Baltic") },
	{ GLIMMER_ENCODING_ISO_8859_5,
	  "ISO-8859-5", N_("Cyrillic") },
	{ GLIMMER_ENCODING_ISO_8859_6,
	  "ISO-8859-6", N_("Arabic") },
	{ GLIMMER_ENCODING_ISO_8859_7,
	  "ISO-8859-7", N_("Greek") },
	{ GLIMMER_ENCODING_ISO_8859_8,
	  "ISO-8859-8", N_("Hebrew Visual") },
	{ GLIMMER_ENCODING_ISO_8859_8_I,
	  "ISO-8859-8-I", N_("Hebrew") },
	{ GLIMMER_ENCODING_ISO_8859_9,
	  "ISO-8859-9", N_("Turkish") },
	{ GLIMMER_ENCODING_ISO_8859_10,
	  "ISO-8859-10", N_("Nordic") },
	{ GLIMMER_ENCODING_ISO_8859_13,
	  "ISO-8859-13", N_("Baltic") },
	{ GLIMMER_ENCODING_ISO_8859_14,
	  "ISO-8859-14", N_("Celtic") },
	{ GLIMMER_ENCODING_ISO_8859_15,
	  "ISO-8859-15", N_("Western") },
	{ GLIMMER_ENCODING_ISO_8859_16,
	  "ISO-8859-16", N_("Romanian") },

	{ GLIMMER_ENCODING_UTF_7,
	  "UTF-7", N_("Unicode") },
	{ GLIMMER_ENCODING_UTF_16,
	  "UTF-16", N_("Unicode") },
	{ GLIMMER_ENCODING_UCS_2,
	  "UCS-2", N_("Unicode") },
	{ GLIMMER_ENCODING_UCS_4,
	  "UCS-4", N_("Unicode") },

	{ GLIMMER_ENCODING_ARMSCII_8,
	  "ARMSCII-8", N_("Armenian") },
	{ GLIMMER_ENCODING_BIG5,
	  "BIG5", N_("Chinese Traditional") },
	{ GLIMMER_ENCODING_BIG5_HKSCS,
	  "BIG5-HKSCS", N_("Chinese Traditional") },
	{ GLIMMER_ENCODING_CP_866,
	  "CP866", N_("Cyrillic/Russian") },

	{ GLIMMER_ENCODING_EUC_JP,
	  "EUC-JP", N_("Japanese") },
	{ GLIMMER_ENCODING_EUC_KR,
	  "EUC-KR", N_("Korean") },
	{ GLIMMER_ENCODING_EUC_TW,
	  "EUC-TW", N_("Chinese Traditional") },

	{ GLIMMER_ENCODING_GB18030,
	  "GB18030", N_("Chinese Simplified") },
	{ GLIMMER_ENCODING_GB2312,
	  "GB2312", N_("Chinese Simplified") },
	{ GLIMMER_ENCODING_GBK,
	  "GBK", N_("Chinese Simplified") },
	{ GLIMMER_ENCODING_GEOSTD8,
	  "GEORGIAN-ACADEMY", N_("Georgian") }, /* FIXME GEOSTD8 ? */
	{ GLIMMER_ENCODING_HZ,
	  "HZ", N_("Chinese Simplified") },

	{ GLIMMER_ENCODING_IBM_850,
	  "IBM850", N_("Western") },
	{ GLIMMER_ENCODING_IBM_852,
	  "IBM852", N_("Central European") },
	{ GLIMMER_ENCODING_IBM_855,
	  "IBM855", N_("Cyrillic") },
	{ GLIMMER_ENCODING_IBM_857,
	  "IBM857", N_("Turkish") },
	{ GLIMMER_ENCODING_IBM_862,
	  "IBM862", N_("Hebrew") },
	{ GLIMMER_ENCODING_IBM_864,
	  "IBM864", N_("Arabic") },

	{ GLIMMER_ENCODING_ISO_2022_JP,
	  "ISO-2022-JP", N_("Japanese") },
	{ GLIMMER_ENCODING_ISO_2022_KR,
	  "ISO-2022-KR", N_("Korean") },
	{ GLIMMER_ENCODING_ISO_IR_111,
	  "ISO-IR-111", N_("Cyrillic") },
	{ GLIMMER_ENCODING_JOHAB,
	  "JOHAB", N_("Korean") },
	{ GLIMMER_ENCODING_KOI8_R,
	  "KOI8R", N_("Cyrillic") },
	{ GLIMMER_ENCODING_KOI8_U,
	  "KOI8U", N_("Cyrillic/Ukrainian") },

	{ GLIMMER_ENCODING_SHIFT_JIS,
	  "SHIFT_JIS", N_("Japanese") },
	{ GLIMMER_ENCODING_TCVN,
	  "TCVN", N_("Vietnamese") },
	{ GLIMMER_ENCODING_TIS_620,
	  "TIS-620", N_("Thai") },
	{ GLIMMER_ENCODING_UHC,
	  "UHC", N_("Korean") },
	{ GLIMMER_ENCODING_VISCII,
	  "VISCII", N_("Vietnamese") },

	{ GLIMMER_ENCODING_WINDOWS_1250,
	  "WINDOWS-1250", N_("Central European") },
	{ GLIMMER_ENCODING_WINDOWS_1251,
	  "WINDOWS-1251", N_("Cyrillic") },
	{ GLIMMER_ENCODING_WINDOWS_1252,
	  "WINDOWS-1252", N_("Western") },
	{ GLIMMER_ENCODING_WINDOWS_1253,
	  "WINDOWS-1253", N_("Greek") },
	{ GLIMMER_ENCODING_WINDOWS_1254,
	  "WINDOWS-1254", N_("Turkish") },
	{ GLIMMER_ENCODING_WINDOWS_1255,
	  "WINDOWS-1255", N_("Hebrew") },
	{ GLIMMER_ENCODING_WINDOWS_1256,
	  "WINDOWS-1256", N_("Arabic") },
	{ GLIMMER_ENCODING_WINDOWS_1257,
	  "WINDOWS-1257", N_("Baltic") },
	{ GLIMMER_ENCODING_WINDOWS_1258,
	  "WINDOWS-1258", N_("Vietnamese") }
};

static void
glimmer_encoding_lazy_init (void)
{
	static gboolean initialized = FALSE;
	gint i;

	if (initialized)
		return;

	g_return_if_fail (G_N_ELEMENTS (encodings) == GLIMMER_ENCODING_LAST);

	i = 0;
	while (i < GLIMMER_ENCODING_LAST) {
		g_return_if_fail (encodings[i].index == i);

		/* Translate the names */
		encodings[i].name = _(encodings[i].name);

		++i;
	}

	initialized = TRUE;
}

const GlimmerEncoding *
glimmer_encoding_get_from_charset (const gchar *charset)
{
	gint i;

	glimmer_encoding_lazy_init ();

	i = 0; 
	while (i < GLIMMER_ENCODING_LAST) {
		if (strcmp (charset, encodings[i].charset) == 0)
			return &encodings[i];
		i++;
	}

	return NULL;
}

const GlimmerEncoding *
glimmer_encoding_get_from_index (gint index)
{
	g_return_val_if_fail (index >= 0, NULL);

	if (index >= GLIMMER_ENCODING_LAST)
		return NULL;

	glimmer_encoding_lazy_init ();

	return &encodings[index];
}


gchar *
glimmer_encoding_to_string (const GlimmerEncoding* enc)
{
	g_return_val_if_fail (enc != NULL, NULL);
	g_return_val_if_fail (enc->name != NULL, NULL);
	g_return_val_if_fail (enc->charset != NULL, NULL);

	glimmer_encoding_lazy_init ();

	return g_strdup_printf ("%s (%s)", enc->name, enc->charset);
}

const gchar *
glimmer_encoding_get_charset (const GlimmerEncoding* enc)
{
	g_return_val_if_fail (enc != NULL, NULL);
	g_return_val_if_fail (enc->charset != NULL, NULL);

	glimmer_encoding_lazy_init ();

	return enc->charset;
}
