/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  glimmer-commands.c
 *
 *  Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <gconf/gconf-client.h>
#include <libgnome/gnome-i18n.h>
#include <gtksourceview/gtksourceiter.h>
#include "glimmer-commands.h"
#include "glimmer-print.h"
#include "glimmer-settings.h"
#include "glimmer-utils.h"
#include "dialogs/glimmer-dialogs.h"

void
undo_cb (BonoboUIComponent *component,
	 GlimmerView       *view,
	 const gchar       *verb)
{
	GtkSourceBuffer *buffer;

	g_return_if_fail (GLIMMER_IS_VIEW (view));

	buffer = GTK_SOURCE_BUFFER (GTK_TEXT_VIEW (view)->buffer);

	if (gtk_source_buffer_can_undo (buffer))
		gtk_source_buffer_undo (buffer);
}

void
redo_cb (BonoboUIComponent *component,
	 GlimmerView       *view,
	 const gchar       *verb)
{
	GtkSourceBuffer *buffer;

	g_return_if_fail (GLIMMER_IS_VIEW (view));

	buffer = GTK_SOURCE_BUFFER (GTK_TEXT_VIEW (view)->buffer);

	if (gtk_source_buffer_can_redo (buffer))
		gtk_source_buffer_redo (buffer);
}

void
cut_cb (BonoboUIComponent *component,
	GlimmerView       *view,
	const gchar       *verb)
{
	GtkTextBuffer *buffer;

	g_return_if_fail (GLIMMER_IS_VIEW (view));

	buffer = GTK_TEXT_VIEW (view)->buffer;

	gtk_text_buffer_cut_clipboard (buffer, gtk_clipboard_get (GDK_NONE),
				       GTK_TEXT_VIEW (view)->editable);
	gtk_text_view_scroll_mark_onscreen (GTK_TEXT_VIEW (view),
					    gtk_text_buffer_get_insert (buffer));
}

void
copy_cb (BonoboUIComponent *component,
	 GlimmerView       *view,
	 const gchar       *verb)
{
	GtkTextBuffer *buffer;

	g_return_if_fail (GLIMMER_IS_VIEW (view));

	buffer = GTK_TEXT_VIEW (view)->buffer;

	gtk_text_buffer_copy_clipboard (buffer, gtk_clipboard_get (GDK_NONE));
	gtk_text_view_scroll_mark_onscreen (GTK_TEXT_VIEW (view),
					    gtk_text_buffer_get_insert (buffer));
}

void
paste_cb (BonoboUIComponent *component,
	  GlimmerView       *view,
	  const gchar       *verb)
{
	GtkTextBuffer *buffer;

	g_return_if_fail (GLIMMER_IS_VIEW (view));

	buffer = GTK_TEXT_VIEW (view)->buffer;

	gtk_text_buffer_paste_clipboard (buffer, gtk_clipboard_get (GDK_NONE), NULL,
					 GTK_TEXT_VIEW (view)->editable);
	gtk_text_view_scroll_mark_onscreen (GTK_TEXT_VIEW (view),
					    gtk_text_buffer_get_insert (buffer));
}

void
select_all_cb (BonoboUIComponent *component,
	       GlimmerView       *view,
	       const gchar       *verb)
{
	GtkTextBuffer *buffer;
	GtkTextIter start;
	GtkTextIter end;

	g_return_if_fail (GLIMMER_IS_VIEW (view));

	buffer = GTK_TEXT_VIEW (view)->buffer;

	gtk_text_buffer_get_start_iter (buffer, &start);
	gtk_text_buffer_get_end_iter (buffer, &end);

	gtk_text_buffer_place_cursor (buffer, &start);

	gtk_text_buffer_move_mark_by_name (buffer,
					   "selection_bound",
					   &end);
}

void
preferences_cb (BonoboUIComponent *component,
		GlimmerView       *view,
		const gchar       *verb)
{
	g_return_if_fail (GLIMMER_IS_VIEW (view));

	glimmer_dialog_preferences (view);
}

void
page_setup_cb (BonoboUIComponent *component,
	       GlimmerView       *view,
	       const gchar       *verb)
{
	g_return_if_fail (GLIMMER_IS_VIEW (view));

	glimmer_dialog_page_setup (view);
}

void
print_preview_cb (BonoboUIComponent *component,
		  GlimmerView       *view,
		  const gchar       *verb)
{
	g_return_if_fail (GLIMMER_IS_VIEW (view));

	glimmer_print_preview (view);
}

void
print_cb (BonoboUIComponent *component,
	  GlimmerView       *view,
	  const gchar       *verb)
{
	g_return_if_fail (GLIMMER_IS_VIEW (view));

	glimmer_print (view);
}

void
find_cb (BonoboUIComponent *component,
	 GlimmerView       *view,
	 const gchar       *verb)
{
	GSList *history_list;
	const char *sensitivity;

	g_return_if_fail (GLIMMER_IS_VIEW (view));

	glimmer_dialog_find (view);

	history_list = g_object_get_data (G_OBJECT (view), "find-history");
	if (g_slist_length (history_list) > 0)
		sensitivity = "1";
	else
		sensitivity = "0";

	bonobo_ui_component_set_prop (component, "/commands/SearchFindNext",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (component, "/commands/SearchFindPrevious",
				      "sensitive", sensitivity, NULL);
}

void
find_next_cb (BonoboUIComponent *component,
	      GlimmerView       *view,
	      const gchar       *verb)
{
	GtkTextBuffer *buffer = GTK_TEXT_VIEW (view)->buffer;
	GConfClient *client;
	GSList *history_list;
	gchar *search_string;
	gboolean match_case, match_word, wrap_around, found;
	GtkTextIter iter, wrap, match_start, match_end;
	GtkTextSearchFlags search_flags;
	GtkWidget *window, *message_dlg;

	g_return_if_fail (GLIMMER_IS_VIEW (view));

	/* The top item in the find history is the one we want. */
	history_list = g_object_get_data (G_OBJECT (view), "find-history");
	search_string = history_list->data;

	if (strlen (search_string) <= 0)
		return;

	/* Get the find settings from gconf. */
	client = gconf_client_get_default ();
	match_case = gconf_client_get_bool (client,
					    GLIMMER_BASE_KEY GLIMMER_SETTING_FIND_MATCH_CASE,
					    NULL);
	match_word = gconf_client_get_bool (client,
					    GLIMMER_BASE_KEY GLIMMER_SETTING_FIND_MATCH_WORD,
					    NULL);
	wrap_around = gconf_client_get_bool (client,
					     GLIMMER_BASE_KEY GLIMMER_SETTING_FIND_WRAP_AROUND,
					     NULL);
	g_object_unref (G_OBJECT (client));

	search_flags = GTK_TEXT_SEARCH_VISIBLE_ONLY | GTK_TEXT_SEARCH_TEXT_ONLY;
	if (!match_case)
		search_flags |= GTK_TEXT_SEARCH_CASE_INSENSITIVE;

	gtk_text_buffer_get_iter_at_mark (buffer, &iter,
					  gtk_text_buffer_get_mark (buffer,
								    "selection_bound"));

	found = gtk_source_iter_forward_search (&iter, search_string,
						search_flags, &match_start,
						&match_end, NULL);
	if (found && match_word) {
		found = gtk_text_iter_starts_word (&match_start) &&
			gtk_text_iter_ends_word (&match_end);
	}

	if (!found && wrap_around) {
		gtk_text_buffer_get_start_iter (buffer, &wrap);
		found = gtk_source_iter_forward_search (&wrap, search_string,
							search_flags, &match_start,
							&match_end, &iter);
	}

	if (found) {
		/* Highlight text found. */
		gtk_text_buffer_place_cursor (buffer, &match_start);

		gtk_text_buffer_move_mark_by_name (buffer,
						   "selection_bound",
						   &match_end);

		/* Scroll to text. */
		gtk_text_view_scroll_to_mark (GTK_TEXT_VIEW (view),
					      gtk_text_buffer_get_insert (buffer),
					      0.0, TRUE, 0.5, 0.5);
	}

	if (!found) {
		/* Unable to get toplevel GtkWindow if part of out-of-process Bonobo component. */
		window = gtk_widget_get_toplevel (GTK_WIDGET (view));
		if (!GTK_WIDGET_TOPLEVEL (window))
			window = NULL;

		message_dlg = gtk_message_dialog_new (
				GTK_WINDOW (window),
				GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_INFO,
				GTK_BUTTONS_OK,
				_("The text \"%s\" was not found."), search_string);
		gtk_dialog_set_default_response (GTK_DIALOG (message_dlg), GTK_RESPONSE_OK);
		gtk_window_set_resizable (GTK_WINDOW (message_dlg), FALSE);

		gtk_dialog_run (GTK_DIALOG (message_dlg));
		gtk_widget_destroy (message_dlg);
	}
}

void
find_previous_cb (BonoboUIComponent *component,
		  GlimmerView       *view,
		  const gchar       *verb)
{
	GtkTextBuffer *buffer = GTK_TEXT_VIEW (view)->buffer;
	GConfClient *client;
	GSList *history_list;
	gchar *search_string;
	gboolean match_case, match_word, wrap_around, found;
	GtkTextIter iter, wrap, match_start, match_end;
	GtkTextSearchFlags search_flags;
	GtkWidget *window, *message_dlg;

	g_return_if_fail (GLIMMER_IS_VIEW (view));

	/* The top item in the find history is the one we want. */
	history_list = g_object_get_data (G_OBJECT (view), "find-history");
	search_string = history_list->data;

	if (strlen (search_string) <= 0)
		return;

	/* Get the find settings from gconf. */
	client = gconf_client_get_default ();
	match_case = gconf_client_get_bool (client,
					    GLIMMER_BASE_KEY GLIMMER_SETTING_FIND_MATCH_CASE,
					    NULL);
	match_word = gconf_client_get_bool (client,
					    GLIMMER_BASE_KEY GLIMMER_SETTING_FIND_MATCH_WORD,
					    NULL);
	wrap_around = gconf_client_get_bool (client,
					     GLIMMER_BASE_KEY GLIMMER_SETTING_FIND_WRAP_AROUND,
					     NULL);
	g_object_unref (G_OBJECT (client));

	search_flags = GTK_TEXT_SEARCH_VISIBLE_ONLY | GTK_TEXT_SEARCH_TEXT_ONLY;
	if (!match_case)
		search_flags |= GTK_TEXT_SEARCH_CASE_INSENSITIVE;

	gtk_text_buffer_get_iter_at_mark (buffer, &iter,
					  gtk_text_buffer_get_insert (buffer));

	found = gtk_source_iter_backward_search (&iter, search_string,
						 search_flags, &match_start,
						 &match_end, NULL);
	if (found && match_word) {
		found = gtk_text_iter_starts_word (&match_start) &&
			gtk_text_iter_ends_word (&match_end);
	}

	if (!found && wrap_around) {
		gtk_text_buffer_get_end_iter (buffer, &wrap);
		found = gtk_source_iter_backward_search (&wrap, search_string,
							 search_flags, &match_start,
							 &match_end, &iter);
	}

	if (found) {
		/* Highlight text found. */
		gtk_text_buffer_place_cursor (buffer, &match_start);

		gtk_text_buffer_move_mark_by_name (buffer,
						   "selection_bound",
						   &match_end);

		/* Scroll to text. */
		gtk_text_view_scroll_to_mark (GTK_TEXT_VIEW (view),
					      gtk_text_buffer_get_insert (buffer),
					      0.0, TRUE, 0.5, 0.5);
	}

	if (!found) {
		/* Unable to get toplevel GtkWindow if part of out-of-process Bonobo component. */
		window = gtk_widget_get_toplevel (GTK_WIDGET (view));
		if (!GTK_WIDGET_TOPLEVEL (window))
			window = NULL;

		message_dlg = gtk_message_dialog_new (
				GTK_WINDOW (window),
				GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_INFO,
				GTK_BUTTONS_OK,
				_("The text \"%s\" was not found."), search_string);
		gtk_dialog_set_default_response (GTK_DIALOG (message_dlg), GTK_RESPONSE_OK);
		gtk_window_set_resizable (GTK_WINDOW (message_dlg), FALSE);

		gtk_dialog_run (GTK_DIALOG (message_dlg));
		gtk_widget_destroy (message_dlg);
	}
}

void
replace_cb (BonoboUIComponent *component,
	    GlimmerView       *view,
	    const gchar       *verb)
{
	g_return_if_fail (GLIMMER_IS_VIEW (view));

	glimmer_dialog_replace (view);
}

void
goto_line_cb (BonoboUIComponent *component,
	      GlimmerView       *view,
	      const gchar       *verb)
{
	g_return_if_fail (GLIMMER_IS_VIEW (view));

	glimmer_dialog_goto_line (view);
}
