/*  glimmer-style-cache.c
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include <gconf/gconf-client.h>
#include "glimmer-file-settings.h"
#include "glimmer-file-utils.h"
#include "glimmer-style-cache.h"
#include "glimmer-style-styles.h"

static GHashTable *style_cache = NULL;

static void
glimmer_style_cache_modify_tag_internal (GtkTextTag   *tag,
					 GlimmerStyle *style)
{
	GValue italic = { 0, };
	GValue bold = { 0, };
	GValue foreground = { 0, };
	GValue background = { 0, };

	/* Foreground color. */
	g_value_init (&foreground, GDK_TYPE_COLOR);
	g_value_set_boxed (&foreground, &style->foreground);
	g_object_set_property (G_OBJECT (tag), "foreground_gdk", &foreground);

	/* Background color. */
	g_value_init (&background, GDK_TYPE_COLOR);
	g_value_set_boxed (&background, &style->background);
	g_object_set_property (G_OBJECT (tag), "background_gdk", &background);

	/* Bold setting. */
	g_value_init (&italic, PANGO_TYPE_STYLE);
	g_value_set_enum (&italic, style->italic ? PANGO_STYLE_ITALIC : PANGO_STYLE_NORMAL);
	g_object_set_property (G_OBJECT (tag), "style", &italic);

	/* Italic setting. */
	g_value_init (&bold, G_TYPE_INT);
	g_value_set_int (&bold, style->bold ? PANGO_WEIGHT_BOLD : PANGO_WEIGHT_NORMAL);
	g_object_set_property (G_OBJECT (tag), "weight", &bold);
}

/* ----------------------------------------------------------------------
 * Public interface 
 * ---------------------------------------------------------------------- */

gboolean
glimmer_style_cache_init ()
{
	gboolean was = TRUE;
	int i;
	GConfClient *client;
	const char *theme;

	if (!style_cache) {
		style_cache = g_hash_table_new (g_str_hash, g_str_equal);

		/* Create GlimmerStyles. */
		for (i = 0; style_strings[i]; i++) {
			GlimmerStyle *style = g_new0 (GlimmerStyle, 1);
			style->modified = FALSE;
			g_hash_table_insert (style_cache, (char *)style_strings[i], style);
		}

		client = gconf_client_get_default ();
		theme = gconf_client_get_string (client,
						 GLIMMER_BASE_KEY GLIMMER_SETTING_COLOR_THEME,
						 NULL);
		g_object_unref (client);

		/* Load theme. */
		glimmer_style_cache_load_theme (theme);

		was = FALSE;
	}

	return was;
}

void
glimmer_style_cache_load_theme (const char *theme)
{
	GConfClient *client;
	int i;
	GlimmerStyle *style;
	char *key, *value;

	client = gconf_client_get_default ();

	for (i = 0; style_strings[i]; i++) {
		style = g_hash_table_lookup (style_cache, style_strings[i]);

		/* Foreground color. */
		key = g_strconcat (GLIMMER_BASE_KEY,
				   GLIMMER_THEMES_KEY,
				   "/", theme, "/",
				   style_strings[i],
				   "_foreground",
				   NULL);
		value = gconf_client_get_string (client, key, NULL);
		glimmer_style_set_color (value, &style->foreground);
		g_free (key);

		/* Background color. */
		key = g_strconcat (GLIMMER_BASE_KEY,
				   GLIMMER_THEMES_KEY,
				   "/", theme, "/",
				   style_strings[i],
				   "_background",
				   NULL);
		value = gconf_client_get_string (client, key, NULL);
		glimmer_style_set_color (value, &style->background);
		g_free (key);

		/* Italic setting. */
		key = g_strconcat (GLIMMER_BASE_KEY,
				   GLIMMER_THEMES_KEY,
				   "/", theme, "/",
				   style_strings[i],
				   "_italic",
				   NULL);
		style->italic = gconf_client_get_bool (client, key, NULL);
		g_free (key);

		/* Bold setting. */
		key = g_strconcat (GLIMMER_BASE_KEY,
				   GLIMMER_THEMES_KEY,
				   "/", theme, "/",
				   style_strings[i],
				   "_bold",
				   NULL);
		style->bold = gconf_client_get_bool (client, key, NULL);
		g_free (key);

		style->modified = TRUE;
	}

	g_object_unref (client);
}

GlimmerStyle *
glimmer_style_cache_get_style_from_class (const char *klass)
{
	return g_hash_table_lookup (style_cache, klass);
}

gboolean
glimmer_style_cache_update_tag (GtkTextTag *tag)
{
	GlimmerStyle *style = NULL;
	gboolean modified = FALSE;

	style = g_object_get_data (G_OBJECT (tag), "GlimmerStyle");
	if (style && style->modified) {
		glimmer_style_cache_modify_tag_internal (tag, style);
		modified = TRUE;
	}

	return modified;
}

gboolean
glimmer_style_cache_modify_tag (GtkTextTag   *tag,
				GlimmerStyle *style)
{
	gboolean modified = FALSE;

	if (style) {
		glimmer_style_cache_modify_tag_internal (tag, style);
		g_object_set_data (G_OBJECT (tag), "GlimmerStyle", style);
		modified = TRUE;
	}

	return modified;
}
