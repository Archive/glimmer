/*  glimmer-style-cache.h
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GLIMMER_STYLE_CACHE_H_
#define _GLIMMER_STYLE_CACHE_H_

#include <glib.h>
#include <gdk/gdk.h>
#include <pango/pango-font.h>
#include <gtk/gtktexttag.h>

G_BEGIN_DECLS

typedef struct _GlimmerStyle GlimmerStyle;

struct _GlimmerStyle {
	GdkColor foreground;
	GdkColor background;
	gboolean italic;
	gboolean bold;
	guint modified : 1;
};

gboolean      glimmer_style_cache_init (void);
void          glimmer_style_cache_load_theme (const char *theme);

GlimmerStyle *glimmer_style_cache_get_style_from_class (const char *klass);

gboolean      glimmer_style_cache_update_tag (GtkTextTag *tag);
gboolean      glimmer_style_cache_modify_tag (GtkTextTag   *tag,
					      GlimmerStyle *style);

G_END_DECLS

#endif /* _GLIMMER_STYLE_CACHE_H_ */
