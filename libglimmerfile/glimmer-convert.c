/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *  glimmer-convert.c
 *
 *  Copyright (C) 2003 - Paolo Maggi
 *                       Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
/*
 * Copied from the gedit project. Original author Paolo Maggi.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <stdio.h>
#include <gconf/gconf-client.h>
#include "glimmer-convert.h"
#include "glimmer-encodings.h"
#include "glimmer-settings.h"

static gchar *
glimmer_convert_to_utf8_from_charset (const gchar *content,
				      gsize        len,
				      const gchar *charset)
{
	gchar *utf8_content = NULL;
	GError *conv_error = NULL;
	gchar* converted_contents = NULL;
	gsize bytes_written;

	g_return_val_if_fail (content != NULL, NULL);

	converted_contents = g_convert (content, len, "UTF-8",
					charset, NULL, &bytes_written,
					&conv_error); 

	if ((conv_error != NULL) || 
	    !g_utf8_validate (converted_contents, bytes_written, NULL)) {
		if (converted_contents != NULL)
			g_free (converted_contents);

		if (conv_error != NULL) {
			g_error_free (conv_error);
			conv_error = NULL;
		}

		utf8_content = NULL;
	} else {
		utf8_content = converted_contents;
	}

	return utf8_content;
}

GSList *
glimmer_convert_get_encodings (void)
{
	GConfClient *client;
	GSList *strings;
	GSList *res = NULL;

	client = gconf_client_get_default ();
	g_assert (client != NULL);

	strings = gconf_client_get_list (client,
					 GLIMMER_BASE_KEY GLIMMER_SETTING_FILE_ENCODINGS,
					 GCONF_VALUE_STRING,
					 NULL);
	g_object_unref (G_OBJECT (client));

	if (strings != NULL) {
		GSList *tmp;
		const GlimmerEncoding *enc;

		tmp = strings;

		while (tmp) {
			const char *charset = tmp->data;

			if (strcmp (charset, "current") == 0)
				g_get_charset (&charset);

			g_return_val_if_fail (charset != NULL, NULL);
			enc = glimmer_encoding_get_from_charset (charset);

			if (enc != NULL)
				res = g_slist_prepend (res, (gpointer)enc);

			tmp = g_slist_next (tmp);
		}

		g_slist_foreach (strings, (GFunc) g_free, NULL);
		g_slist_free (strings);

		res = g_slist_reverse (res);
	}

	return res;
}

gchar *
glimmer_convert_to_utf8 (const gchar *content,
			 gsize        len,
			 gchar      **encoding_used)
{
	GSList *encodings = NULL;
	GSList *start;
	const gchar *locale_charset;

	g_return_val_if_fail (!g_utf8_validate (content, len, NULL), 
			g_strndup (content, len < 0 ? strlen (content) : len));

	encodings = glimmer_convert_get_encodings ();

	if (g_get_charset (&locale_charset) == FALSE) {
		const GlimmerEncoding *locale_encoding;

		/* Not using a UTF-8 locale, so try converting from that first */
		if (locale_charset != NULL) {
			locale_encoding = glimmer_encoding_get_from_charset (locale_charset);
			encodings = g_slist_prepend (encodings, (gpointer)locale_encoding);
		}
	}

	start = encodings;

	while (encodings != NULL) {
		GlimmerEncoding *enc;
		const gchar *charset;
		gchar *utf8_content;

		enc = (GlimmerEncoding *)encodings->data;

		charset = glimmer_encoding_get_charset (enc);

		utf8_content = glimmer_convert_to_utf8_from_charset (content, len, charset);

		if (utf8_content != NULL)  {
			if (encoding_used != NULL) {
				if (*encoding_used != NULL)
					g_free (*encoding_used);

				*encoding_used = g_strdup (charset);
			}

			return utf8_content;
		}

		encodings = encodings->next;
	}

	g_slist_free (start);

	return NULL;
}
