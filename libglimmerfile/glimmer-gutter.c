/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  glimmer-gutter.c
 *
 *  Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, 
 *  Boston, MA 02111-1307, USA. 
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "glimmer-gutter.h"
#include <gtksourceview/gtksourcebuffer.h>

static BonoboObjectClass *parent_class;

static void glimmer_gutter_class_init (GlimmerGutterClass *klass);
static void glimmer_gutter_init (GlimmerGutter *gutter);

static void impl_add_marker (PortableServer_Servant servant,
			     const CORBA_long       line,
			     const CORBA_char      *type,
			     CORBA_Environment     *ev);
static void impl_remove_marker (PortableServer_Servant servant,
				const CORBA_long       line,
				const CORBA_char      *type,
				CORBA_Environment     *ev);
static GNOME_Development_EditorGutter_MarkerList *impl_get_markers (PortableServer_Servant servant,
								    CORBA_Environment      *ev);

/* Private functions. */
static void
glimmer_gutter_class_init (GlimmerGutterClass *klass)
{
	GObjectClass *object_class;
	POA_GNOME_Development_EditorGutter__epv *epv = &klass->epv;

	object_class = G_OBJECT_CLASS (klass);
	parent_class = g_type_class_peek_parent (klass);

	epv->addMarker = impl_add_marker;
	epv->removeMarker = impl_remove_marker;
	epv->getMarkers = impl_get_markers;
}

static void
glimmer_gutter_init (GlimmerGutter *gutter)
{
}

static void
impl_add_marker (PortableServer_Servant servant,
		 const CORBA_long       line,
		 const CORBA_char      *type,
		 CORBA_Environment     *ev)
{
	GlimmerGutter *gutter;
	GtkSourceBuffer *buffer;
	GtkTextIter iter;

	gutter = GLIMMER_GUTTER (bonobo_object_from_servant (servant));
	bonobo_return_if_fail (gutter != NULL, ev);

	buffer = GTK_SOURCE_BUFFER (GTK_TEXT_VIEW (gutter->view)->buffer);
	gtk_text_buffer_get_iter_at_line (GTK_TEXT_BUFFER (buffer), &iter, line);
	gtk_source_buffer_create_marker (buffer, type, "glimmer", &iter);
}

static void
impl_remove_marker (PortableServer_Servant servant,
		    const CORBA_long       line,
		    const CORBA_char      *type,
		    CORBA_Environment     *ev)
{
	GlimmerGutter *gutter;
	GtkSourceBuffer *buffer;
	GtkSourceMarker *marker;

	gutter = GLIMMER_GUTTER (bonobo_object_from_servant (servant));
	bonobo_return_if_fail (gutter != NULL, ev);

	buffer = GTK_SOURCE_BUFFER (GTK_TEXT_VIEW (gutter->view)->buffer);
	marker = gtk_source_buffer_get_marker (buffer, type);
	gtk_source_buffer_delete_marker (buffer, marker);
}

static GNOME_Development_EditorGutter_MarkerList *
impl_get_markers (PortableServer_Servant servant,
		  CORBA_Environment     *ev)
{
#if 0
	GlimmerGutter *gutter;
	GtkSourceBuffer *buffer;
	GList *marker_list, *l;
	GNOME_Development_EditorGutter_MarkerList *list;
	int length = 0;
	int i;

	gutter = GLIMMER_GUTTER (bonobo_object_from_servant (servant));
	bonobo_return_val_if_fail (gutter != NULL, CORBA_OBJECT_NIL, ev);

	g_warning ("The getMarkers method has not been tested and probably doesn't work!\n");

	buffer = GTK_SOURCE_BUFFER (GTK_TEXT_VIEW (gutter->view)->buffer);
	marker_list = gtk_source_buffer_get_all_markers (buffer);
	length = g_list_length (marker_list);

	list = GNOME_Development_EditorGutter_MarkerList__alloc ();
	CORBA_sequence_set_release (list, TRUE);
	list->_buffer = GNOME_Development_EditorGutter_MarkerList_allocbuf (length);
	list->_length = list->_maximum = length;

	for (i = 0, l = marker_list; i < length; i++) {
		GtkSourceBufferMarker *marker = l->data;
		list->_buffer[i].line_num = marker->line;
		list->_buffer[i].type = CORBA_string_dup (marker->name);
		l = l->next;
		g_free (marker->name);
		g_free (marker);
	}
	g_list_free (marker_list);

	return list;
#endif
	return NULL;
}

/* ----------------------------------------------------------------------
 * Public interface 
 * ---------------------------------------------------------------------- */

GlimmerGutter *
glimmer_gutter_new (GlimmerView *view)
{
	GlimmerGutter *gutter;

	g_return_val_if_fail (view != NULL, NULL);
	g_return_val_if_fail (GLIMMER_IS_VIEW (view), NULL);

	gutter = GLIMMER_GUTTER (g_object_new (GLIMMER_TYPE_GUTTER, NULL));

	gutter->view = view;

	return gutter;
}

BONOBO_TYPE_FUNC_FULL (GlimmerGutter,
		       GNOME_Development_EditorGutter,
		       BONOBO_OBJECT_TYPE,
		       glimmer_gutter);
