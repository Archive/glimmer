/*  glimmer-file-search.c
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <libgnome/libgnome.h>
#include <libgnomeui/libgnomeui.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "widgets/gnome-search-dialog.h"
#include "widgets/gnome-goto-line.h"
#include "widgets/gnome-common-funcs.h"
#include "glimmer-file-settings.h"
#include "glimmer-file.h"

#include "glimmer-file-search.h"

static void find_text_cb (GtkWidget *widget, GnomeSearchDialog *search_dialog);
static gboolean find_text (GtkTextBuffer *buffer, GtkTextIter *start, GtkTextIter *end,
			   gchar *search_string, GtkTextIter *fstart, GtkTextIter *fend,
			   gboolean case_sensitive, gboolean is_regex, gboolean select);
static void replace_text_cb (GtkWidget *widget, GnomeSearchDialog *search_dialog);
static void replace_all_text_cb (GtkWidget *widget, GnomeSearchDialog *search_dialog);
static gint replace_all_text (GtkTextBuffer *buffer, GtkTextIter *start, gchar *search_text,
			      gchar *new_text, GtkTextIter *end, gboolean iscase, gboolean regex);
static gboolean replace_text (GtkTextBuffer *buffer, gchar *new_text);
static void find_line_cb (GtkWidget *widget, GnomeGotoLineDialog *gtl);
static void set_find_text_from_selection (GnomeSearchDialog *sd);
static void change_parent_file (GtkWidget *widget, GlimmerFile *file);


void
glimmer_file_search_load_config ()
{
	GConfClient *client;

	client = gconf_client_get_default ();
	g_assert (client != NULL);

	glimmer_file_settings->case_sensitive =
		gconf_client_get_bool (client, BASE_KEY "/case-sensitive", NULL);
	glimmer_file_settings->search_regex =
		gconf_client_get_bool (client, BASE_KEY "/search-regex", NULL);
	glimmer_file_settings->search_start =
		gconf_client_get_bool (client, BASE_KEY "/search-start", NULL);
	glimmer_file_settings->find_keep_open =
		gconf_client_get_bool (client, BASE_KEY "/find-keep-open", NULL);
	glimmer_file_settings->line_keep_open =
		gconf_client_get_bool (client, BASE_KEY "/line-keep-open", NULL);
	glimmer_file_settings->history_items =
		gconf_client_get_int (client, BASE_KEY "/history-items", NULL);
	g_object_unref (G_OBJECT (client));
}

void
glimmer_file_search_save_config ()
{
	GConfClient *client;

	client = gconf_client_get_default ();
	g_assert (client != NULL);

	gconf_client_set_bool (client, BASE_KEY "/case-sensitive",
			       glimmer_file_settings->case_sensitive, NULL);
	gconf_client_set_bool (client, BASE_KEY "/search-regex",
			       glimmer_file_settings->search_regex, NULL);
	gconf_client_set_bool (client, BASE_KEY "/search-start",
			       glimmer_file_settings->search_start, NULL);
	gconf_client_set_bool (client, BASE_KEY "/find-keep-open",
			       glimmer_file_settings->find_keep_open, NULL);
	gconf_client_set_bool (client, BASE_KEY "/line-keep-open",
			       glimmer_file_settings->line_keep_open, NULL);
	gconf_client_set_int (client, BASE_KEY "/history-items",
			      glimmer_file_settings->history_items, NULL);
	g_object_unref (G_OBJECT (client));
}

/*
 *Find (F6)
 *If the file has text in the filename_entry, look for it.
 *Elsewise we need to popup the dialog.
 */
void
glimmer_file_find_cb (BonoboUIComponent *component, GlimmerFile *file, const gchar *verb)
{
	GtkWidget *widget;
	gchar *text;
	GList *history;

#if 0
	if (!file->search_dialog) {
		widget = file->search_dialog = gnome_search_dialog_new (GNOME_SEARCH_FIND);
		gtk_signal_connect (GTK_OBJECT (widget), "destroy",
				    GTK_SIGNAL_FUNC (change_parent_file), file);
		gnome_search_dialog_set_attach_widget (GNOME_SEARCH_DIALOG (widget),
						       GTK_WIDGET (file));
		g_signal_connect (G_OBJECT (GNOME_SEARCH_DIALOG (widget)->find_button), "clicked",
				  (GCallback) find_text_cb, widget);
		g_signal_connect_swapped (G_OBJECT (GNOME_SEARCH_DIALOG (widget)->cancel_button),
					  "clicked", (GCallback) gtk_widget_destroy,
					  G_OBJECT (widget));
		gnome_search_dialog_set_max_entries (GNOME_SEARCH_DIALOG (widget),
						     glimmer_file_settings->history_items);
		history =
			gnome_common_build_glist_from_file ("glimmer-file-find",
							    glimmer_file_settings->history_items);
		gnome_search_dialog_set_find_history (GNOME_SEARCH_DIALOG (widget), history);
		set_find_text_from_selection (GNOME_SEARCH_DIALOG (widget));
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
					      (GNOME_SEARCH_DIALOG (widget)->check_case_sensitive),
					      glimmer_file_settings->case_sensitive);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
					      (GNOME_SEARCH_DIALOG (widget)->check_search_style),
					      glimmer_file_settings->search_regex);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
					      (GNOME_SEARCH_DIALOG (widget)->check_search_start),
					      glimmer_file_settings->search_start);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
					      (GNOME_SEARCH_DIALOG (widget)->check_stay_open),
					      glimmer_file_settings->find_keep_open);
		g_signal_connect (G_OBJECT (GNOME_SEARCH_DIALOG (widget)->check_case_sensitive),
				  "clicked", (GCallback) gnome_common_toggle_button_switch,
				  &glimmer_file_settings->case_sensitive);
		g_signal_connect (G_OBJECT (GNOME_SEARCH_DIALOG (widget)->check_search_style),
				  "clicked", (GCallback) gnome_common_toggle_button_switch,
				  &glimmer_file_settings->search_regex);
		g_signal_connect (G_OBJECT (GNOME_SEARCH_DIALOG (widget)->check_search_start),
				  "clicked", (GCallback) gnome_common_toggle_button_switch,
				  &glimmer_file_settings->search_start);
		g_signal_connect (G_OBJECT (GNOME_SEARCH_DIALOG (widget)->check_stay_open),
				  "clicked", (GCallback) gnome_common_toggle_button_switch,
				  &glimmer_file_settings->find_keep_open);
		gtk_widget_show (widget);
		gtk_widget_grab_focus (GTK_COMBO (GNOME_SEARCH_DIALOG (widget)->find_combo)->entry);
	}
#endif
}

void
glimmer_file_find_next_cb (BonoboUIComponent *component, GlimmerFile *file, const gchar *verb)
{
	GtkTextBuffer *buffer;
	GtkTextMark *mark;
	GtkTextIter start;
	GtkTextIter end;
	GtkTextIter fstart;
	GtkTextIter fend;
	GList *history;
	gchar *search_text;

	history = gnome_common_build_glist_from_file ("glimmer-file-find", 1);
	if (history) {
		search_text = (gchar *) history->data;
		buffer = GTK_TEXT_VIEW (file->view)->buffer;
		mark = gtk_text_buffer_get_insert (buffer);
		gtk_text_buffer_get_iter_at_mark (buffer, &start, mark);
		gtk_text_buffer_get_end_iter (buffer, &end);
		find_text (buffer, &start, &end, search_text, &fstart, &fend,
			   glimmer_file_settings->case_sensitive,
			   glimmer_file_settings->search_regex, TRUE);
		gnome_common_glist_free_all (history);
	}
}

/*
 *Find callback
 */
static void
find_text_cb (GtkWidget *widget, GnomeSearchDialog *search_dialog)
{
	GlimmerFile *file;
	GtkTextBuffer *buffer;
	GtkTextIter pos;
	GtkTextIter temp;
	GtkTextIter fstart;
	GtkTextIter fend;
	gchar *search_string;
	gboolean found;
	gchar text[256];

	file = GLIMMER_FILE (search_dialog->attached);
	buffer = GTK_TEXT_VIEW (file->view)->buffer;

	gtk_text_buffer_get_iter_at_mark (buffer, &pos, gtk_text_buffer_get_insert (buffer));

	search_string = gnome_search_dialog_get_find_text (GNOME_SEARCH_DIALOG (search_dialog));
	search_string = g_strdup (search_string);
	if (glimmer_file_settings->search_start == GNOME_SEARCH_START_TOP)
		gtk_text_buffer_get_start_iter (buffer, &pos);
	gnome_search_dialog_set_startpos (search_dialog, (guint) gtk_text_iter_get_offset (&pos));
	if (!search_dialog->search_selection) {
		gtk_text_buffer_get_end_iter (buffer, &temp);
		gnome_search_dialog_set_endpos (search_dialog,
						(guint) gtk_text_iter_get_offset (&temp));
	}
	gnome_search_dialog_add_find_string (search_dialog, search_string);
	gnome_common_add_string_to_file ("glimmer-file-find", search_string);
	gtk_text_buffer_get_iter_at_offset (buffer, &temp, search_dialog->endpos);
	found = find_text (buffer, &pos, &temp, search_string, &fstart, &fend,
			   glimmer_file_settings->case_sensitive,
			   glimmer_file_settings->search_regex, TRUE);
	if (found) {
		if (gnome_search_dialog_get_dialog_type (search_dialog) == GNOME_SEARCH_FIND
		    && !glimmer_file_settings->find_keep_open) {
			gtk_widget_destroy (GTK_WIDGET (search_dialog));
		}
	} else {
		g_snprintf (text, sizeof (text), "%s was not found.", search_string);
		gnome_ok_dialog (text);
	}
	g_free (search_string);
}

/*
 *Find some text in a buffer with the given settings
 */
static gboolean
find_text (GtkTextBuffer *buffer, GtkTextIter *start, GtkTextIter *end, gchar *search_string,
	   GtkTextIter *fstart, GtkTextIter *fend, gboolean case_sensitive, gboolean is_regex,
	   gboolean select)
{
	//Regex regex;
	GtkTextMark *mark;
	gboolean found = FALSE;
	gchar *str;

	if (is_regex) {
/*
        if(gtk_source_tag_compile_regex(search_string, &regex))
        {
            pos = gtk_extext_regex_search(text, start, &regex, TRUE, match);
            if(pos == -1) pos = 0;
            if(regex.buf.fastmap) g_free(regex.buf.fastmap);
            regex.buf.fastmap = NULL;
            regfree(&regex.buf);
        }
*/
	} else if (case_sensitive) {
		found = gtk_text_iter_forward_search (start, search_string,
						      GTK_TEXT_SEARCH_TEXT_ONLY, fstart, fend, end);
	} else if (!case_sensitive) {
		str = gtk_text_iter_get_slice (start, end);
		if (str) {
			gint s;
			gint e;
			gint i;
			gint length;
			gint segment;

			length = strlen (str);
			segment = strlen (search_string);
			length -= segment;
			*fstart = *start;
			for (i = 0; i <= length; i++) {
				if (!g_ascii_strncasecmp (&str[i], search_string, segment)) {
					*fend = *fstart;
					gtk_text_iter_forward_chars (fend, segment);
					found = TRUE;
					break;
				}
				gtk_text_iter_forward_char (fstart);
			}
			g_free (str);
		}
	}

	if (found && select) {
		mark = gtk_text_buffer_get_selection_bound (buffer);
		gtk_text_buffer_move_mark (buffer, mark, fstart);
		mark = gtk_text_buffer_get_insert (buffer);
		gtk_text_buffer_move_mark (buffer, mark, fend);
	}
	return (found);
}

/* 
 *Replace (F7)
 *If the file has a selection and text in the filename_entry, replace the text.
 *Elsewise we need to popup the dialog.
 */
void
glimmer_file_replace_cb (BonoboUIComponent *component, GlimmerFile *file,
			 const gchar *verb)
{
	GtkWidget *widget;
	GList *history;

#if 0
	if (!file->search_dialog) {
		widget = file->search_dialog = gnome_search_dialog_new (GNOME_SEARCH_REPLACE);
		g_signal_connect (G_OBJECT (widget), "destroy", (GCallback) change_parent_file,
				  file);
		gnome_search_dialog_set_attach_widget (GNOME_SEARCH_DIALOG (widget),
						       GTK_WIDGET (file));
		g_signal_connect (G_OBJECT (GNOME_SEARCH_DIALOG (widget)->find_button), "clicked",
				  (GCallback) find_text_cb, widget);
		g_signal_connect (G_OBJECT (GNOME_SEARCH_DIALOG (widget)->replace_button),
				  "clicked", (GCallback) replace_text_cb, widget);
		g_signal_connect (G_OBJECT (GNOME_SEARCH_DIALOG (widget)->replace_all), "clicked",
				  (GCallback) replace_all_text_cb, widget);
		g_signal_connect_swapped (G_OBJECT (GNOME_SEARCH_DIALOG (widget)->cancel_button),
					  "clicked", (GCallback) gtk_widget_destroy,
					  G_OBJECT (widget));
		gnome_search_dialog_set_max_entries (GNOME_SEARCH_DIALOG (widget),
						     glimmer_file_settings->history_items);
		history =
			gnome_common_build_glist_from_file ("glimmer-file-find",
							    glimmer_file_settings->history_items);
		gnome_search_dialog_set_find_history (GNOME_SEARCH_DIALOG (widget), history);
		history =
			gnome_common_build_glist_from_file ("glimmer-replace-find",
							    glimmer_file_settings->history_items);
		gnome_search_dialog_set_replace_history (GNOME_SEARCH_DIALOG (widget), history);
		set_find_text_from_selection (GNOME_SEARCH_DIALOG (widget));
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
					      (GNOME_SEARCH_DIALOG (widget)->check_case_sensitive),
					      glimmer_file_settings->case_sensitive);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
					      (GNOME_SEARCH_DIALOG (widget)->check_search_style),
					      glimmer_file_settings->search_regex);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
					      (GNOME_SEARCH_DIALOG (widget)->check_search_start),
					      glimmer_file_settings->search_start);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
					      (GNOME_SEARCH_DIALOG (widget)->check_stay_open),
					      glimmer_file_settings->find_keep_open);
		g_signal_connect (G_OBJECT (GNOME_SEARCH_DIALOG (widget)->check_case_sensitive),
				  "clicked", (GCallback) gnome_common_toggle_button_switch,
				  &glimmer_file_settings->case_sensitive);
		g_signal_connect (G_OBJECT (GNOME_SEARCH_DIALOG (widget)->check_search_style),
				  "clicked", (GCallback) gnome_common_toggle_button_switch,
				  &glimmer_file_settings->search_regex);
		g_signal_connect (G_OBJECT (GNOME_SEARCH_DIALOG (widget)->check_search_start),
				  "clicked", (GCallback) gnome_common_toggle_button_switch,
				  &glimmer_file_settings->search_start);
		g_signal_connect (G_OBJECT (GNOME_SEARCH_DIALOG (widget)->check_stay_open),
				  "clicked", (GCallback) gnome_common_toggle_button_switch,
				  &glimmer_file_settings->find_keep_open);
		gtk_widget_show (widget);
		gtk_widget_grab_focus (GTK_COMBO (GNOME_SEARCH_DIALOG (widget)->find_combo)->entry);
	}
#endif
}

/*
 *Replace callback
 */
static void
replace_text_cb (GtkWidget *widget, GnomeSearchDialog *search_dialog)
{
	GlimmerFile *file;
	gchar *find_text;
	gchar *new_text;
	gboolean found = FALSE;

	file = GLIMMER_FILE (search_dialog->attached);
	find_text = gnome_search_dialog_get_find_text (search_dialog);
	new_text = gnome_search_dialog_get_replace_text (search_dialog);
	found = replace_text (GTK_TEXT_VIEW (file->view)->buffer, new_text);
	search_dialog->endpos += (strlen (new_text) - strlen (find_text));
	gnome_search_dialog_add_replace_string (search_dialog,
						gnome_search_dialog_get_replace_text
						(search_dialog));
	gnome_common_add_string_to_file ("glimmer-file-replace",
					 gnome_search_dialog_get_replace_text (search_dialog));
	if (found && !gnome_search_dialog_keep_window_open (search_dialog))
		gtk_widget_destroy (GTK_WIDGET (search_dialog));
	else
		g_signal_emit_by_name (G_OBJECT (search_dialog->find_button), "clicked", NULL);
}

/*
 *Replace All callback
 */
static void
replace_all_text_cb (GtkWidget *widget, GnomeSearchDialog *search_dialog)
{
	GlimmerFile *file;
	GtkTextBuffer *buffer;
	gint replacements;
	gchar *search_text;
	gchar *new_text;
	GtkTextIter start;
	GtkTextIter end;
	gchar text[256];

	file = GLIMMER_FILE (search_dialog->attached);
	buffer = GTK_TEXT_VIEW (file->view)->buffer;
	search_text = gnome_search_dialog_get_find_text (search_dialog);
	new_text = gnome_search_dialog_get_replace_text (search_dialog);
	if (search_dialog->search_selection) {
		gtk_text_buffer_get_iter_at_offset (buffer, &start, search_dialog->startpos);
		gtk_text_buffer_get_iter_at_offset (buffer, &end, search_dialog->endpos);
	} else {
		gtk_text_buffer_get_iter_at_mark (buffer, &start,
						  gtk_text_buffer_get_insert (buffer));
		gtk_text_buffer_get_end_iter (buffer, &end);
	}
	if (glimmer_file_settings->search_start == GNOME_SEARCH_START_TOP
	    && !search_dialog->search_selection)
		gtk_text_buffer_get_start_iter (buffer, &start);

	replacements =
		replace_all_text (GTK_TEXT_VIEW (file->view)->buffer, &start, search_text, new_text,
				  &end, glimmer_file_settings->case_sensitive,
				  glimmer_file_settings->search_regex);
	if (replacements > 0) {
		gnome_search_dialog_add_find_string (search_dialog,
						     gnome_search_dialog_get_find_text
						     (search_dialog));
		gnome_common_add_string_to_file ("glimmer-file-find",
						 gnome_search_dialog_get_find_text (search_dialog));
		gnome_search_dialog_add_replace_string (search_dialog,
							gnome_search_dialog_get_replace_text
							(search_dialog));
		gnome_common_add_string_to_file ("glimmer-file-replace",
						 gnome_search_dialog_get_replace_text
						 (search_dialog));
		if (!glimmer_file_settings->find_keep_open)
			gtk_widget_destroy (GTK_WIDGET (search_dialog));
		g_snprintf (text, sizeof (text), "%d replacements made.", replacements);
		gnome_ok_dialog (text);
	} else {
		g_snprintf (text, sizeof (text), "%s was not found.", search_text);
		gnome_ok_dialog (text);
	}
}

static gint
replace_all_text (GtkTextBuffer *buffer, GtkTextIter *start, gchar *search_text,
		  gchar *new_text, GtkTextIter *end, gboolean iscase, gboolean regex)
{
	GtkTextIter fstart;
	GtkTextIter fend;
	gboolean found;
	gint replacements = 0;
	gint change;
	gint start_offset;
	gint end_offset;

	start_offset = gtk_text_iter_get_offset (start);
	end_offset = gtk_text_iter_get_offset (end);
	do {
		found = find_text (buffer, start, end, search_text, &fstart, &fend, iscase, regex,
				   TRUE);
		if (found) {
			start_offset = gtk_text_iter_get_offset (&fstart);
			replace_text (buffer, new_text);
			change = new_text ? strlen (new_text) -
				strlen (search_text) : strlen (search_text);
			end_offset += change;
			g_print ("end position changed to %d\n", end_offset);
			gtk_text_buffer_get_iter_at_offset (buffer, start, start_offset);
			gtk_text_buffer_get_iter_at_offset (buffer, end, end_offset);
			replacements++;
		}
	} while (found);
	return (replacements);
}

/*
 *Replace the current selection in editable with replace_text
 */
static gboolean
replace_text (GtkTextBuffer *buffer, gchar *new_text)
{
	GtkTextIter start;
	GtkTextIter end;
	gboolean retval = FALSE;

	if (gtk_text_buffer_get_selection_bounds (buffer, &start, &end)) {
		gtk_text_buffer_delete (buffer, &start, &end);
		gtk_text_buffer_place_cursor (buffer, &start);
		if (new_text && strlen (new_text) > 0)
			gtk_text_buffer_insert (buffer, &start, new_text, strlen (new_text));
		retval = TRUE;
	}
	return (retval);
}

/*
 *Lets hop down to some line in the file.
 */
void
glimmer_file_goto_line_cb (BonoboUIComponent *component, GlimmerFile *file,
			   const gchar *verb)
{
	GtkWidget *widget;
	gchar *line_text = NULL;
	gint line_num;
	GList *history;

#if 0
	if (!file->search_dialog) {
		widget = file->search_dialog = gnome_goto_line_dialog_new ();
		g_signal_connect (G_OBJECT (widget), "destroy", (GCallback) change_parent_file,
				  file);
		gnome_goto_line_dialog_set_attach_widget (GNOME_GOTO_LINE_DIALOG (widget),
							  GTK_WIDGET (file));
		g_signal_connect (G_OBJECT (GNOME_GOTO_LINE_DIALOG (widget)->goto_button),
				  "clicked", (GCallback) find_line_cb, widget);
		g_signal_connect_swapped (GTK_OBJECT
					  (GNOME_GOTO_LINE_DIALOG (widget)->cancel_button),
					  "clicked", (GCallback) gtk_widget_destroy,
					  GTK_OBJECT (widget));
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
					      (GNOME_GOTO_LINE_DIALOG (widget)->check_stay_open),
					      glimmer_file_settings->line_keep_open);
		g_signal_connect (G_OBJECT (GNOME_GOTO_LINE_DIALOG (widget)->check_stay_open),
				  "clicked", (GCallback) gnome_common_toggle_button_switch,
				  &glimmer_file_settings->line_keep_open);
		history =
			gnome_common_build_glist_from_file ("glimmer-file-line",
							    glimmer_file_settings->history_items);
		if (history)
			gnome_goto_line_dialog_set_history (GNOME_GOTO_LINE_DIALOG (widget),
							    history);
		gtk_widget_show (widget);
		gtk_widget_grab_focus (GNOME_GOTO_LINE_DIALOG (widget)->line_entry);
	}
#endif
}

static void
find_line_cb (GtkWidget *widget, GnomeGotoLineDialog *gtl)
{
	GtkTextBuffer *buffer;
	gchar *text = NULL;
	gint line_num;
	GtkTextIter iter;

	buffer = GTK_TEXT_VIEW (GLIMMER_FILE (gtl->attached)->view)->buffer;
	line_num = gnome_goto_line_dialog_get_line (gtl);
	if (line_num > -1 && line_num <= gtk_text_buffer_get_line_count (buffer)) {
		text = (gchar *) gtk_entry_get_text (GTK_ENTRY (gtl->line_entry));
		gtk_text_iter_set_line (&iter, line_num - 1);
		gtk_text_buffer_place_cursor (buffer, &iter);
		gtk_text_view_place_cursor_onscreen (GTK_TEXT_VIEW
						     (GLIMMER_FILE (gtl->attached)->view));
		gnome_goto_line_dialog_add_search_string (gtl, text);
		gnome_common_add_string_to_file ("glimmer-file-line", text);
		if (!glimmer_file_settings->line_keep_open)
			gtk_widget_destroy (GTK_WIDGET (gtl));
	}
}

/* Utility functions */

static void
set_find_text_from_selection (GnomeSearchDialog *sd)
{
	GtkTextBuffer *buffer;
	GtkTextIter start;
	GtkTextIter end;
	gchar *sel_text;
	gint start_offset;
	gint end_offset;

	buffer = GTK_TEXT_VIEW (GLIMMER_FILE (sd->attached)->view)->buffer;

	if (gtk_text_buffer_get_selection_bounds (buffer, &start, &end)) {
		start_offset = gtk_text_iter_get_offset (&start);
		end_offset = gtk_text_iter_get_offset (&end);
		if ((end_offset - start_offset) <= 48) {
			sel_text = gtk_text_iter_get_slice (&start, &end);
			gnome_search_dialog_set_find_text (sd, sel_text);
			gnome_search_dialog_search_selection (sd, FALSE);
			g_free (sel_text);
		} else {
			gtk_text_buffer_place_cursor (buffer, &start);
			gnome_search_dialog_set_startpos (sd, (guint) start_offset);
			gnome_search_dialog_set_endpos (sd, end_offset);
			gnome_search_dialog_search_selection (sd, TRUE);
		}
	} else {
		gnome_search_dialog_search_selection (sd, FALSE);
	}
}

void
glimmer_file_match_bracket_cb (BonoboUIComponent *component, GlimmerFile *file,
			       const gchar *verb)
{
/*
   GtkExText *text;
   gchar cur_char;
   gint start = 0;

   text = GTK_EXTEXT(file->text);
   start = text->current_pos - 1;
   cur_char = GTK_EXTEXT_INDEX(text, start);
   if(!strchr("<{[()]}>", cur_char)) return;
   if(glimmer_file_find_bracket_match(file, &start))
      gtk_extext_set_position(text, start+1);
*/
}

static void
change_parent_file (GtkWidget *widget, GlimmerFile *file)
{
	/*file->search_dialog = NULL;*/
	glimmer_file_search_save_config ();
}
