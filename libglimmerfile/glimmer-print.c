/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *  glimmer-encodings.c
 *
 *  Copyright (C) 2000, 2001 - Chema Celorio, Paolo Maggi
 *  Copyright (C) 2002 - Paolo Maggi  
 *  Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
/*
 * Copied from the gedit project. Original author Paolo Maggi.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <eel/eel-string.h>
#include <gconf/gconf-client.h>
#include <gtksourceview/gtksourceprintjob.h>
#include <libgnome/libgnome.h>
#include <libgnomeprintui/gnome-print-dialog.h>
#include <libgnomeprintui/gnome-print-job-preview.h>
#include "glimmer-print.h"
#include "glimmer-settings.h"
#include "glimmer-utils.h"
#include "glimmer-view.h"

enum {
	PREVIEW_NO,
	PREVIEW,
	PREVIEW_FROM_DIALOG
};

typedef struct _GlimmerPrintJobInfo	GlimmerPrintJobInfo;

struct _GlimmerPrintJobInfo {
	GlimmerView       *view;
	GtkSourceBuffer   *buffer;
	
	GtkSourcePrintJob *pjob;
		
	gint               preview;

	gint               range_type;

	gint               first_line_to_print;
	gint               last_line_to_print;

	/* Popup dialog */
	GtkWidget	  *dialog;
	GtkWidget         *label;
	GtkWidget         *progressbar;
};

static GlimmerPrintJobInfo* glimmer_print_job_info_new 	(GlimmerView         *view);
static void glimmer_print_job_info_destroy		(GlimmerPrintJobInfo *pji, 
							 gboolean             save_config);
static void glimmer_print_real 				(GlimmerPrintJobInfo *pji, 
							 GtkTextIter         *start, 
							 GtkTextIter         *end);
static void glimmer_print_preview_real 			(GlimmerPrintJobInfo *pji, 
							 GtkTextIter         *start, 
							 GtkTextIter         *end);

static void
glimmer_print_job_info_destroy (GlimmerPrintJobInfo *pji,
				gboolean save_config)
{
	g_return_if_fail (pji != NULL);

	if (pji->pjob != NULL)
		g_object_unref (pji->pjob);

	g_free (pji);
}

static GtkWidget *
get_print_dialog (GlimmerPrintJobInfo *pji)
{
	GtkTextIter iter, sel_bound;
	GtkWidget *dialog;
	gint selection_flag;
	gint lines;
	GnomePrintConfig *config;
	GtkWidget *window;

	g_return_val_if_fail (pji != NULL, NULL);
	g_return_val_if_fail (pji->pjob != NULL, NULL);
	
	gtk_text_buffer_get_iter_at_mark (GTK_TEXT_BUFFER (pji->buffer), &iter,
		gtk_text_buffer_get_insert (GTK_TEXT_BUFFER (pji->buffer)));
	gtk_text_buffer_get_iter_at_mark (GTK_TEXT_BUFFER (pji->buffer), &sel_bound,
		gtk_text_buffer_get_selection_bound (GTK_TEXT_BUFFER (pji->buffer)));
	gtk_text_iter_order (&iter, &sel_bound);

	if (gtk_text_iter_equal (&sel_bound, &iter))	
		selection_flag = GNOME_PRINT_RANGE_SELECTION_UNSENSITIVE;
	else
		selection_flag = GNOME_PRINT_RANGE_SELECTION;
	
	config = gtk_source_print_job_get_config (pji->pjob);
	
	dialog = g_object_new (GNOME_TYPE_PRINT_DIALOG, "print_config", config, NULL);
	
	gnome_print_dialog_construct (GNOME_PRINT_DIALOG (dialog), _("Print"),
			              GNOME_PRINT_DIALOG_RANGE | GNOME_PRINT_DIALOG_COPIES);
	
	lines = gtk_text_buffer_get_line_count (GTK_TEXT_BUFFER (pji->buffer));

	gnome_print_dialog_construct_range_page (GNOME_PRINT_DIALOG (dialog),
						 GNOME_PRINT_RANGE_ALL |
						 GNOME_PRINT_RANGE_RANGE |
						 selection_flag,
						 1, lines, "A", _("Lines"));

	window = gtk_widget_get_toplevel (GTK_WIDGET (pji->view));
	if (GTK_WIDGET_TOPLEVEL (window))
		gtk_window_set_transient_for (GTK_WINDOW (dialog),
					      GTK_WINDOW (window));

	gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
	gtk_window_set_modal (GTK_WINDOW (dialog), TRUE); 
	gtk_window_set_destroy_with_parent (GTK_WINDOW (dialog), TRUE);

	return dialog;
}

static void
glimmer_print_dialog_response (GtkWidget *dialog,
			       int response,
			       GlimmerPrintJobInfo *pji)
{
	GtkTextIter start, end;
	gint line_start, line_end;
	
	pji->range_type = gnome_print_dialog_get_range (GNOME_PRINT_DIALOG (dialog));
	gtk_text_buffer_get_bounds (GTK_TEXT_BUFFER (pji->buffer), &start, &end);

	switch (pji->range_type) {
		case GNOME_PRINT_RANGE_ALL:
			break;
		case GNOME_PRINT_RANGE_SELECTION:
			gtk_text_buffer_get_iter_at_mark (GTK_TEXT_BUFFER (pji->buffer), &start,
				gtk_text_buffer_get_insert (GTK_TEXT_BUFFER (pji->buffer)));
			gtk_text_buffer_get_iter_at_mark (GTK_TEXT_BUFFER (pji->buffer), &end,
				gtk_text_buffer_get_selection_bound (GTK_TEXT_BUFFER (pji->buffer)));
			gtk_text_iter_order (&start, &end);
			break;
		case GNOME_PRINT_RANGE_RANGE:
			gnome_print_dialog_get_range_page (GNOME_PRINT_DIALOG (dialog),
							   &line_start, &line_end);
			gtk_text_iter_set_line (&start, line_start - 1);
			gtk_text_iter_set_line (&end, line_end - 1);
			gtk_text_iter_forward_to_line_end (&end);
			break;
		default:
			g_return_if_fail (FALSE);
	}

	switch (response) {
		case GNOME_PRINT_DIALOG_RESPONSE_PRINT:
			pji->preview = PREVIEW_NO;
			glimmer_print_real (pji, &start, &end);
			gtk_widget_destroy (dialog);
			break;
		case GNOME_PRINT_DIALOG_RESPONSE_PREVIEW:
			pji->preview = PREVIEW_FROM_DIALOG;
			glimmer_print_preview_real (pji, &start, &end);
			break;
		default:
			gtk_widget_destroy (dialog);
			glimmer_print_job_info_destroy (pji, FALSE);
			return;
        }
} 

static void
show_printing_dialog (GlimmerPrintJobInfo *pji)
{
	GtkWidget *window;
	GtkWidget *parent;
	GtkWidget *frame;
	GtkWidget *hbox;
	GtkWidget *image;
	GtkWidget *vbox;
	GtkWidget *label;
	GtkWidget *progressbar;

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_modal (GTK_WINDOW (window), TRUE);
	gtk_window_set_resizable (GTK_WINDOW (window), FALSE);
	gtk_window_set_destroy_with_parent (GTK_WINDOW (window), TRUE);
	gtk_window_set_position (GTK_WINDOW (window), GTK_WIN_POS_CENTER_ON_PARENT);
	gtk_window_set_decorated (GTK_WINDOW (window), FALSE);

	parent = gtk_widget_get_toplevel (GTK_WIDGET (pji->view));
	if (GTK_WIDGET_TOPLEVEL (parent))
		gtk_window_set_transient_for (GTK_WINDOW (window),
					      GTK_WINDOW (parent));

	frame = gtk_frame_new (NULL);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_OUT);
	gtk_container_add (GTK_CONTAINER (window), frame);

	hbox = gtk_hbox_new (FALSE, 12);
 	gtk_container_add (GTK_CONTAINER (frame), hbox);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 12);

	image = gtk_image_new_from_stock ("gtk-print", GTK_ICON_SIZE_DIALOG);
	gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, FALSE, FALSE, 0);

	label = gtk_label_new (_("Preparing pages..."));
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);

	progressbar = gtk_progress_bar_new ();
	gtk_box_pack_start (GTK_BOX (vbox), progressbar, FALSE, FALSE, 0);
	
	pji->dialog = window;
	pji->label = label;
	pji->progressbar = progressbar;
	
	gtk_widget_show_all (pji->dialog);

	/* Update UI */
	while (gtk_events_pending ())
		gtk_main_iteration ();
}

static void
page_cb (GtkSourcePrintJob *job,
	 GlimmerPrintJobInfo *pji)
{
	gchar *str;
	gint page_num = gtk_source_print_job_get_page (pji->pjob);
	gint total = gtk_source_print_job_get_page_count (pji->pjob);

	if (pji->preview != PREVIEW_NO)
		str = g_strdup_printf (_("Rendering page %d of %d..."), page_num, total);
	else
		str = g_strdup_printf (_("Printing page %d of %d..."), page_num, total);

	gtk_label_set_label (GTK_LABEL (pji->label), str);
	g_free (str);

	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (pji->progressbar), 
				       1.0 * page_num / total);

	/* Update UI */
	while (gtk_events_pending ())
		gtk_main_iteration ();
}

static void
preview_finished_cb (GtkSourcePrintJob *job,
		     GlimmerPrintJobInfo *pji)
{
	GnomePrintJob *gjob;
	GtkWidget *parent;
	GtkWidget *preview = NULL;

	gjob = gtk_source_print_job_get_print_job (job);

	preview = gnome_print_job_preview_new (gjob, _("Print preview"));

	parent = gtk_widget_get_toplevel (GTK_WIDGET (pji->view));
	if (GTK_WIDGET_TOPLEVEL (parent)) {
		gtk_window_set_transient_for (GTK_WINDOW (preview),
					      GTK_WINDOW (parent));
		gtk_window_set_modal (GTK_WINDOW (preview), TRUE);
	}

 	g_object_unref (gjob);

	if (pji->preview == PREVIEW) {
		glimmer_print_job_info_destroy (pji, FALSE);
	} else {
		g_signal_handlers_disconnect_by_func (pji->pjob,
						      G_CALLBACK (page_cb), pji);
		g_signal_handlers_disconnect_by_func (pji->pjob,
						      G_CALLBACK (preview_finished_cb),
						      pji);
	}

	gtk_widget_destroy (pji->dialog);
	gtk_widget_show (preview);
}

static void
print_finished_cb (GtkSourcePrintJob *job,
		   GlimmerPrintJobInfo *pji)
{
	GnomePrintJob *gjob;

	gjob = gtk_source_print_job_get_print_job (job);

	gnome_print_job_print (gjob);
	
 	g_object_unref (gjob);

	glimmer_print_job_info_destroy (pji, TRUE);

	gtk_widget_destroy (pji->dialog);
}

void 
glimmer_print (GlimmerView *view)
{
	GlimmerPrintJobInfo *pji;
	GtkWidget *dialog;
	
	g_return_if_fail (view != NULL);

	pji = glimmer_print_job_info_new (view);
	pji->preview = PREVIEW_NO;

	dialog = get_print_dialog (pji);
	
	g_signal_connect (dialog, "response",
			  G_CALLBACK (glimmer_print_dialog_response), pji);

	gtk_widget_show (dialog);
}

static void 
glimmer_print_preview_real (GlimmerPrintJobInfo *pji, 
			    GtkTextIter         *start, 
			    GtkTextIter         *end)
{
	show_printing_dialog (pji);

	g_signal_connect (pji->pjob, "begin_page",
			  G_CALLBACK (page_cb), pji);
	g_signal_connect (pji->pjob, "finished",
			  G_CALLBACK (preview_finished_cb), pji);

	if (!gtk_source_print_job_print_range_async (pji->pjob, start, end)) {
		/* FIXME */
		g_warning ("Async print failed");
		gtk_widget_destroy (pji->dialog);
	}
}

static void 
glimmer_print_real (GlimmerPrintJobInfo *pji, 
		    GtkTextIter         *start, 
		    GtkTextIter         *end)
{
	show_printing_dialog (pji);

	g_signal_connect (pji->pjob, "begin_page",
			  G_CALLBACK (page_cb), pji);
	g_signal_connect (pji->pjob, "finished",
			  G_CALLBACK (print_finished_cb), pji);

	if (!gtk_source_print_job_print_range_async (pji->pjob, start, end)) {
		/* FIXME */
		g_warning ("Async print failed");
		gtk_widget_destroy (pji->dialog);
	}
}

void 
glimmer_print_preview (GlimmerView *view)
{
	GlimmerPrintJobInfo *pji;
	GtkTextIter start, end;

	g_return_if_fail (view != NULL);

	pji = glimmer_print_job_info_new (view);

	gtk_text_buffer_get_bounds (GTK_TEXT_BUFFER (pji->buffer), &start, &end);

	pji->preview = PREVIEW;
	glimmer_print_preview_real (pji, &start, &end);
}

static GlimmerPrintJobInfo *
glimmer_print_job_info_new (GlimmerView *view)
{	
	GtkSourceBuffer *buffer;
	GtkSourcePrintJob *pjob;
	GnomePrintConfig *config;
	GConfClient *client;
	gchar *str;
	GlimmerPrintJobInfo *pji;

	gchar *print_font_body;
	gchar *print_font_header;
	gchar *print_font_numbers;
	
	g_return_val_if_fail (view != NULL, NULL);

	buffer = GTK_SOURCE_BUFFER (gtk_text_view_get_buffer (GTK_TEXT_VIEW (view)));

	config = gnome_print_config_default ();
	g_return_val_if_fail (config != NULL, NULL);

	gnome_print_config_set_int (config, GNOME_PRINT_KEY_NUM_COPIES, 1);
	gnome_print_config_set_boolean (config, GNOME_PRINT_KEY_COLLATE, FALSE);

	pjob = gtk_source_print_job_new_with_buffer (config, buffer);

	gnome_print_config_unref (config);

	client = gconf_client_get_default ();

	gtk_source_print_job_set_highlight (pjob,
		gtk_source_buffer_get_highlight (buffer));

	gtk_source_print_job_set_print_numbers (pjob,
		gconf_client_get_int (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
				      GLIMMER_SETTING_PRINT_LINE_NUMBERS, NULL));

	str = gconf_client_get_string (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
				       GLIMMER_SETTING_PRINT_WRAP_MODE, NULL);
	gtk_source_print_job_set_wrap_mode (pjob,
		glimmer_util_get_wrap_mode_from_string (str));
	g_free (str);

	gtk_source_print_job_set_tabs_width (pjob,
		gconf_client_get_int (client, GLIMMER_BASE_KEY
				      GLIMMER_SETTING_TAB_WIDTH, NULL));
	
	if (gconf_client_get_bool (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
				   GLIMMER_SETTING_PRINT_HEADER, NULL)) {
		/*gchar *doc_name;*/
		const gchar *name_to_display = "FIXME: get filename from somewhere";
		gchar *left;

		/*doc_name = gedit_document_get_uri (doc);
		name_to_display = eel_str_middle_truncate (doc_name, 60);*/

		left = g_strdup_printf (_("File: %s"), name_to_display);
		
		/* Translators: %N is the current page number, %Q is the total
		 * number of pages (ex. Page 2 of 10) 
		 */
		gtk_source_print_job_set_header_format (pjob, left, NULL,
							_("Page %N of %Q"), TRUE);

		gtk_source_print_job_set_print_header (pjob, TRUE);

		/*g_free (doc_name);
		g_free (name_to_display);*/
		g_free (left);
	} else {
		gtk_source_print_job_set_print_header (pjob, FALSE);
	}

	gtk_source_print_job_set_print_footer (pjob, FALSE);

	print_font_body =
		gconf_client_get_string (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
					 GLIMMER_SETTING_PRINT_FONT_BODY, NULL);
	print_font_header =
		gconf_client_get_string (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
					 GLIMMER_SETTING_PRINT_FONT_HEADER, NULL);
	print_font_numbers =
		gconf_client_get_string (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
					 GLIMMER_SETTING_PRINT_FONT_NUMBERS, NULL);
	
	gtk_source_print_job_set_font (pjob, print_font_body);
	gtk_source_print_job_set_numbers_font (pjob, print_font_numbers);
	gtk_source_print_job_set_header_footer_font (pjob, print_font_header);

	g_free (print_font_body);
	g_free (print_font_header);
	g_free (print_font_numbers);

	g_object_unref (G_OBJECT (client));

	pji = g_new0 (GlimmerPrintJobInfo, 1);

	pji->pjob = pjob;

	pji->view = view;
	pji->buffer = buffer;
	pji->preview = PREVIEW_NO;
	pji->range_type = GNOME_PRINT_RANGE_ALL;

	return pji;
}
