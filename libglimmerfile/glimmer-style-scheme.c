/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  glimmer-style-scheme.c
 *
 *  Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <gconf/gconf-client.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-macros.h>
#include "glimmer-style-scheme.h"
#include "glimmer-themes.h"

enum {
	PROP_0,
	PROP_THEMES,
	PROP_LANGUAGE,
	PROP_THEME
};

struct _GlimmerStyleSchemePrivate {
	GlimmerThemes *themes;
	GtkSourceLanguage *language;
	gchar *theme;
	GHashTable *styles;
};

static GObjectClass *parent_class = NULL;

static void
set_tag_style (gpointer key, gpointer value, gpointer data)
{
	gchar *id = key;
	GtkSourceTagStyle *style = value;
	GtkSourceLanguage *language = GTK_SOURCE_LANGUAGE (data);

	gtk_source_language_set_tag_style (language, id, style);
}

static void
update_scheme (GlimmerStyleScheme *scheme)
{
	GHashTable *tags;

	if (!scheme->priv->theme)
		return;

	g_hash_table_destroy (scheme->priv->styles);

	/* Replace the basic styles first. */
	scheme->priv->styles = glimmer_themes_get_style_tags (scheme->priv->themes,
							      scheme->priv->theme,
							      NULL);

	/* Now set the language-specific tags. */
	if (scheme->priv->language) {
		gchar *lang_id = gtk_source_language_get_id (scheme->priv->language);
		tags = glimmer_themes_get_style_tags (scheme->priv->themes,
						      scheme->priv->theme,
						      lang_id);
		g_hash_table_foreach (tags, set_tag_style,
				      scheme->priv->language);
		g_free (lang_id);
	}
}

static void 
glimmer_style_scheme_get_property (GObject    *object,
				   guint       prop_id,
				   GValue     *value,
				   GParamSpec *pspec)
{
	GlimmerStyleScheme *scheme;

	g_return_if_fail (GLIMMER_IS_STYLE_SCHEME (object));

	scheme = GLIMMER_STYLE_SCHEME (object);

	switch (prop_id) {
		case PROP_THEMES:
			g_value_set_object (value, scheme->priv->themes);
			break;
		case PROP_LANGUAGE:
			g_value_set_object (value, scheme->priv->language);
			break;
		case PROP_THEME:
			g_value_set_string (value, scheme->priv->theme);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void 
glimmer_style_scheme_set_property (GObject      *object,
				   guint         prop_id,
				   const GValue *value,
				   GParamSpec   *pspec)
{
	GlimmerStyleScheme *scheme;

	g_return_if_fail (GLIMMER_IS_STYLE_SCHEME (object));

	scheme = GLIMMER_STYLE_SCHEME (object);

	switch (prop_id) {
		case PROP_THEMES:
			if (scheme->priv->themes) {
				g_object_unref (G_OBJECT (scheme->priv->themes));
				scheme->priv->themes = NULL;
			}
			scheme->priv->themes = GLIMMER_THEMES (g_value_get_object (value));
			g_object_ref (G_OBJECT (scheme->priv->themes));
			break;
		case PROP_LANGUAGE:
			if (scheme->priv->language) {
				g_object_unref (G_OBJECT (scheme->priv->language));
				scheme->priv->language = NULL;
			}
			glimmer_style_scheme_set_language (scheme,
				GTK_SOURCE_LANGUAGE (g_value_get_object (value)));
			break;
		case PROP_THEME:
			if (scheme->priv->theme) {
				g_free (scheme->priv->theme);
				scheme->priv->theme = NULL;
			}
			glimmer_style_scheme_set_theme (scheme,
							g_value_get_string (value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void
glimmer_style_scheme_finalize (GObject *object)
{
	GlimmerStyleScheme *scheme = GLIMMER_STYLE_SCHEME (object);

	if (scheme->priv) {
		if (scheme->priv->language)
			g_free (scheme->priv->language);
		if (scheme->priv->theme)
			g_free (scheme->priv->theme);
		g_hash_table_destroy (scheme->priv->styles);
		g_free (scheme->priv);
		scheme->priv = NULL;
	}
}

static const gchar *
glimmer_style_scheme_get_name (GtkSourceStyleScheme *scheme)
{
	GlimmerStyleScheme *ss;

	g_return_val_if_fail (GLIMMER_IS_STYLE_SCHEME (scheme), NULL);

	ss = GLIMMER_STYLE_SCHEME (scheme);

	return ss->priv->theme;
}

static GtkSourceTagStyle *
glimmer_style_scheme_get_tag_style (GtkSourceStyleScheme *scheme,
				    const gchar *style_name)
{
	GlimmerStyleScheme *ss;
	GtkSourceTagStyle *style;
	char *id;

	g_return_val_if_fail (GLIMMER_IS_STYLE_SCHEME (scheme), NULL);
	g_return_val_if_fail (style_name != NULL, NULL);

	ss = GLIMMER_STYLE_SCHEME (scheme);

	id = gconf_escape_key (style_name, strlen (style_name));
	style = g_hash_table_lookup (ss->priv->styles, id);
	g_free (id);

	if (style) {
		return gtk_source_tag_style_copy (style);
	} else {
		g_warning ("Unknown style '%s'", style_name);
		return NULL;
	}
}

static void
add_style_name (gpointer key, gpointer value, gpointer user_data)
{
	GSList **names = user_data;

	*names = g_slist_append (*names, g_strdup (key));
}

static GSList *
glimmer_style_scheme_get_style_names (GtkSourceStyleScheme *scheme)
{
	GlimmerStyleScheme *ss;
	GSList *names = NULL;

	g_return_val_if_fail (GLIMMER_IS_STYLE_SCHEME (scheme), NULL);

	ss = GLIMMER_STYLE_SCHEME (scheme);

	g_hash_table_foreach (ss->priv->styles, add_style_name, &names);

	return names;
}

static void
glimmer_style_scheme_class_init (GlimmerStyleSchemeClass *klass) 
{
	GObjectClass *g_object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	g_object_class->get_property = glimmer_style_scheme_get_property;
	g_object_class->set_property = glimmer_style_scheme_set_property;
	g_object_class->finalize = glimmer_style_scheme_finalize;

	g_object_class_install_property 
		(g_object_class, PROP_THEMES,
		 g_param_spec_object ("themes", 
				      _("Themes class"),
				      _("GlimmerThemes instance used to retrieve theme data"),
				      GLIMMER_TYPE_THEMES,
				      G_PARAM_READWRITE));
	g_object_class_install_property 
		(g_object_class, PROP_LANGUAGE,
		 g_param_spec_object ("language", 
				      _("Language"),
				      _("The language for which this style is used"),
				      GTK_TYPE_SOURCE_LANGUAGE,
				      G_PARAM_READWRITE));
	g_object_class_install_property 
		(g_object_class, PROP_THEME,
		 g_param_spec_string ("theme", 
				      _("Style theme"),
				      _("The theme used for this scheme"),
				      "Default",
				      G_PARAM_READWRITE));
}

static void   
glimmer_style_scheme_iface_init (GtkSourceStyleSchemeClass *iface)
{
	iface->get_tag_style 	= glimmer_style_scheme_get_tag_style;
	iface->get_name		= glimmer_style_scheme_get_name;
	iface->get_style_names  = glimmer_style_scheme_get_style_names;
}

static void
glimmer_style_scheme_instance_init (GlimmerStyleScheme *scheme)
{
	scheme->priv = g_new0 (GlimmerStyleSchemePrivate, 1);
	scheme->priv->language = NULL;
	scheme->priv->theme = g_strdup ("Default");
	scheme->priv->styles = g_hash_table_new_full ((GHashFunc)g_str_hash,
						      (GEqualFunc)g_str_equal,
						      (GDestroyNotify)g_free,
						      (GDestroyNotify)gtk_source_tag_style_free);
}

GType
glimmer_style_scheme_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (GlimmerStyleSchemeClass),
			NULL,		/* base_init */
			NULL,		/* base_finalize */
			(GClassInitFunc) glimmer_style_scheme_class_init,
			NULL,		/* class_finalize */
			NULL,		/* class_data */
			sizeof (GlimmerStyleScheme),
			0,		/* n_preallocs */
			(GInstanceInitFunc) glimmer_style_scheme_instance_init,
		};
      
		static const GInterfaceInfo iface_info = {
			(GInterfaceInitFunc) glimmer_style_scheme_iface_init, /* interface_init */
		        NULL, /* interface_finalize */
			NULL  /* interface_data */
		};

		type = g_type_register_static (G_TYPE_OBJECT, "GlimmerStyleScheme",
					       &info, 0);

		g_type_add_interface_static (type, GTK_TYPE_SOURCE_STYLE_SCHEME,
					     &iface_info);
	}
	
	return type;
}

GlimmerStyleScheme *
glimmer_style_scheme_new (GlimmerThemes *themes,
			  GtkSourceLanguage *language,
			  const gchar *theme)
{
	return GLIMMER_STYLE_SCHEME (g_object_new (GLIMMER_TYPE_STYLE_SCHEME,
						   "themes", themes,
						   "language", language,
						   "theme", theme, NULL));
}

GlimmerStyleScheme *
glimmer_style_scheme_new_default (GlimmerThemes *themes)
{
	return GLIMMER_STYLE_SCHEME (g_object_new (GLIMMER_TYPE_STYLE_SCHEME,
						   "themes", themes, 
						   "theme", "Default", NULL));
}

GtkSourceLanguage *
glimmer_style_scheme_get_language (GlimmerStyleScheme *scheme)
{
	g_return_val_if_fail (GLIMMER_IS_STYLE_SCHEME (scheme), NULL);

	return scheme->priv->language;
}

void
glimmer_style_scheme_set_language (GlimmerStyleScheme *scheme,
				   GtkSourceLanguage  *language)
{
	g_return_if_fail (GLIMMER_IS_STYLE_SCHEME (scheme));

	if (scheme->priv->language) {
		g_object_unref (G_OBJECT (scheme->priv->language));
		scheme->priv->language = NULL;
	}

	if (GTK_IS_SOURCE_LANGUAGE (language)) {
		g_object_ref (G_OBJECT (language));
		scheme->priv->language = language;
	}

	update_scheme (scheme);

	g_object_notify (G_OBJECT (scheme), "language"); 
}

gchar *
glimmer_style_scheme_get_theme (GlimmerStyleScheme *scheme)
{
	g_return_val_if_fail (GLIMMER_IS_STYLE_SCHEME (scheme), NULL);

	return g_strdup (scheme->priv->theme);
}

void
glimmer_style_scheme_set_theme (GlimmerStyleScheme *scheme,
				const gchar        *name)
{
	g_return_if_fail (GLIMMER_IS_STYLE_SCHEME (scheme));

	if (scheme->priv->theme) {
		g_free (scheme->priv->theme);
		scheme->priv->theme = NULL;
	}

	if (name != NULL)
		scheme->priv->theme = g_strdup (name);

	update_scheme (scheme);

	g_object_notify (G_OBJECT (scheme), "theme");
}

void
glimmer_style_scheme_set_tag_style (GlimmerStyleScheme      *scheme,
				    const gchar             *tag_id,
				    const GtkSourceTagStyle *style)
{
	GtkSourceTagStyle *ts;

	g_return_if_fail (GLIMMER_IS_STYLE_SCHEME (scheme));
	g_return_if_fail (tag_id != NULL);
	g_return_if_fail (style != NULL);

	ts = g_hash_table_lookup (scheme->priv->styles, tag_id);
	g_message ("ts == NULL: %d", ts == NULL);
	if (ts) {
		ts->mask = style->mask;
		ts->foreground.red = style->foreground.red;
		ts->foreground.green = style->foreground.green;
		ts->foreground.blue = style->foreground.blue;
		ts->background.red = style->background.red;
		ts->background.green = style->background.green;
		ts->background.blue = style->background.blue;
		ts->italic = style->italic;
		ts->bold = style->bold;
		ts->underline = style->underline;
		ts->strikethrough = style->strikethrough;
		
		g_signal_emit_by_name (G_OBJECT (scheme), "style_changed", tag_id);
	} else {
		if (!scheme->priv->language) {
			g_warning ("Can't set non-default style without a language set");
			return;
		}

		gtk_source_language_set_tag_style (scheme->priv->language,
						   tag_id, style);
	}
}
