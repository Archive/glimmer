/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * glimmer-utils.h
 *
 * Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GLIMMER_UTILS_H__
#define __GLIMMER_UTILS_H__

#include <gtk/gtkwidget.h>
#include <gtk/gtktextview.h>

#define GTK_TEXT_SEARCH_CASE_INSENSITIVE (1 << 7)

G_BEGIN_DECLS

GtkWidget  *glimmer_button_new_with_stock_image    (const gchar *text,
						    const gchar *stock_id);
GtkWrapMode glimmer_util_get_wrap_mode_from_string (const gchar *str);

G_END_DECLS

#endif /* __GLIMMER_UTILS_H__ */
