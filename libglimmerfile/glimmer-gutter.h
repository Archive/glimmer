/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *
 * glimmer-gutter.h
 *
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef __GLIMMER_GUTTER_H__
#define __GLIMMER_GUTTER_H__

#include <libbonobo.h>
#include "glimmer-view.h"
#include "editor-gutter.h"

G_BEGIN_DECLS

#define GLIMMER_TYPE_GUTTER		(glimmer_gutter_get_type ())
#define GLIMMER_GUTTER(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), GLIMMER_TYPE_GUTTER, GlimmerGutter))
#define GLIMER_CLASS_GUTTER(k)		(G_TYPE_CHECK_CLASS_CAST((k), GLIMMER_TYPE_GUTTER, GlimmerGutterClass))
#define GLIMMER_IS_GUTTER(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), GLIMMER_TYPE_GUTTER))
#define GLIMMER_IS_GUTTER_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), GLIMMER_TYPE_GUTTER))

typedef struct _GlimmerGutter      GlimmerGutter;
typedef struct _GlimmerGutterClass GlimmerGutterClass;

struct _GlimmerGutter {
	BonoboObject parent;

	GlimmerView *view;
};

struct _GlimmerGutterClass {
	BonoboObjectClass parent_class;

	POA_GNOME_Development_EditorGutter__epv epv;
};


GType          glimmer_gutter_get_type (void) G_GNUC_CONST;

GlimmerGutter *glimmer_gutter_new (GlimmerView *view);


G_END_DECLS

#endif /* __GLIMMER_GUTTER_H__ */
