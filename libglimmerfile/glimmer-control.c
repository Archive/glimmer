/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  glimmer-control.c
 *
 *  Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "glimmer-commands.h"
#include "glimmer-control.h"
#include "glimmer-buffer.h"
#include "glimmer-gutter.h"
#include "glimmer-persist-stream.h"
#include "glimmer-persist-file.h"
#include "glimmer-view.h"

enum {
	PROP_POSITION,
	PROP_LINE_NUM,
	PROP_SELECTION_START,
	PROP_SELECTION_END,
	PROP_READONLY,
	PROP_DIRTY
};

/* Bonobo UI callbacks. */
static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB ("FilePageSetup", page_setup_cb),
	BONOBO_UI_UNSAFE_VERB ("FilePrintPreview", print_preview_cb),
	BONOBO_UI_UNSAFE_VERB ("FilePrint", print_cb),
	BONOBO_UI_UNSAFE_VERB ("EditUndo", undo_cb),
	BONOBO_UI_UNSAFE_VERB ("EditRedo", redo_cb),
	BONOBO_UI_UNSAFE_VERB ("EditCut", cut_cb),
	BONOBO_UI_UNSAFE_VERB ("EditCopy", copy_cb),
	BONOBO_UI_UNSAFE_VERB ("EditPaste", paste_cb),
	BONOBO_UI_UNSAFE_VERB ("EditSelectAll", select_all_cb),
	BONOBO_UI_UNSAFE_VERB ("EditorPreferences", preferences_cb),
	BONOBO_UI_UNSAFE_VERB ("SearchFind", find_cb),
	BONOBO_UI_UNSAFE_VERB ("SearchFindNext", find_next_cb),
	BONOBO_UI_UNSAFE_VERB ("SearchFindPrevious", find_previous_cb),
	BONOBO_UI_UNSAFE_VERB ("SearchReplace", replace_cb),
	BONOBO_UI_UNSAFE_VERB ("SearchGotoLine", goto_line_cb),
	BONOBO_UI_VERB_END
};

static BonoboUIVerb popup_menu_verbs[] = {
	BONOBO_UI_UNSAFE_VERB ("PopupUndo", undo_cb),
	BONOBO_UI_UNSAFE_VERB ("PopupRedo", redo_cb),
	BONOBO_UI_UNSAFE_VERB ("PopupCut", cut_cb),
	BONOBO_UI_UNSAFE_VERB ("PopupCopy", copy_cb),
	BONOBO_UI_UNSAFE_VERB ("PopupPaste", paste_cb),
	BONOBO_UI_VERB_END
};

static void
set_prop (BonoboPropertyBag *bag,
	  const BonoboArg   *arg,
	  guint              arg_id,
	  CORBA_Environment *ev,
	  gpointer           data)
{
	GtkTextView *view = GTK_TEXT_VIEW (data);
	GtkTextBuffer *buffer = view->buffer;
	GtkTextIter iter;
	GtkTextMark *mark;
	long pos;

	switch (arg_id) {
	case PROP_POSITION:
		pos = BONOBO_ARG_GET_LONG (arg);
		mark = gtk_text_buffer_get_insert (buffer);
		gtk_text_buffer_get_iter_at_mark (buffer, &iter, mark);
		gtk_text_buffer_place_cursor (buffer, &iter);
		gtk_text_view_scroll_to_mark (view,
					      gtk_text_buffer_get_insert (buffer),
					      0.0, TRUE, 0.0, 0.5);
		break;
	case PROP_LINE_NUM:
		pos = BONOBO_ARG_GET_LONG (arg);
		pos--;
		gtk_text_buffer_get_iter_at_line (buffer, &iter, pos);
		gtk_text_buffer_place_cursor (buffer, &iter);
		gtk_text_view_scroll_to_mark (view,
					      gtk_text_buffer_get_insert (buffer),
					      0.0, TRUE, 0.0, 0.5);
		break;
	case PROP_SELECTION_START:
		pos = BONOBO_ARG_GET_LONG (arg);
		gtk_text_buffer_get_iter_at_offset (buffer, &iter, pos);
		mark = gtk_text_buffer_get_selection_bound (buffer);
		gtk_text_buffer_move_mark (buffer, mark, &iter);
		break;
	case PROP_SELECTION_END:
		pos = BONOBO_ARG_GET_LONG (arg);
		gtk_text_buffer_get_iter_at_offset (buffer, &iter, pos);
		mark = gtk_text_buffer_get_insert (buffer);
		gtk_text_buffer_move_mark (buffer, mark, &iter);
		break;
	case PROP_READONLY:
		pos = BONOBO_ARG_GET_BOOLEAN (arg);
		/*view->priv->editable = pos ? TRUE : FALSE;*/
		break;
	case PROP_DIRTY:
		break;
	}
}

static void
get_prop (BonoboPropertyBag *bag,
	  BonoboArg         *arg,
	  guint              arg_id,
	  CORBA_Environment *ev,
	  gpointer           data)
{
	GtkTextView *view = GTK_TEXT_VIEW (data);
	GtkTextBuffer *buffer = view->buffer;
	GtkTextIter iter;
	GtkTextMark *mark;
	long pos;

	switch (arg_id) {
	case PROP_POSITION:
		mark = gtk_text_buffer_get_insert (buffer);
		gtk_text_buffer_get_iter_at_mark (buffer, &iter, mark);
		pos = gtk_text_iter_get_offset (&iter);
		BONOBO_ARG_SET_LONG (arg, pos);
		break;
	case PROP_LINE_NUM:
		mark = gtk_text_buffer_get_insert (buffer);
		gtk_text_buffer_get_iter_at_mark (buffer, &iter, mark);
		pos = gtk_text_iter_get_line (&iter);
		pos++;
		BONOBO_ARG_SET_LONG (arg, pos);
		break;
	case PROP_SELECTION_START:
		mark = gtk_text_buffer_get_selection_bound (buffer);
		gtk_text_buffer_get_iter_at_mark (buffer, &iter, mark);
		pos = gtk_text_iter_get_offset (&iter);
		BONOBO_ARG_SET_LONG (arg, pos);
		break;
	case PROP_SELECTION_END:
		mark = gtk_text_buffer_get_insert (buffer);
		gtk_text_buffer_get_iter_at_mark (buffer, &iter, mark);
		pos = gtk_text_iter_get_offset (&iter);
		BONOBO_ARG_SET_LONG (arg, pos);
		break;
	case PROP_READONLY:
		/*BONOBO_ARG_SET_BOOLEAN (arg, file->priv->editable);*/
		break;
	case PROP_DIRTY:
		BONOBO_ARG_SET_BOOLEAN (arg, gtk_text_buffer_get_modified (buffer));
		break;
	}
}

static void
update_cursor_position (GtkTextBuffer *buffer, GlimmerView *view)
{
	GtkWidget *cursor_pos;
	GtkTextIter iter, start;
	gchar *msg;
	gint tab_width, row, col;

	cursor_pos = g_object_get_data (G_OBJECT (view), "CursorPosition");
	if (!cursor_pos)
		return;

	gtk_statusbar_pop (GTK_STATUSBAR (cursor_pos), 0);

	gtk_text_buffer_get_iter_at_mark (buffer,
					  &iter,
					  gtk_text_buffer_get_insert (buffer));

	tab_width = gtk_source_view_get_tabs_width (GTK_SOURCE_VIEW (view));
	row = gtk_text_iter_get_line (&iter) + 1;

	start = iter;
	gtk_text_iter_set_line_offset (&start, 0);
	col = 0;

	while (!gtk_text_iter_equal (&start, &iter)) {
		if (gtk_text_iter_get_char (&start) == '\t') {
			col += (tab_width - (col % tab_width));
		} else {
			++col;
		}

		gtk_text_iter_forward_char (&start);
	}

	msg = g_strdup_printf (_("  Ln %d, Col. %d"), row, col);
	gtk_statusbar_push (GTK_STATUSBAR (cursor_pos), 0, msg);
	g_free (msg);
}

static void
toggle_overwrite_cb (GtkTextView *view, gpointer data)
{
	GtkWidget *mode;
	gboolean overwrite;

	mode = g_object_get_data (G_OBJECT (view), "OverwriteMode");
	if (!mode)
		return;

	gtk_statusbar_pop (GTK_STATUSBAR (mode), 0);
	overwrite = glimmer_view_get_overwrite_mode (GLIMMER_VIEW (view));
	gtk_statusbar_push (GTK_STATUSBAR (mode), 0,
			    overwrite ? _("  OVR") : _("  INS"));
}

static void
activate_cb (BonoboControl *control,
	     gboolean       activate,
	     GlimmerView   *view)
{
	BonoboUIComponent *ui_component, *popup_component;
	GtkWidget *widget;
	BonoboControl *ctrl;

	ui_component = bonobo_control_get_ui_component (control);

	if (activate) {
		Bonobo_UIContainer remote_uic;

		remote_uic = bonobo_control_get_remote_ui_container (control, NULL);
		bonobo_ui_component_set_container (ui_component, remote_uic, NULL);

		bonobo_ui_component_freeze (ui_component, NULL);

		/* Hook up the user-interface. */
		bonobo_ui_component_add_verb_list_with_data (ui_component,
							     verbs, view);
		bonobo_object_release_unref (remote_uic, NULL);
		bonobo_ui_util_set_ui (ui_component,
				       DATADIR,
				       "glimmer-ui.xml",
				       "glimmer-control",
				       NULL);

		/* Add the popup menu ui. */
		popup_component = bonobo_control_get_popup_ui_component (control);

		bonobo_ui_component_freeze (popup_component, NULL);

		bonobo_ui_component_add_verb_list_with_data (popup_component,
							     popup_menu_verbs,
							     view);
		bonobo_ui_util_set_ui (popup_component,
				       DATADIR,
				       "glimmer-ui-popup.xml",
				       "glimmer-popup",
				       NULL);

		bonobo_ui_component_thaw (popup_component, NULL);

		/* Add cursor position status bar */
		widget = gtk_statusbar_new ();
		ctrl = bonobo_control_new (widget);

		gtk_widget_set_size_request (widget, 150, 10);
		gtk_statusbar_set_has_resize_grip (GTK_STATUSBAR (widget), FALSE);
		gtk_widget_show (widget);

		bonobo_ui_component_object_set (ui_component,
						"/status/CursorPosition",
						BONOBO_OBJREF (ctrl),
						NULL);
		bonobo_object_unref (BONOBO_OBJECT (ctrl));

		g_object_set_data (G_OBJECT (view), "CursorPosition", widget);

		/* Add overwrite mode status bar */
		widget = gtk_statusbar_new ();
		ctrl = bonobo_control_new (widget);

		gtk_widget_set_size_request (widget, 80, 10);
		gtk_statusbar_set_has_resize_grip (GTK_STATUSBAR (widget), TRUE);
		gtk_widget_show (widget);

		bonobo_ui_component_object_set (ui_component,
						"/status/OverwriteMode",
						BONOBO_OBJREF (ctrl),
						NULL);
		bonobo_object_unref (BONOBO_OBJECT (ctrl));

		g_object_set_data (G_OBJECT (view), "OverwriteMode", widget);

		update_cursor_position (GTK_TEXT_VIEW (view)->buffer, view);
		toggle_overwrite_cb (GTK_TEXT_VIEW (view), view);

		bonobo_ui_component_thaw (ui_component, NULL);
	} else {
		bonobo_ui_component_unset_container (ui_component, NULL);

		g_object_set_data (G_OBJECT (view), "CursorPosition", NULL);
		g_object_set_data (G_OBJECT (view), "OverwriteMode", NULL);
	}
}

static void 
move_cursor_cb (GtkTextBuffer *buffer,
		GtkTextIter   *cursoriter,
		GtkTextMark   *mark,
		gpointer       data)
{
	if (mark != gtk_text_buffer_get_insert (buffer))
		return;

	update_cursor_position (buffer, data);
}

static void
modified_changed_cb (GtkTextBuffer *buffer, gpointer data)
{
	GlimmerView *view = GLIMMER_VIEW (data);
	BonoboControl *control;
	BonoboPersistStream *stream;
	Bonobo_PropertyBag bag;
	gboolean modified;

	control = g_object_get_data (G_OBJECT (view), "BonoboControl");
	stream = g_object_get_data (G_OBJECT (view), "PersistStream");
	bag = bonobo_control_get_properties (control);
	modified = gtk_text_buffer_get_modified (GTK_TEXT_VIEW (view)->buffer);

	bonobo_persist_set_dirty (BONOBO_PERSIST (stream), modified);
	bonobo_pbclient_set_boolean (bag, "dirty", modified, NULL);
}

static void
mark_set_cb (GtkTextBuffer *buffer,
	     GtkTextIter   *iter,
	     GtkTextMark   *mark,
	     gpointer       data)
{
	GlimmerView *view = GLIMMER_VIEW (data);
	BonoboControl *control;
	GtkTextIter insert, sel_bound;
	BonoboUIComponent *component;
	const char *value;

	control = g_object_get_data (G_OBJECT (view), "BonoboControl");

	gtk_text_buffer_get_iter_at_mark (buffer, &insert,
					  gtk_text_buffer_get_insert (buffer));
	gtk_text_buffer_get_iter_at_mark (buffer, &sel_bound,
					  gtk_text_buffer_get_selection_bound (buffer));

	value = gtk_text_iter_equal (&insert, &sel_bound) ? "0" : "1";

	/* Update menuitems and toolbar buttons. */
	component = bonobo_control_get_ui_component (control);
	if (bonobo_ui_component_get_container (component) != NULL) {
		bonobo_ui_component_set_prop (component, "/commands/EditCut",
					      "sensitive", value, NULL);
		bonobo_ui_component_set_prop (component, "/commands/EditCopy",
					      "sensitive", value, NULL);
	}
	/* Update popup menu. */
	component = bonobo_control_get_popup_ui_component (control);
	if (bonobo_ui_component_get_container (component) != NULL) {
		bonobo_ui_component_set_prop (component, "/commands/PopupCut",
					      "sensitive", value, NULL);
		bonobo_ui_component_set_prop (component, "/commands/PopupCopy",
					      "sensitive", value, NULL);
	}
}

static void
can_undo_cb (GtkSourceBuffer *buffer,
	     gboolean         can_undo,
	     gpointer         data)
{
	GlimmerView *view = GLIMMER_VIEW (data);
	BonoboControl *control;
	BonoboUIComponent *component;

	control = g_object_get_data (G_OBJECT (view), "BonoboControl");

	component = bonobo_control_get_ui_component (control);
	bonobo_ui_component_set_prop (component,
				      "/commands/EditUndo",
				      "sensitive",
				      can_undo ? "1" : "0",
				      NULL);
	component = bonobo_control_get_popup_ui_component (control);
	bonobo_ui_component_set_prop (component,
				      "/commands/PopupUndo",
				      "sensitive",
				      can_undo ? "1" : "0",
				      NULL);
}

static void
can_redo_cb (GtkSourceBuffer *buffer,
	     gboolean         can_redo,
	     gpointer         data)
{
	GlimmerView *view = GLIMMER_VIEW (data);
	BonoboControl *control;
	BonoboUIComponent *component;

	control = g_object_get_data (G_OBJECT (view), "BonoboControl");

	component = bonobo_control_get_ui_component (control);
	bonobo_ui_component_set_prop (component,
				      "/commands/EditRedo",
				      "sensitive",
				      can_redo ? "1" : "0",
				      NULL);
	component = bonobo_control_get_popup_ui_component (control);
	bonobo_ui_component_set_prop (component,
				      "/commands/PopupRedo",
				      "sensitive",
				      can_redo ? "1" : "0",
				      NULL);
}

static gboolean
popup_menu_cb (GtkWidget      *widget,
	       GdkEventButton *event,
	       gpointer        user_data)
{
	GlimmerView *view = GLIMMER_VIEW (user_data);
	BonoboControl *control;

	control = g_object_get_data (G_OBJECT (view), "BonoboControl");

	if (event->type == GDK_BUTTON_PRESS && event->button == 3) {
		bonobo_control_do_popup_full (control, NULL, NULL, NULL, view,
					      event->button, event->time);
		return TRUE;
	}

	return FALSE;
}

BonoboControl *
glimmer_control_new (void)
{
	GtkWidget *view, *sw;
	BonoboControl *control;
	BonoboPersistStream *persist_stream;
	BonoboPersistFile *persist_file;
	GlimmerBuffer *editor_buffer;
	GlimmerGutter *editor_gutter;
	BonoboPropertyBag *properties;

	view = glimmer_view_new ();
	gtk_widget_show (view);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw),
					     GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (sw), view);
	gtk_widget_show (sw);

	control = bonobo_control_new (sw);
	g_object_set_data (G_OBJECT (view), "BonoboControl", control);

	persist_stream = glimmer_persist_stream_new (GLIMMER_VIEW (view));
	bonobo_object_add_interface (BONOBO_OBJECT (control),
				     BONOBO_OBJECT (persist_stream));

	persist_file = glimmer_persist_file_new (GLIMMER_VIEW (view));
	bonobo_object_add_interface (BONOBO_OBJECT (control),
				     BONOBO_OBJECT (persist_file));

	editor_buffer = glimmer_buffer_new (GLIMMER_VIEW (view));
	bonobo_object_add_interface (BONOBO_OBJECT (control), 
				     BONOBO_OBJECT (editor_buffer));

	editor_gutter = glimmer_gutter_new (GLIMMER_VIEW (view));
	bonobo_object_add_interface (BONOBO_OBJECT (control), 
				     BONOBO_OBJECT (editor_gutter));

	properties = bonobo_property_bag_new (get_prop, set_prop, view);

	bonobo_property_bag_add (properties, "position", PROP_POSITION,
				 BONOBO_ARG_LONG, NULL,
				 _("Position in the buffer"),
				 BONOBO_PROPERTY_READABLE | BONOBO_PROPERTY_WRITEABLE);
	bonobo_property_bag_add (properties, "line_num", PROP_LINE_NUM,
				 BONOBO_ARG_LONG, NULL,
				 _("Current line number"),
				 BONOBO_PROPERTY_READABLE | BONOBO_PROPERTY_WRITEABLE);
	bonobo_property_bag_add (properties, "selection_start", PROP_SELECTION_START,
				 BONOBO_ARG_LONG, NULL,
				 _("Beginning of the selection"),
				 BONOBO_PROPERTY_READABLE | BONOBO_PROPERTY_WRITEABLE);
	bonobo_property_bag_add (properties, "selection_end", PROP_SELECTION_END,
				 BONOBO_ARG_LONG, NULL,
				 _("End of the selection"),
				 BONOBO_PROPERTY_READABLE | BONOBO_PROPERTY_WRITEABLE);
	bonobo_property_bag_add (properties, "readonly", PROP_READONLY,
				 BONOBO_ARG_BOOLEAN, NULL,
				 _("Editable state of the buffer"),
				 BONOBO_PROPERTY_READABLE | BONOBO_PROPERTY_WRITEABLE);
	bonobo_property_bag_add (properties, "dirty", PROP_DIRTY,
				 BONOBO_ARG_BOOLEAN, NULL,
				 _("Buffer is dirty"),
				 BONOBO_PROPERTY_READABLE | BONOBO_PROPERTY_WRITEABLE);

	bonobo_control_set_properties (control, BONOBO_OBJREF (properties), NULL);
	bonobo_object_unref (BONOBO_OBJECT (properties));

	g_signal_connect (G_OBJECT (control), "activate",
			  G_CALLBACK (activate_cb), view);
	g_signal_connect (G_OBJECT (view), "toggle-overwrite",
			  G_CALLBACK (toggle_overwrite_cb), view);
	g_signal_connect (G_OBJECT (view), "button_press_event",
			  G_CALLBACK (popup_menu_cb), view);
	g_signal_connect (G_OBJECT (GTK_TEXT_VIEW (view)->buffer), "mark_set",
			  G_CALLBACK (move_cursor_cb), view);
	g_signal_connect (G_OBJECT (GTK_TEXT_VIEW (view)->buffer), "changed",
			  G_CALLBACK (update_cursor_position), view);
	g_signal_connect (G_OBJECT (GTK_TEXT_VIEW (view)->buffer), "modified_changed",
			  G_CALLBACK (modified_changed_cb), view);
	g_signal_connect (G_OBJECT (GTK_TEXT_VIEW (view)->buffer), "mark_set",
			  G_CALLBACK (mark_set_cb), view);
	g_signal_connect (G_OBJECT (GTK_TEXT_VIEW (view)->buffer), "can_undo",
			  G_CALLBACK (can_undo_cb), view);
	g_signal_connect (G_OBJECT (GTK_TEXT_VIEW (view)->buffer), "can_redo",
			  G_CALLBACK (can_redo_cb), view);

	return control;
}
