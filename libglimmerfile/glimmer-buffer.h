/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *
 * glimmer-buffer.h
 *
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef __GLIMMER_BUFFER_H__
#define __GLIMMER_BUFFER_H__

#include <libbonobo.h>
#include "glimmer-view.h"
#include "editor-buffer.h"

G_BEGIN_DECLS

#define GLIMMER_TYPE_BUFFER		(glimmer_buffer_get_type ())
#define GLIMMER_BUFFER(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), GLIMMER_TYPE_BUFFER, GlimmerBuffer))
#define GLIMER_CLASS_BUFFER(k)		(G_TYPE_CHECK_CLASS_CAST((k), GLIMMER_TYPE_BUFFER, GlimmerBufferClass))
#define GLIMMER_IS_BUFFER(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), GLIMMER_TYPE_BUFFER))
#define GLIMMER_IS_BUFFER_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), GLIMMER_TYPE_BUFFER))

typedef struct _GlimmerBuffer      GlimmerBuffer;
typedef struct _GlimmerBufferClass GlimmerBufferClass;

struct _GlimmerBuffer {
	BonoboObject parent;

	GlimmerView *view;
};

struct _GlimmerBufferClass {
	BonoboObjectClass parent_class;

	POA_GNOME_Development_EditorBuffer__epv epv;
};


GType          glimmer_buffer_get_type (void) G_GNUC_CONST;

GlimmerBuffer *glimmer_buffer_new (GlimmerView *view);


G_END_DECLS

#endif /* __GLIMMER_BUFFER_H__ */
