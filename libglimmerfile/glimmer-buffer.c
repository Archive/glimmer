/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  glimmer-buffer.c
 *
 *  Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, 
 *  Boston, MA 02111-1307, USA. 
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include "glimmer-buffer.h"

static BonoboObjectClass *parent_class;

static void glimmer_buffer_class_init (GlimmerBufferClass *klass);
static void glimmer_buffer_init (GlimmerBuffer *buffer);

static CORBA_long impl_get_length (PortableServer_Servant servant,
				   CORBA_Environment     *ev);
static void impl_get_chars (PortableServer_Servant                 servant,
			    const CORBA_long                       offset,
			    const CORBA_long                       count,
			    GNOME_Development_EditorBuffer_iobuf **buffer,
			    CORBA_Environment                     *ev);
static void impl_insert (PortableServer_Servant servant,
			 const CORBA_long       offset,
			 const CORBA_char      *str,
			 CORBA_Environment     *ev);
static void impl_delete (PortableServer_Servant servant,
			 const CORBA_long       offset,
			 const CORBA_long       count,
			 CORBA_Environment     *ev);

/* Private functions. */
static void
glimmer_buffer_class_init (GlimmerBufferClass *klass)
{
	GObjectClass *object_class;
	POA_GNOME_Development_EditorBuffer__epv *epv = &klass->epv;

	object_class = G_OBJECT_CLASS (klass);
	parent_class = g_type_class_peek_parent (klass);

	epv->getLength = impl_get_length;
	epv->getChars = impl_get_chars;
	epv->insert = impl_insert;
	epv->delete = impl_delete;
}

static void
glimmer_buffer_init (GlimmerBuffer *buffer)
{
}

static CORBA_long
impl_get_length (PortableServer_Servant servant,
		 CORBA_Environment     *ev)
{
	GlimmerBuffer *buffer;

	buffer = GLIMMER_BUFFER (bonobo_object_from_servant (servant));
	bonobo_return_val_if_fail (buffer != NULL, -1, ev);

	return gtk_text_buffer_get_char_count (GTK_TEXT_BUFFER (GTK_TEXT_VIEW (buffer->view)->buffer));
}

static void
impl_get_chars (PortableServer_Servant                 servant,
		const CORBA_long                       offset,
		const CORBA_long                       count,
		GNOME_Development_EditorBuffer_iobuf **buffer,
		CORBA_Environment                     *ev)
{
	GlimmerBuffer *glimmer_buffer;
	GtkTextBuffer *text_buffer;
	GtkTextIter start, end;
	char *text;

	glimmer_buffer = GLIMMER_BUFFER (bonobo_object_from_servant (servant));
	bonobo_return_if_fail (glimmer_buffer != NULL, ev);

	*buffer = GNOME_Development_EditorBuffer_iobuf__alloc ();
	CORBA_sequence_set_release (*buffer, TRUE);
	(*buffer)->_buffer = CORBA_sequence_CORBA_octet_allocbuf (count);
	(*buffer)->_length = count;

	text_buffer = GTK_TEXT_BUFFER (GTK_TEXT_VIEW (glimmer_buffer->view)->buffer);
	gtk_text_buffer_get_iter_at_offset (text_buffer, &start, offset);
	gtk_text_buffer_get_iter_at_offset (text_buffer, &end, offset + count);

	text =  gtk_text_buffer_get_slice (text_buffer, &start, &end, FALSE);
	(*buffer)->_buffer = CORBA_string_dup (text);
	g_free (text);
}

static void
impl_insert (PortableServer_Servant servant,
	     const CORBA_long       offset,
	     const CORBA_char      *str,
	     CORBA_Environment     *ev)
{
	GlimmerBuffer *buffer;
	GtkTextBuffer *text_buffer;
	GtkTextIter iter;

	buffer = GLIMMER_BUFFER (bonobo_object_from_servant (servant));
	bonobo_return_if_fail (buffer != NULL, ev);

	text_buffer = GTK_TEXT_BUFFER (GTK_TEXT_VIEW (buffer->view)->buffer);
	gtk_text_buffer_get_iter_at_offset (text_buffer, &iter, offset);
	gtk_text_buffer_begin_user_action (text_buffer);
	gtk_text_buffer_insert (text_buffer, &iter, str, strlen (str));
	gtk_text_buffer_end_user_action (text_buffer);
}

static void
impl_delete (PortableServer_Servant servant,
	     const CORBA_long       offset,
	     const CORBA_long       count,
	     CORBA_Environment     *ev)
{
	GlimmerBuffer *buffer;
	GtkTextBuffer *text_buffer;
	GtkTextIter start, end;

	buffer = GLIMMER_BUFFER (bonobo_object_from_servant (servant));
	bonobo_return_if_fail (buffer != NULL, ev);

	text_buffer = GTK_TEXT_BUFFER (GTK_TEXT_VIEW (buffer->view)->buffer);
	gtk_text_buffer_get_iter_at_offset (text_buffer, &start, offset);
	gtk_text_buffer_get_iter_at_offset (text_buffer, &end, offset + count);
	gtk_text_buffer_begin_user_action (text_buffer);
	gtk_text_buffer_delete (text_buffer, &start, &end);
	gtk_text_buffer_end_user_action (text_buffer);
}

/* ----------------------------------------------------------------------
 * Public interface 
 * ---------------------------------------------------------------------- */

GlimmerBuffer *
glimmer_buffer_new (GlimmerView *view)
{
	GlimmerBuffer *buffer;

	g_return_val_if_fail (view != NULL, NULL);
	g_return_val_if_fail (GLIMMER_IS_VIEW (view), NULL);

	buffer = GLIMMER_BUFFER (g_object_new (GLIMMER_TYPE_BUFFER, NULL));

	buffer->view = view;

	return buffer;
}

BONOBO_TYPE_FUNC_FULL (GlimmerBuffer,
		       GNOME_Development_EditorBuffer,
		       BONOBO_OBJECT_TYPE,
		       glimmer_buffer);
