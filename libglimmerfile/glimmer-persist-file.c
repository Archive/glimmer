/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  glimer-persist-file.c
 *
 *  Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.  
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <bonobo.h>
#include <gtk/gtk.h>

#include <libgnomevfs/gnome-vfs-types.h>
#include <libgnomevfs/gnome-vfs-ops.h>
#include <libgnomevfs/gnome-vfs-uri.h>
#include <libgnomevfs/gnome-vfs-init.h>
#include <libgnomevfs/gnome-vfs-file-info.h>
#include <libgnomevfs/gnome-vfs-handle.h>
#include <libgnomevfs/gnome-vfs-result.h>
#include <libgnomevfs/gnome-vfs-mime-utils.h>
#include <libgnomevfs/gnome-vfs-utils.h>

#include <gtksourceview/gtksourcebuffer.h>
#include <gtksourceview/gtksourceview.h>

#include "glimmer-persist-file.h"

/* Static functions */
static gboolean check_if_file_exists (gchar *path);
static gboolean create_file (gchar *filename);

static gboolean
check_if_file_exists (gchar *filename)
{
	GnomeVFSResult result;
	GnomeVFSFileInfo *finfo;
	gboolean exists = FALSE;

	finfo = gnome_vfs_file_info_new ();
	result = gnome_vfs_get_file_info (filename, finfo, GNOME_VFS_FILE_INFO_DEFAULT);

	if (result == GNOME_VFS_OK)
		if (finfo->type == GNOME_VFS_FILE_TYPE_REGULAR)
			exists = TRUE;

	gnome_vfs_file_info_unref (finfo);

	return exists;
}

static gboolean
create_file (gchar *filename)
{
	GnomeVFSHandle *handle;
	GnomeVFSResult result;
	gboolean created = FALSE;

	if ((result = gnome_vfs_create (&handle,
					filename,
					GNOME_VFS_OPEN_WRITE,
					FALSE,
					GNOME_VFS_PERM_USER_READ |
					GNOME_VFS_PERM_USER_WRITE |
					GNOME_VFS_PERM_GROUP_READ |
					GNOME_VFS_PERM_GROUP_WRITE |
					GNOME_VFS_PERM_OTHER_READ)) == GNOME_VFS_OK)
		created = TRUE;

	gnome_vfs_close (handle);

	return created;
}

static int
impl_save (BonoboPersistFile *pf,
	   const CORBA_char  *filename,
	   CORBA_Environment *ev,
	   void              *closure)
{
	GtkTextBuffer *buffer = GTK_TEXT_VIEW (closure)->buffer;
	GtkTextIter start;
	GtkTextIter end;
	gchar *all_text = NULL;
	GnomeVFSHandle *handle = NULL;
	GnomeVFSFileSize bytes = 0;
	GnomeVFSResult result;
	gint length = 0;
	gboolean retval = FALSE;

	gtk_text_buffer_get_start_iter (buffer, &start);
	gtk_text_buffer_get_end_iter (buffer, &end);
	all_text = gtk_text_iter_get_slice (&start, &end);
	length = all_text ? gtk_text_buffer_get_char_count (buffer) : 0;

	if (!check_if_file_exists ((gchar *) filename))
		create_file ((gchar *) filename);

	if ((gnome_vfs_open (&handle, (gchar *) filename, GNOME_VFS_OPEN_WRITE) == GNOME_VFS_OK)) {
		if (all_text)
			result = gnome_vfs_write (handle, (gconstpointer) all_text, length, &bytes);

		gnome_vfs_truncate_handle (handle, (GnomeVFSFileSize) length);
		gnome_vfs_close (handle);
		retval = TRUE;
	}

	g_free (all_text);

	gtk_text_buffer_set_modified (buffer, FALSE);

	return retval;
}

static int
impl_load (BonoboPersistFile *pf,
	   const CORBA_char  *filename,
	   CORBA_Environment *ev,
	   void              *closure)
{
	GtkTextView *view = GTK_TEXT_VIEW (closure);
	GtkTextBuffer *buffer = view->buffer;
	GtkTextIter start;
	GtkTextIter end;
	gchar *uri, *mime_type;
	GnomeVFSHandle *handle;
	gpointer buf;
	GnomeVFSFileSize bytes;
	gint read_size = 64 *1024;
	gboolean retval = FALSE;

	if (g_path_is_absolute (filename)) {
		uri = gnome_vfs_get_uri_from_local_path (filename);
	} else {
		gchar *curdir, *path;

		curdir = g_get_current_dir ();
		path = g_strconcat (curdir, "/", filename, NULL);
		g_free (curdir);
		uri = gnome_vfs_get_uri_from_local_path (path);
		g_free (path);
	}

	mime_type = gnome_vfs_get_mime_type (uri);
	g_free (uri);
	if (mime_type) {
		glimmer_view_set_mime_type (GLIMMER_VIEW (view), mime_type);
		g_free (mime_type);
	}

	gtk_source_buffer_begin_not_undoable_action (GTK_SOURCE_BUFFER (buffer));

	gtk_text_buffer_get_start_iter (buffer, &start);
	gtk_text_buffer_get_end_iter (buffer, &end);
	gtk_text_buffer_delete (buffer, &start, &end);

	if ((gnome_vfs_open (&handle, (gchar *) filename, GNOME_VFS_OPEN_READ) == GNOME_VFS_OK)) {
		buf = g_new (char, read_size);

		while ((gnome_vfs_read (handle, buf, read_size, &bytes) == GNOME_VFS_OK)) {
			gtk_text_buffer_get_end_iter (buffer, &end);
			gtk_text_buffer_insert (buffer, &end, (gchar *) buf, bytes);
			if (bytes < read_size)
				break;
		}
		g_free (buf);
		gnome_vfs_close (handle);
	}

	gtk_text_buffer_get_start_iter (buffer, &start);
	gtk_text_buffer_place_cursor (buffer, &start);
	gtk_text_view_place_cursor_onscreen (view);
	gtk_text_buffer_set_modified (buffer, FALSE);

	gtk_source_buffer_end_not_undoable_action (GTK_SOURCE_BUFFER (buffer));

	return retval;
}

BonoboPersistFile *
glimmer_persist_file_new (GlimmerView *view)
{
	return bonobo_persist_file_new ((BonoboPersistFileIOFn) impl_load,
					(BonoboPersistFileIOFn) impl_save,
					"GlimmerPersistFile",
					view);
}
