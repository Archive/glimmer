/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  glimmer-style-scheme.h
 *
 *  Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GLIMMER_STYLE_SCHEME_H__
#define __GLIMMER_STYLE_SCHEME_H__

#include "glimmer-themes.h"
#include <gtksourceview/gtksourcelanguage.h>

G_BEGIN_DECLS

#define GLIMMER_TYPE_STYLE_SCHEME		(glimmer_style_scheme_get_type ())
#define GLIMMER_STYLE_SCHEME(o)			(G_TYPE_CHECK_INSTANCE_CAST ((o), GLIMMER_TYPE_STYLE_SCHEME, GlimmerStyleScheme))
#define GLIMMER_STYLE_SCHEME_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), GLIMMER_TYPE_STYLE_SCHEME, GlimmerStyleSchemeClass))
#define GLIMMER_IS_STYLE_SCHEME(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), GLIMMER_TYPE_STYLE_SCHEME))
#define GLIMMER_IS_STYLE_SCHEME_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), GLIMMER_TYPE_STYLE_SCHEME))

typedef struct _GlimmerStyleScheme        GlimmerStyleScheme;
typedef struct _GlimmerStyleSchemePrivate GlimmerStyleSchemePrivate;
typedef struct _GlimmerStyleSchemeClass   GlimmerStyleSchemeClass;

struct _GlimmerStyleScheme {
	GObject parent;

	GlimmerStyleSchemePrivate *priv;
};

struct _GlimmerStyleSchemeClass {
	GObjectClass parent_class;
};

GType               glimmer_style_scheme_get_type      (void);

GlimmerStyleScheme *glimmer_style_scheme_new           (GlimmerThemes      *themes,
							GtkSourceLanguage  *language,
							const gchar        *theme);
GlimmerStyleScheme *glimmer_style_scheme_new_default   (GlimmerThemes      *themes);

GtkSourceLanguage  *glimmer_style_scheme_get_language  (GlimmerStyleScheme *scheme);
void                glimmer_style_scheme_set_language  (GlimmerStyleScheme *scheme,
							GtkSourceLanguage  *language);

gchar              *glimmer_style_scheme_get_theme     (GlimmerStyleScheme *scheme);
void                glimmer_style_scheme_set_theme     (GlimmerStyleScheme *scheme,
							const gchar        *name);

void                glimmer_style_scheme_set_tag_style (GlimmerStyleScheme *scheme,
							const gchar        *style_name,
							const GtkSourceTagStyle *style);

#endif /* __GLIMMER_STYLE_SCHEME_H__ */
