/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  glimmer-dialogs.h
 *
 *  Copyright (C) 2002, 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GLIMMER_DIALOGS_H__
#define __GLIMMER_DIALOGS_H__

#include "glimmer-view.h"

G_BEGIN_DECLS

void glimmer_dialog_goto_line   (GlimmerView *view);
void glimmer_dialog_find        (GlimmerView *view);
void glimmer_dialog_replace     (GlimmerView *view);
void glimmer_dialog_preferences (GlimmerView *view);
void glimmer_dialog_page_setup  (GlimmerView *view);

G_END_DECLS

#endif /* __GLIMMER_DIALOGS_H__ */
