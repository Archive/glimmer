/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  glimmer-dialog-replace.c
 *
 *  Copyright (C) 2002, 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <gconf/gconf-client.h>
#include <glade/glade-xml.h>
#include <libgnome/libgnome.h>
#include <gtksourceview/gtksourceiter.h>
#include "glimmer-dialogs.h"
#include "glimmer-settings.h"
#include "glimmer-utils.h"

#define GLIMMER_RESPONSE_FIND		101
#define GLIMMER_RESPONSE_REPLACE	102
#define GLIMMER_RESPONSE_REPLACE_ALL	103

typedef struct _GlimmerDialogReplace GlimmerDialogReplace;

struct _GlimmerDialogReplace {
	GtkWidget *dialog;
	GtkWidget *search_combo;
	GtkWidget *replace_combo;
	GtkWidget *match_case;
	GtkWidget *match_word;
	GtkWidget *wrap_around;
	GtkWidget *direction_forward;
};

static void dialog_replace_response_handler	   (GtkDialog            *dlg,
						    gint                  res_id,
						    GlimmerDialogReplace *replace_dialog);
static void dialog_replace_load_settings	   (GlimmerView          *view,
						    GlimmerDialogReplace *dialog);
static void dialog_replace_save_settings	   (GlimmerView          *view,
						    GlimmerDialogReplace *dialog);

static void replace_dlg_find_button_pressed	   (GlimmerView          *view,
						    GlimmerDialogReplace *dialog);
static void replace_dlg_replace_button_pressed     (GlimmerView          *view,
						    GlimmerDialogReplace *dialog);
static void replace_dlg_replace_all_button_pressed (GlimmerView          *view,
						    GlimmerDialogReplace *dialog);

static GlimmerDialogReplace *dialog_replace_get_dialog (GlimmerView *view);

#if 0
static void
dialog_destroyed (GtkObject *obj,
		  void     **dialog_pointer)
{
	if (dialog_pointer != NULL) {
		g_free (*dialog_pointer);
		*dialog_pointer = NULL;
	}
}
#endif

static void
dialog_replace_response_handler (GtkDialog            *dlg,
				 gint                  res_id,
				 GlimmerDialogReplace *replace_dialog)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dlg), "GlimmerView");

	switch (res_id) {
		case GLIMMER_RESPONSE_FIND:
			replace_dlg_find_button_pressed (view, replace_dialog);
			break;
		case GLIMMER_RESPONSE_REPLACE:
			replace_dlg_replace_button_pressed (view, replace_dialog);
			break;
		case GLIMMER_RESPONSE_REPLACE_ALL:
			replace_dlg_replace_all_button_pressed (view, replace_dialog);
			break;
		default:
			dialog_replace_save_settings (view, replace_dialog);
			gtk_widget_destroy (replace_dialog->dialog);
	}
}

static void
dialog_replace_load_settings (GlimmerView *view,
			      GlimmerDialogReplace *dialog)
{
	GConfClient *client;
	GSList *history_list;

	client = gconf_client_get_default ();
	g_assert (client != NULL);

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->wrap_around),
				      gconf_client_get_bool (client,
							     GLIMMER_BASE_KEY GLIMMER_SETTING_FIND_WRAP_AROUND,
							     NULL));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->match_case),
				      gconf_client_get_bool (client,
							     GLIMMER_BASE_KEY GLIMMER_SETTING_FIND_MATCH_CASE,
							     NULL));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->match_word),
				      gconf_client_get_bool (client,
							     GLIMMER_BASE_KEY GLIMMER_SETTING_FIND_MATCH_WORD,
							     NULL));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->direction_forward),
				      gconf_client_get_bool (client,
							     GLIMMER_BASE_KEY GLIMMER_SETTING_FIND_DIRECTION,
							     NULL));

	history_list = gconf_client_get_list (client,
					      GLIMMER_BASE_KEY GLIMMER_SETTING_FIND_HISTORY,
					      GCONF_VALUE_STRING,
					      NULL);
	if (history_list)
		gtk_combo_set_popdown_strings (GTK_COMBO (dialog->search_combo),
					       (GList *)history_list);
	g_object_set_data (G_OBJECT (view), "find-history", history_list);

	history_list = gconf_client_get_list (client,
					      GLIMMER_BASE_KEY GLIMMER_SETTING_REPLACE_HISTORY,
					      GCONF_VALUE_STRING,
					      NULL);
	if (history_list)
		gtk_combo_set_popdown_strings (GTK_COMBO (dialog->replace_combo),
					       (GList *)history_list);
	g_object_set_data (G_OBJECT (view), "replace-history", history_list);

	g_object_unref (G_OBJECT (client));
}

static void
dialog_replace_save_settings (GlimmerView *view,
			      GlimmerDialogReplace *dialog)
{
	GConfClient *client;
	int max_history;
	GSList *history_list, *l;
	const char *search_string;
	const char *replace_string;

	client = gconf_client_get_default ();
	g_assert (client != NULL);

	gconf_client_set_bool (client, GLIMMER_BASE_KEY GLIMMER_SETTING_FIND_WRAP_AROUND,
			       gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->wrap_around)),
			       NULL);
	gconf_client_set_bool (client, GLIMMER_BASE_KEY GLIMMER_SETTING_FIND_MATCH_WORD,
			       gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->match_word)),
			       NULL);
	gconf_client_set_bool (client, GLIMMER_BASE_KEY GLIMMER_SETTING_FIND_MATCH_CASE,
			       gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->match_case)),
			       NULL);
	gconf_client_set_bool (client, GLIMMER_BASE_KEY GLIMMER_SETTING_FIND_DIRECTION,
			       gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->direction_forward)),
			       NULL);

	/* Check if search string is already in history, and add it if not. */
	max_history = gconf_client_get_int (client,
					    GLIMMER_BASE_KEY GLIMMER_SETTING_HISTORY_ITEMS,
					    NULL);
	history_list = g_object_get_data (G_OBJECT (view), "find-history");
	search_string = gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (dialog->search_combo)->entry));

	if (strlen (search_string) > 0) {
		l = g_slist_find_custom (history_list, search_string, (GCompareFunc)strcmp);
		if (!l) {
			history_list = g_slist_prepend (history_list, g_strdup (search_string));

			/* Delete excess history. */
			if (g_slist_length (history_list) > max_history) {
				gchar *key = g_slist_last (history_list)->data;
				history_list = g_slist_remove_link (history_list,
								    g_slist_last (history_list));
				g_free (key);
			}
		} else {
			history_list = g_slist_remove_link (history_list, l);
			history_list = g_slist_concat (l, history_list);
		}

		gconf_client_set_list (client, GLIMMER_BASE_KEY GLIMMER_SETTING_FIND_HISTORY,
				       GCONF_VALUE_STRING, history_list, NULL);

		g_object_set_data (G_OBJECT (view), "find-history", history_list);
	}

	/* Check if search string is already in history, and add it if not. */
	history_list = g_object_get_data (G_OBJECT (view), "replace-history");
	replace_string = gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (dialog->replace_combo)->entry));

	if (strlen (replace_string) > 0) {
		l = g_slist_find_custom (history_list, replace_string, (GCompareFunc)strcmp);
		if (!l) {
			history_list = g_slist_prepend (history_list, g_strdup (replace_string));

			/* Delete excess history. */
			if (g_slist_length (history_list) > max_history) {
				gchar *key = g_slist_last (history_list)->data;
				history_list = g_slist_remove_link (history_list,
								    g_slist_last (history_list));
				g_free (key);
			}
		} else {
			history_list = g_slist_remove_link (history_list, l);
			history_list = g_slist_concat (l, history_list);
		}

		gconf_client_set_list (client, GLIMMER_BASE_KEY GLIMMER_SETTING_REPLACE_HISTORY,
				       GCONF_VALUE_STRING, history_list, NULL);
	}

	g_object_unref (G_OBJECT (client));
}

static void
replace_dlg_find_button_pressed (GlimmerView *view,
				 GlimmerDialogReplace *dialog)
{
	GtkTextBuffer *buffer = GTK_TEXT_VIEW (view)->buffer;
	gchar *search_string;
	gboolean match_word, forward_search, wrap_around, found;
	GtkTextIter iter, wrap, match_start, match_end;
	GtkSourceSearchFlags search_flags;
	GtkWidget *window, *message_dlg;

	search_string = g_strdup (gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (dialog->search_combo)->entry)));
	g_return_if_fail (search_string != NULL);

	if (strlen (search_string) <= 0)
		return;

	search_flags = GTK_SOURCE_SEARCH_VISIBLE_ONLY | GTK_SOURCE_SEARCH_TEXT_ONLY;
	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->match_case)))
		search_flags |= GTK_SOURCE_SEARCH_CASE_INSENSITIVE;

	match_word = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->match_word));
	wrap_around = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->wrap_around));
	forward_search = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->direction_forward));

	if (forward_search) {
		gtk_text_buffer_get_iter_at_mark (buffer, &iter,
						  gtk_text_buffer_get_mark (buffer,
									    "selection_bound"));
	} else {
		gtk_text_buffer_get_iter_at_mark (buffer, &iter,
						  gtk_text_buffer_get_insert (buffer));
	}

	found = FALSE;
	if (forward_search) {
		found = gtk_source_iter_forward_search (&iter, search_string,
							search_flags, &match_start,
							&match_end, NULL);
		if (found && match_word) {
			found = gtk_text_iter_starts_word (&match_start) &&
				gtk_text_iter_ends_word (&match_end);
		}
		if (!found && wrap_around) {
			gtk_text_buffer_get_start_iter (buffer, &wrap);
			found = gtk_source_iter_forward_search (&wrap, search_string,
								search_flags, &match_start,
								&match_end, &iter);
		}
	} else {
		found = gtk_source_iter_backward_search (&iter, search_string,
							 search_flags, &match_start,
							 &match_end, NULL);
		if (found && match_word) {
			found = gtk_text_iter_starts_word (&match_start) &&
				gtk_text_iter_ends_word (&match_end);
		}
		if (!found && wrap_around) {
			gtk_text_buffer_get_end_iter (buffer, &wrap);
			found = gtk_source_iter_backward_search (&wrap, search_string,
								 search_flags, &match_start,
								 &match_end, &iter);
		}
	}

	if (found) {
		/* Highlight text found. */
		gtk_text_buffer_place_cursor (buffer, &match_start);

		gtk_text_buffer_move_mark_by_name (buffer,
						   "selection_bound",
						   &match_end);

		/* Scroll to text. */
		gtk_text_view_scroll_to_mark (GTK_TEXT_VIEW (view),
					      gtk_text_buffer_get_insert (buffer),
					      0.0, TRUE, 0.5, 0.5);
	}

	if (!found) {
		/* Unable to get toplevel GtkWindow if part of out-of-process Bonobo component. */
		window = gtk_widget_get_toplevel (GTK_WIDGET (view));
		if (!GTK_WIDGET_TOPLEVEL (window))
			window = NULL;

		message_dlg = gtk_message_dialog_new (
				GTK_WINDOW (window),
				GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_INFO,
				GTK_BUTTONS_OK,
				_("The text \"%s\" was not found."), search_string);
		gtk_dialog_set_default_response (GTK_DIALOG (message_dlg), GTK_RESPONSE_OK);
		gtk_window_set_resizable (GTK_WINDOW (message_dlg), FALSE);

		gtk_dialog_run (GTK_DIALOG (message_dlg));
		gtk_widget_destroy (message_dlg);
	}

	gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog->dialog), 
					   GLIMMER_RESPONSE_REPLACE,
					   found);

	g_free (search_string);
}

static void
replace_dlg_replace_button_pressed (GlimmerView *view,
				    GlimmerDialogReplace *dialog)
{
	GtkTextBuffer *buffer = GTK_TEXT_VIEW (view)->buffer;
	gchar *search_string, *replace_string;
	GtkTextIter iter, wrap, sel_bound, match_start, match_end;
	GtkSourceSearchFlags search_flags;
	gboolean match_word, forward_search, wrap_around, found;

	search_string = g_strdup (gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (dialog->search_combo)->entry)));
	replace_string = g_strdup (gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (dialog->replace_combo)->entry)));

	if (strlen (search_string) <= 0 || strlen (replace_string) <= 0)
		return;

	gtk_text_buffer_get_iter_at_mark (buffer,
					  &iter,
					  gtk_text_buffer_get_mark (buffer,
								    "insert"));
	gtk_text_buffer_get_iter_at_mark (buffer,
					  &sel_bound,
					  gtk_text_buffer_get_mark (buffer,
								    "selection_bound"));
	gtk_text_iter_order (&iter, &sel_bound);

	gtk_text_buffer_delete (buffer,
				&iter,
				&sel_bound);
	gtk_text_buffer_insert (buffer,
				&iter,
				replace_string,
				strlen (replace_string));
	g_free (replace_string);

	search_flags = GTK_SOURCE_SEARCH_VISIBLE_ONLY | GTK_SOURCE_SEARCH_TEXT_ONLY;
	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->match_case)))
		search_flags |= GTK_SOURCE_SEARCH_CASE_INSENSITIVE;

	match_word = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->match_word));
	wrap_around = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->wrap_around));
	forward_search = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->direction_forward));

	if (forward_search) {
		gtk_text_buffer_get_iter_at_mark (buffer, &iter,
						  gtk_text_buffer_get_mark (buffer,
									    "selection_bound"));
	} else {
		gtk_text_buffer_get_iter_at_mark (buffer, &iter,
						  gtk_text_buffer_get_insert (buffer));
	}

	/* Go ahead and find the next one. */
	found = FALSE;
	if (forward_search) {
		found = gtk_source_iter_forward_search (&iter, search_string,
							search_flags, &match_start,
							&match_end, NULL);
		if (found && match_word) {
			found = gtk_text_iter_starts_word (&match_start) &&
				gtk_text_iter_ends_word (&match_end);
		}
		if (!found && wrap_around) {
			gtk_text_buffer_get_start_iter (buffer, &wrap);
			found = gtk_source_iter_forward_search (&wrap, search_string,
								search_flags, &match_start,
								&match_end, &iter);
		}
	} else {
		found = gtk_source_iter_backward_search (&iter, search_string,
							 search_flags, &match_start,
							 &match_end, NULL);
		if (found && match_word) {
			found = gtk_text_iter_starts_word (&match_start) &&
				gtk_text_iter_ends_word (&match_end);
		}
		if (!found && wrap_around) {
			gtk_text_buffer_get_end_iter (buffer, &wrap);
			found = gtk_source_iter_backward_search (&wrap, search_string,
								 search_flags, &match_start,
								 &match_end, &iter);
		}
	}

	if (found) {
		/* Highlight text found. */
		gtk_text_buffer_place_cursor (buffer, &match_start);

		gtk_text_buffer_move_mark_by_name (buffer,
						   "selection_bound",
						   &match_end);

		/* Scroll to text. */
		gtk_text_view_scroll_to_mark (GTK_TEXT_VIEW (view),
					      gtk_text_buffer_get_insert (buffer),
					      0.0, TRUE, 0.5, 0.5);
	}

	gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog->dialog), 
					   GLIMMER_RESPONSE_REPLACE, found);

	g_free (search_string);
}

static void
replace_dlg_replace_all_button_pressed (GlimmerView *view,
					GlimmerDialogReplace *dialog)
{
	GtkTextBuffer *buffer = GTK_TEXT_VIEW (view)->buffer;
	gchar *search_string, *replace_string;
	GtkTextIter iter, match_start, match_end;
	GtkSourceSearchFlags search_flags;
	GtkWidget *window, *message_dlg;
	gboolean match_word, found;
	int count = 0;

	search_string = g_strdup (gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (dialog->search_combo)->entry)));
	replace_string = g_strdup (gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (dialog->replace_combo)->entry)));

	if (strlen (search_string) <= 0 || strlen (replace_string) <= 0)
		return;

	search_flags = GTK_SOURCE_SEARCH_VISIBLE_ONLY | GTK_SOURCE_SEARCH_TEXT_ONLY;
	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->match_case)))
		search_flags |= GTK_SOURCE_SEARCH_CASE_INSENSITIVE;

	match_word = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->match_word));

	gtk_text_buffer_get_start_iter (buffer, &iter);

	do {
		found = gtk_source_iter_forward_search (&iter, search_string,
							search_flags, &match_start,
							&match_end, NULL);
		if (found && match_word) {
			found = gtk_text_iter_starts_word (&match_start) &&
				gtk_text_iter_ends_word (&match_end);
			/* If an entire word hasn't been found, continue the search. */
			if (!found) {
				iter = match_end;
				found = TRUE;
				continue;
			}
		}

		if (found) {
			gtk_text_buffer_delete (buffer,
						&match_start,
						&match_end);
			gtk_text_buffer_insert (buffer,
						&match_start,
						replace_string,
						strlen (replace_string));
			iter = match_start;
			count++;
		}
	} while (found);

	/* Unable to get toplevel GtkWindow if part of out-of-process Bonobo component. */
	window = gtk_widget_get_toplevel (GTK_WIDGET (view));
	if (!GTK_WIDGET_TOPLEVEL (window))
		window = NULL;

	if (count == 0) {
		message_dlg = gtk_message_dialog_new (
			GTK_WINDOW (window),
			GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_INFO,
			GTK_BUTTONS_OK,
			_("The text \"%s\" was not found."), search_string);
	} else {
		message_dlg = gtk_message_dialog_new (
			GTK_WINDOW (window),
			GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_INFO,
			GTK_BUTTONS_OK,
			_("Found and replaced %d occurences."), count);
	}

	gtk_dialog_set_default_response (GTK_DIALOG (message_dlg), GTK_RESPONSE_OK);
	gtk_window_set_resizable (GTK_WINDOW (message_dlg), FALSE);

	gtk_dialog_run (GTK_DIALOG (message_dlg));
	gtk_widget_destroy (message_dlg);

	g_free (search_string);
	g_free (replace_string);
}

static GlimmerDialogReplace *
dialog_replace_get_dialog (GlimmerView *view)
{
	static GlimmerDialogReplace *dialog = NULL;
	GladeXML *gui;
	GtkWindow *window;
	GtkWidget *vbox, *button, *toplevel;

	gui = glade_xml_new (GLADEDIR "dialogs.glade", "replace-vbox", NULL);
	if (!gui) {
		g_warning ("Could not find dialogs.glade, reinstall glimmer");
		return NULL;
	}

	/* Unable to get toplevel GtkWindow if part of out-of-process Bonobo component. */
	toplevel = gtk_widget_get_toplevel (GTK_WIDGET (view));
	if (!GTK_WIDGET_TOPLEVEL (toplevel))
		window = NULL;
	else
		window = GTK_WINDOW (toplevel);

	dialog = g_new0 (GlimmerDialogReplace, 1);
	dialog->dialog = gtk_dialog_new_with_buttons (_("Replace"),
						      window,
						      GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_NO_SEPARATOR,
						      GTK_STOCK_CLOSE,
						      GTK_RESPONSE_CLOSE,
						      NULL);
	g_return_val_if_fail (dialog->dialog != NULL, NULL);
	gtk_container_set_border_width (GTK_CONTAINER (dialog->dialog), 5);
	gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dialog->dialog)->vbox), 2);

	g_object_set_data (G_OBJECT (dialog->dialog), "GlimmerView", view);

	/* Add Replace All button */
	gtk_dialog_add_button (GTK_DIALOG (dialog->dialog), 
			       _("Replace _All"), GLIMMER_RESPONSE_REPLACE_ALL);

	/* Add Replace button */
	button = glimmer_button_new_with_stock_image (_("_Replace"), GTK_STOCK_FIND_AND_REPLACE);
	g_return_val_if_fail (button != NULL, NULL);

	GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);

	gtk_widget_show (button);

	gtk_dialog_add_action_widget (GTK_DIALOG (dialog->dialog), 
				      button, GLIMMER_RESPONSE_REPLACE);

	/* Add Find button */
	gtk_dialog_add_button (GTK_DIALOG (dialog->dialog), 
			       GTK_STOCK_FIND, GLIMMER_RESPONSE_FIND);

	vbox = glade_xml_get_widget (gui, "replace-vbox");
	dialog->search_combo      = glade_xml_get_widget (gui, "search-combo");
	dialog->replace_combo     = glade_xml_get_widget (gui, "replace-combo");
	dialog->match_case        = glade_xml_get_widget (gui, "match-case-checkbutton");
	dialog->match_word        = glade_xml_get_widget (gui, "match-word-checkbutton");
	dialog->wrap_around       = glade_xml_get_widget (gui, "wrap-around-checkbutton");
	dialog->direction_forward = glade_xml_get_widget (gui, "direction-forward-radiobutton");
	g_object_unref (G_OBJECT (gui));

	if (!vbox || !dialog->search_combo || !dialog->replace_combo || 
	    !dialog->match_case || !dialog->match_word || !dialog->wrap_around ||
	    !dialog->direction_forward) {
		g_error ("Could not find the required widgets inside dialogs.glade");
		return NULL;
	}

	gtk_combo_disable_activate (GTK_COMBO (dialog->search_combo));
	gtk_combo_disable_activate (GTK_COMBO (dialog->replace_combo));

	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog->dialog)->vbox),
			    vbox, FALSE, FALSE, 0);

	gtk_dialog_set_default_response (GTK_DIALOG (dialog->dialog),
					 GLIMMER_RESPONSE_FIND);

	/*g_signal_connect (G_OBJECT (dialog->dialog), "destroy",
			  G_CALLBACK (dialog_destroyed), &dialog);*/
	g_signal_connect (G_OBJECT (dialog->dialog), "response",
			  G_CALLBACK (dialog_replace_response_handler), dialog);

	gtk_window_set_resizable (GTK_WINDOW (dialog->dialog), FALSE);

	return dialog;
}

void
glimmer_dialog_replace (GlimmerView *view)
{
	GlimmerDialogReplace *dialog;
	GtkWidget *window;

	dialog = g_object_get_data (G_OBJECT (view), "replace-dialog");
	if (dialog == NULL) {
		dialog = dialog_replace_get_dialog (view);
		if (dialog == NULL) {
			g_warning ("Could not create the Replace dialog");
			return;
		}
	}

	dialog_replace_load_settings (view, dialog);

	window = gtk_widget_get_toplevel (GTK_WIDGET (view));
	if (GTK_WIDGET_TOPLEVEL (window))
		gtk_window_set_transient_for (GTK_WINDOW (dialog->dialog),
					      GTK_WINDOW (window));

	gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog->dialog), 
					   GLIMMER_RESPONSE_REPLACE, FALSE);

	gtk_window_present (GTK_WINDOW (dialog->dialog));
	gtk_widget_grab_focus (GTK_COMBO (dialog->search_combo)->entry);
}
