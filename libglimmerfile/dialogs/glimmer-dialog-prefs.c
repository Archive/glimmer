/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  glimmer-dialog-prefs.c
 *
 *  Copyright (C) 2002, 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <gconf/gconf-client.h>
#include <glade/glade-xml.h>
#include <libgnome/libgnome.h>
#include <libgnomeui/gnome-font-picker.h>
#include <libgnomeui/gnome-color-picker.h>
#include "glimmer-settings.h"
#include "glimmer-utils.h"
#include "glimmer-dialogs.h"
#include "glimmer-style-scheme.h"

enum {
	COLUMN_NAME,
	COLUMN_PIXBUF,
	COLUMN_ACTIVE,
	COLUMN_CALLBACK,
	COLUMN_USERDATA,
	LAST_COL
};

typedef struct _GlimmerDialogPrefs GlimmerDialogPrefs;

struct _GlimmerDialogPrefs {
	GtkWidget       *dialog;

	/* Editor. */
	GtkWidget       *keymap;
	GtkWidget       *customize;

	GtkWidget       *limit_undo;
	GtkWidget       *undo_levels;

	GtkWidget       *options;
	GtkTreeStore    *options_model;

	/* Display. */
	GtkWidget       *default_font;
	GtkWidget       *fontpicker;

	GtkWidget       *right_margin;
	GtkWidget       *margin_pos;
	
	GtkWidget       *tab_width;
	
	GtkWidget       *wrap_text;
	GtkWidget       *word_wrap;

	/* Color. */
	GtkWidget       *themes;
	GtkWidget	*languages;

	GtkListStore    *styles_model;
	GtkWidget       *styles;
	GtkWidget       *style_bold;
	GtkWidget       *style_italic;
	GtkWidget	*use_foreground;
	GtkWidget       *style_foreground;
	GtkWidget	*use_background;
	GtkWidget       *style_background;
	GtkWidget       *defaults;
	gboolean	 style_update;
};

typedef void (* GlimmerOptionCallback) (GlimmerDialogPrefs *dialog,
					gboolean            active,
					gpointer            data);

static void dialog_prefs_save_settings (GlimmerView        *view,
					GlimmerDialogPrefs *dialog);


/*static void
dialog_destroyed (GtkObject *obj,
		  void     **dialog_pointer)
{
	if (dialog_pointer != NULL) {
		g_free (*dialog_pointer);
		*dialog_pointer = NULL;
	}
}*/

static void
dialog_response_handler (GtkDialog          *dlg,
			 gint                response_id,
			 GlimmerDialogPrefs *dialog)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dlg), "GlimmerView");

	switch (response_id) {
		case GTK_RESPONSE_HELP:
			g_warning ("FIXME: Implement help");
			break;
		default:
			dialog_prefs_save_settings (view, dialog);
			gtk_widget_destroy (dialog->dialog);
			break;
	}
}

static void
dialog_prefs_load_settings (GlimmerView *view,
			    GlimmerDialogPrefs *dialog)
{
	GConfClient *client;
	gchar *str;
	GtkWrapMode wrap_mode;
	gboolean limit_undo;
	gboolean show_margin;

	client = gconf_client_get_default ();

	/* Editor. */
	limit_undo = gconf_client_get_bool (client,
					    GLIMMER_BASE_KEY GLIMMER_SETTING_LIMIT_UNDO,
					    NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->limit_undo), limit_undo);
	gtk_widget_set_sensitive (dialog->undo_levels, limit_undo);

	gtk_spin_button_set_value (GTK_SPIN_BUTTON (dialog->undo_levels),
		gconf_client_get_int (client,
				      GLIMMER_BASE_KEY GLIMMER_SETTING_UNDO_LEVELS,
				      NULL));

	/* Display. */
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->default_font),
		gconf_client_get_bool (client,
				       GLIMMER_BASE_KEY GLIMMER_SETTING_DEFAULT_FONT,
				       NULL));
	str = gconf_client_get_string (client, GLIMMER_BASE_KEY
				       GLIMMER_SETTING_FONT, NULL);
	gnome_font_picker_set_font_name (GNOME_FONT_PICKER (dialog->fontpicker),
					 str);
	g_free (str);

	show_margin = gconf_client_get_bool (client,
				 	     GLIMMER_BASE_KEY GLIMMER_SETTING_SHOW_MARGIN,
					     NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->right_margin),
				      show_margin);
				      
	gtk_widget_set_sensitive (dialog->margin_pos, show_margin);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (dialog->margin_pos),
		gconf_client_get_int (client,
				      GLIMMER_BASE_KEY GLIMMER_SETTING_MARGIN,
				      NULL));

	gtk_spin_button_set_value (GTK_SPIN_BUTTON (dialog->tab_width),
		gconf_client_get_int (client,
				      GLIMMER_BASE_KEY GLIMMER_SETTING_TAB_WIDTH,
				      NULL));

	str = gconf_client_get_string (client,
				       GLIMMER_BASE_KEY GLIMMER_SETTING_WRAP_MODE,
				       NULL);
	wrap_mode = glimmer_util_get_wrap_mode_from_string (str);
	if (wrap_mode == GTK_WRAP_NONE) {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->wrap_text),
					      FALSE);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->word_wrap),
					      FALSE);
	} else if (wrap_mode == GTK_WRAP_WORD) {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->wrap_text),
					      TRUE);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->word_wrap),
					      TRUE);
	} else {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->wrap_text),
					      TRUE);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->word_wrap),
					      FALSE);
	}
	g_free (str);

	g_object_unref (G_OBJECT (client));
}

static void
dialog_prefs_save_settings (GlimmerView *view,
			    GlimmerDialogPrefs *dialog)
{
	GConfClient *client;
	gchar *font;
	GtkWrapMode wrap_mode;
	const gchar *wrap;

	client = gconf_client_get_default ();

	/* Editor. */
	gconf_client_set_bool (client,
			       GLIMMER_BASE_KEY GLIMMER_SETTING_LIMIT_UNDO,
			       glimmer_view_get_limit_undo (view), NULL);
	gconf_client_set_int (client,
			      GLIMMER_BASE_KEY GLIMMER_SETTING_UNDO_LEVELS,
			      glimmer_view_get_max_undo_levels (view), NULL);
	gconf_client_set_bool (client,
			       GLIMMER_BASE_KEY GLIMMER_SETTING_AUTO_INDENT,
			       gtk_source_view_get_auto_indent (GTK_SOURCE_VIEW (view)),
			       NULL);
	gconf_client_set_bool (client,
			       GLIMMER_BASE_KEY GLIMMER_SETTING_LINE_NUMBERS,
			       gtk_source_view_get_show_line_numbers (GTK_SOURCE_VIEW (view)),
			       NULL);
	gconf_client_set_bool (client,
			       GLIMMER_BASE_KEY GLIMMER_SETTING_LINE_PIXMAPS,
			       gtk_source_view_get_show_line_markers (GTK_SOURCE_VIEW (view)),
			       NULL);

	/* Display. */
	gconf_client_set_bool (client,
			       GLIMMER_BASE_KEY GLIMMER_SETTING_DEFAULT_FONT,
			       glimmer_view_get_use_default_font (view), NULL);
	font = glimmer_view_get_font (view);
	gconf_client_set_string (client,
				 GLIMMER_BASE_KEY GLIMMER_SETTING_FONT,
				 font, NULL);
	g_free (font);

	gconf_client_set_bool (client,
			       GLIMMER_BASE_KEY GLIMMER_SETTING_SHOW_MARGIN,
			       gtk_source_view_get_show_margin (GTK_SOURCE_VIEW (view)),
			       NULL);
	gconf_client_set_int (client,
			      GLIMMER_BASE_KEY GLIMMER_SETTING_MARGIN,
			      gtk_source_view_get_margin (GTK_SOURCE_VIEW (view)),
			      NULL);
	gconf_client_set_int (client,
			      GLIMMER_BASE_KEY GLIMMER_SETTING_TAB_WIDTH,
			      gtk_source_view_get_tabs_width (GTK_SOURCE_VIEW (view)),
			      NULL);				      
	wrap_mode = gtk_text_view_get_wrap_mode (GTK_TEXT_VIEW (view));
	if (wrap_mode == GTK_WRAP_NONE)
		wrap = "GTK_WRAP_NONE";
	else if (wrap_mode == GTK_WRAP_WORD)
		wrap = "GTK_WRAP_WORD";
	else if (wrap_mode == GTK_WRAP_CHAR)
		wrap = "GTK_WRAP_CHAR";
	else
		wrap = "";
	gconf_client_set_string (client,
				 GLIMMER_BASE_KEY GLIMMER_SETTING_WRAP_MODE,
				 wrap, NULL);

	g_object_unref (G_OBJECT (client));
}

#if 0
static void
dialog_prefs_save_styles (GConfClient *client,
			  GlimmerView *view)
{
	char *theme;
	int i;
	GlimmerStyle *style;

	theme = gconf_client_get_string (client,
					 GLIMMER_BASE_KEY GLIMMER_SETTING_COLOR_THEME,
					 NULL);

	for (i = 0; style_strings[i]; i++) {
		style = glimmer_style_cache_get_style_from_class (style_strings[i]);

		if (style->modified) {
			gchar *key, *color;

			/* Bold setting. */
			key = g_strconcat (GLIMMER_BASE_KEY,
					   GLIMMER_THEMES_KEY,
					   "/", theme, "/",
					   style_strings[i],
					   "_bold",
					   NULL);

			gconf_client_set_bool (client,
					       key,
					       style->bold,
					       NULL);

			g_free (key);

			/* Italic setting. */
			key = g_strconcat (GLIMMER_BASE_KEY,
					   GLIMMER_THEMES_KEY,
					   "/", theme, "/",
					   style_strings[i],
					   "_italic",
					   NULL);

			gconf_client_set_bool (client,
					       key,
					       style->italic,
					       NULL);

			g_free (key);

			/* Foreground color. */
			key = g_strconcat (GLIMMER_BASE_KEY,
					   GLIMMER_THEMES_KEY,
					   "/", theme, "/",
					   style_strings[i],
					   "_foreground",
					   NULL);

			color = g_strdup_printf ("0x%02X%02X%02X",
						 style->foreground.red / 256,
						 style->foreground.green / 256,
						 style->foreground.blue / 256);

			gconf_client_set_string (client,
						 key,
						 color,
						 NULL);

			g_free (key);
			g_free (color);

			/* Background color. */
			key = g_strconcat (GLIMMER_BASE_KEY,
					   GLIMMER_THEMES_KEY,
					   "/", theme, "/",
					   style_strings[i],
					   "_background",
					   NULL);

			color = g_strdup_printf ("0x%02X%02X%02X",
						 style->background.red / 256,
						 style->background.green / 256,
						 style->background.blue / 256);

			gconf_client_set_string (client,
						 key,
						 color,
						 NULL);

			g_free (key);
			g_free (color);

			style->modified = FALSE;
		}
	}
}
#endif

static void
default_font_cb (GtkToggleButton    *button,
		 GlimmerDialogPrefs *dialog)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dialog->dialog), "GlimmerView");

	gtk_widget_set_sensitive (dialog->fontpicker,
				  !gtk_toggle_button_get_active (button));

	glimmer_view_set_use_default_font (view, gtk_toggle_button_get_active (button));
}

static void
fontpicker_cb (GnomeFontPicker    *picker,
	       gchar              *font,
	       GlimmerDialogPrefs *dialog)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dialog->dialog), "GlimmerView");

	glimmer_view_set_font (view, font);
}

static void
limit_undo_cb (GtkToggleButton    *button,
	       GlimmerDialogPrefs *dialog)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dialog->dialog), "GlimmerView");
	gboolean limit_undo = gtk_toggle_button_get_active (button);

	gtk_widget_set_sensitive (dialog->undo_levels, limit_undo);
	glimmer_view_set_limit_undo (view, limit_undo);
}

static void
undo_levels_cb (GtkSpinButton      *button,
		GlimmerDialogPrefs *dialog)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dialog->dialog), "GlimmerView");

	glimmer_view_set_max_undo_levels (view, gtk_spin_button_get_value_as_int (button));
}

static void
right_margin_cb (GtkToggleButton    *button,
		 GlimmerDialogPrefs *dialog)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dialog->dialog), "GlimmerView");
	gboolean show_margin = gtk_toggle_button_get_active (button);
	
	gtk_widget_set_sensitive (dialog->margin_pos, show_margin);
	gtk_source_view_set_show_margin (GTK_SOURCE_VIEW (view), show_margin);
}

static void
margin_pos_cb (GtkSpinButton      *button,
	       GlimmerDialogPrefs *dialog)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dialog->dialog), "GlimmerView");

	gtk_source_view_set_margin (GTK_SOURCE_VIEW (view), 
				    gtk_spin_button_get_value_as_int (button));
}

static void
tab_width_cb (GtkSpinButton      *button,
	      GlimmerDialogPrefs *dialog)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dialog->dialog), "GlimmerView");

	gtk_source_view_set_tabs_width (GTK_SOURCE_VIEW (view), 
				        gtk_spin_button_get_value_as_int (button));
}

static void
wrap_text_cb (GtkToggleButton    *button,
	      GlimmerDialogPrefs *dialog)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dialog->dialog), "GlimmerView");
	GtkWrapMode wrap_mode;

	if (gtk_toggle_button_get_active (button)) {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->word_wrap),
					      TRUE);
		gtk_widget_set_sensitive (dialog->word_wrap, TRUE);
		wrap_mode = GTK_WRAP_WORD;
	} else {
		gtk_widget_set_sensitive (dialog->word_wrap, FALSE);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->word_wrap),
					      FALSE);
		wrap_mode = GTK_WRAP_NONE;
	}

	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (view), wrap_mode);
}

static void
word_wrap_cb (GtkToggleButton    *button,
	      GlimmerDialogPrefs *dialog)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dialog->dialog), "GlimmerView");
	GtkWrapMode wrap_mode;
	
	if (GTK_WIDGET_IS_SENSITIVE (GTK_WIDGET (button))) {
		if (gtk_toggle_button_get_active (button)) {
			wrap_mode = GTK_WRAP_WORD;
		} else {
			wrap_mode = GTK_WRAP_CHAR;
		}

		gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (view), wrap_mode);
	}
}

static void
theme_changed_cb (GtkOptionMenu *option,
		  GlimmerDialogPrefs *dialog)
{
}

static GtkSourceTagStyle *
get_source_tag_style (GlimmerDialogPrefs *dialog,
		      char **name,
		      GtkSourceLanguage **language)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dialog->dialog), "GlimmerView");
	GtkTreePath *path;
	GtkTreeIter iter;
	char *id;
	int idx;
	GSList *languages;
	GtkSourceTagStyle *style;

	gtk_tree_view_get_cursor (GTK_TREE_VIEW (dialog->styles), &path, NULL);
	gtk_tree_model_get_iter (GTK_TREE_MODEL (dialog->styles_model),
				 &iter, path);
	gtk_tree_path_free (path);
	gtk_tree_model_get (GTK_TREE_MODEL (dialog->styles_model),
			    &iter, 0, name, -1);

	id = gconf_escape_key (*name, strlen (*name));
	
	idx = gtk_option_menu_get_history (GTK_OPTION_MENU (dialog->languages));
	/*if (idx == 0) {
		GtkSourceStyleScheme *scheme = gtk_source_style_scheme_get_default ();
		style = gtk_source_style_scheme_get_tag_style (scheme, *name);
	} else {*/
		languages = (GSList *)glimmer_view_get_available_languages (view);
		*language = g_slist_nth_data (languages, idx/* - 1*/);
		style = gtk_source_language_get_tag_style (*language, id);
	/*}*/

	g_free (id);

	return style;
}

static void
styles_cb (GtkWidget *treeview,
	   GlimmerDialogPrefs *dialog)
{
	GtkSourceLanguage *lang = NULL;
	GtkSourceTagStyle *style;
	char *name;
	gboolean enabled;

	style = get_source_tag_style (dialog, &name, &lang);
	if (!style) {
		g_warning ("Unknown tag '%s'", name);
		g_free (name);
		return;
	}

	/* Set a flag telling the widget signal callbacks that this is only a
	 * style update so they don't go updating gconf values. */
	dialog->style_update = TRUE;

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->style_bold),
				      style->bold);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->style_italic),
				      style->italic);
	enabled = (style->mask & GTK_SOURCE_TAG_STYLE_USE_FOREGROUND) != 0;
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->use_foreground),
				      enabled);
	gtk_widget_set_sensitive (dialog->style_foreground, enabled); 
	gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (dialog->style_foreground),
				    style->foreground.red,
				    style->foreground.green,
				    style->foreground.blue,
				    0);
	enabled = (style->mask & GTK_SOURCE_TAG_STYLE_USE_BACKGROUND) != 0;
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->use_background),
				      enabled); 
	gtk_widget_set_sensitive (dialog->style_background, enabled); 
	gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (dialog->style_background),
				    style->background.red,
				    style->background.green,
				    style->background.blue,
				    0);

	/* Reset flag. */
	dialog->style_update = FALSE;

	gtk_source_tag_style_free (style);
	g_free (name);
}

static void
save_style_to_gconf (GlimmerDialogPrefs *dialog,
		     const char *name,
		     GtkSourceLanguage *lang,
		     const GtkSourceTagStyle *style)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dialog->dialog), "GlimmerView");
	GConfClient *client;
	char *theme, *lang_id, *folder, *key, *gconf_name, *style_key, *val;
	gboolean use_color;

	theme = glimmer_view_get_theme (view);
	lang_id = gtk_source_language_get_id (lang);
	gconf_name = gconf_escape_key (name, strlen (name));
	folder = g_strconcat (GLIMMER_BASE_KEY, GLIMMER_THEMES_KEY, "/", theme,
			      "/", lang_id, NULL);
	key = g_strconcat (GLIMMER_BASE_KEY, GLIMMER_THEMES_KEY, "/", theme, "/",
			   lang_id, "/", gconf_name, NULL);
	g_free (gconf_name);
	g_free (lang_id);
	g_free (theme);

	client = gconf_client_get_default ();

	/* Create the directory if it doesn't exist yet. */
	if (!gconf_client_dir_exists (client, folder, NULL)) {
		gconf_client_add_dir (client, folder,
				      GCONF_CLIENT_PRELOAD_NONE, NULL);
	}
	g_free (folder);

	/* Use background color. */
	style_key = g_strconcat (key, "_usebackground", NULL);
	use_color = (style->mask & GTK_SOURCE_TAG_STYLE_USE_BACKGROUND) != 0;
	gconf_client_set_bool (client, style_key, use_color, NULL);
	g_free (style_key);

	if (use_color) {
		/* Background. */
		style_key = g_strconcat (key, "_background", NULL);
		val = g_strdup_printf ("0x%02X%02X%02X",
				       style->background.red / 256,
				       style->background.green / 256,
				       style->background.blue / 256);
		gconf_client_set_string (client, style_key, val, NULL);
		g_free (val);
		g_free (style_key);
	}

	/* Use foreground color. */
	style_key = g_strconcat (key, "_useforeground", NULL);
	use_color = (style->mask & GTK_SOURCE_TAG_STYLE_USE_FOREGROUND) != 0;
	gconf_client_set_bool (client, style_key, use_color, NULL);
	g_free (style_key);

	if (use_color) {
		/* Foreground. */
		style_key = g_strconcat (key, "_foreground", NULL);
		val = g_strdup_printf ("0x%02X%02X%02X",
				       style->foreground.red / 256,
				       style->foreground.green / 256,
				       style->foreground.blue / 256);
		gconf_client_set_string (client, style_key, val, NULL);
		g_free (val);
		g_free (style_key);
	}

	/* Bold. */
	style_key = g_strconcat (key, "_bold", NULL);
	gconf_client_set_bool (client, style_key, style->bold, NULL);
	g_free (style_key);

	/* Italic. */
	style_key = g_strconcat (key, "_italic", NULL);
	gconf_client_set_bool (client, style_key, style->italic, NULL);
	g_free (style_key);

	g_free (key);
	g_object_unref (G_OBJECT (client));
}

static void
style_bold_cb (GtkToggleButton *button,
	       GlimmerDialogPrefs *dialog)
{
	GtkSourceLanguage *lang = NULL;
	GtkSourceTagStyle *style;
	char *name;

	if (dialog->style_update)
		return;

	style = get_source_tag_style (dialog, &name, &lang);	
	if (!style) {
		g_warning ("Unknown tag '%s'", name);
		g_free (name);
		return;
	}

	style->bold = gtk_toggle_button_get_active (button);
	save_style_to_gconf (dialog, name, lang, style);
	gtk_source_tag_style_free (style);
	g_free (name);
}

static void
style_italic_cb (GtkToggleButton *button,
		 GlimmerDialogPrefs *dialog)
{
	GtkSourceLanguage *lang = NULL;
	GtkSourceTagStyle *style;
	char *name;

	if (dialog->style_update)
		return;

	style = get_source_tag_style (dialog, &name, &lang);	
	if (!style) {
		g_warning ("Unknown tag '%s'", name);
		g_free (name);
		return;
	}

	style->italic = gtk_toggle_button_get_active (button);
	save_style_to_gconf (dialog, name, lang, style);
	gtk_source_tag_style_free (style);
	g_free (name);
}

static void
use_foreground_cb (GtkToggleButton *button,
		   GlimmerDialogPrefs *dialog)
{
	GtkSourceLanguage *lang = NULL;
	GtkSourceTagStyle *style;
	char *name;

	if (dialog->style_update)
		return;

	style = get_source_tag_style (dialog, &name, &lang);	
	if (!style) {
		g_warning ("Unknown tag '%s'", name);
		g_free (name);
		return;
	}

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->use_background)))
		style->mask |= GTK_SOURCE_TAG_STYLE_USE_BACKGROUND;
	else
		style->mask &= ~GTK_SOURCE_TAG_STYLE_USE_BACKGROUND;

	save_style_to_gconf (dialog, name, lang, style);
	gtk_source_tag_style_free (style);
	g_free (name);

	gtk_widget_set_sensitive (dialog->style_foreground,
		gtk_toggle_button_get_active (button));
}

static void
style_foreground_cb (GnomeColorPicker   *picker,
		     guint               red,
		     guint               green,
		     guint               blue,
		     guint               alpha,
		     GlimmerDialogPrefs *dialog)
{
	GtkSourceLanguage *lang = NULL;
	GtkSourceTagStyle *style;
	char *name;

	if (dialog->style_update)
		return;

	style = get_source_tag_style (dialog, &name, &lang);	
	if (!style) {
		g_warning ("Unknown tag '%s'", name);
		g_free (name);
		return;
	}

	style->foreground.red = red;
	style->foreground.green = green;
	style->foreground.blue = blue;
	save_style_to_gconf (dialog, name, lang, style);
	gtk_source_tag_style_free (style);
	g_free (name);
}

static void
use_background_cb (GtkToggleButton *button,
		   GlimmerDialogPrefs *dialog)
{
	GtkSourceLanguage *lang = NULL;
	GtkSourceTagStyle *style;
	char *name;

	if (dialog->style_update)
		return;

	style = get_source_tag_style (dialog, &name, &lang);	
	if (!style) {
		g_warning ("Unknown tag '%s'", name);
		g_free (name);
		return;
	}

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->use_background)))
		style->mask |= GTK_SOURCE_TAG_STYLE_USE_BACKGROUND;
	else
		style->mask &= ~GTK_SOURCE_TAG_STYLE_USE_BACKGROUND;

	save_style_to_gconf (dialog, name, lang, style);
	gtk_source_tag_style_free (style);
	g_free (name);

	gtk_widget_set_sensitive (dialog->style_background,
		gtk_toggle_button_get_active (button));
}

static void
style_background_cb (GnomeColorPicker   *picker,
		     guint               red,
		     guint               green,
		     guint               blue,
		     guint               alpha,
		     GlimmerDialogPrefs *dialog)
{
	GtkSourceLanguage *lang = NULL;
	GtkSourceTagStyle *style;
	char *name;

	if (dialog->style_update)
		return;

	style = get_source_tag_style (dialog, &name, &lang);	
	if (!style) {
		g_warning ("Unknown tag '%s'", name);
		g_free (name);
		return;
	}

	style->background.red = red;
	style->background.green = green;
	style->background.blue = blue;
	save_style_to_gconf (dialog, name, lang, style);
	gtk_source_tag_style_free (style);
	g_free (name);
}

static void
style_defaults_cb (GtkButton *button,
		   GlimmerDialogPrefs *dialog)
{
}

static void
language_changed_cb (GtkOptionMenu *optionmenu,
		     GlimmerDialogPrefs *dialog)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dialog->dialog), "GlimmerView");
	int idx;
	GSList *languages, *tags, *l;
	GtkSourceLanguage *lang;
	GtkSourceStyleScheme *scheme;
	GtkTreeIter iter;
	GtkTreePath *path;

	gtk_list_store_clear (dialog->styles_model);

	idx = gtk_option_menu_get_history (optionmenu);
	/*if (idx == 0) {
		tags = gtk_source_style_scheme_get_style_names (
			gtk_source_style_scheme_get_default ());
			
		for (l = tags; l != NULL; l = l->next) {
			char *name = l->data;
			gtk_list_store_append (dialog->styles_model, &iter);
			gtk_list_store_set (dialog->styles_model, &iter, 0, name, -1);
			g_free (name);
		}

		g_slist_free (tags);
	} else {*/
		languages = (GSList *)glimmer_view_get_available_languages (view);
		lang = g_slist_nth_data (languages, idx/* - 1*/);

		/* This is a gross hack since the GtkSourceStyleScheme API totally
		 * sucks and we have no way of setting our own default style
		 * scheme. */
		scheme = gtk_source_language_get_style_scheme (lang);
		if (!GLIMMER_IS_STYLE_SCHEME (scheme)) {
			GlimmerThemes *themes = _glimmer_view_get_themes (view);
			scheme = GTK_SOURCE_STYLE_SCHEME (
				glimmer_style_scheme_new (themes, lang, "Default"));
			gtk_source_language_set_style_scheme (lang, scheme);
		}

		tags = gtk_source_language_get_tags (lang);
		for (l = tags; l != NULL; l = l->next) {
			GtkTextTag *tag = GTK_TEXT_TAG (l->data);
			char *name;
		
			g_object_get (tag, "name", &name, NULL);
			gtk_list_store_append (dialog->styles_model, &iter);
			gtk_list_store_set (dialog->styles_model, &iter, 0, name, -1);
			g_free (name);
		}
	
		g_slist_foreach (tags, (GFunc)g_object_unref, NULL);
		g_slist_free (tags);

		/*gtk_source_buffer_set_language (GTK_SOURCE_BUFFER (dialog->buffer),
						lang);*/
	/*}*/

	/* Trigger styles_cb on first item so color & font widgets get set. */
	path = gtk_tree_path_new_first ();
	gtk_tree_view_set_cursor (GTK_TREE_VIEW (dialog->styles), path, NULL, FALSE);
	gtk_tree_path_free (path);

	/* FIXME: Update the preview code text from the .lang file. */
	/*gtk_text_buffer_get_start_iter (dialog->buffer, &start);
	gtk_text_buffer_get_end_iter (dialog->buffer, &end);
	gtk_text_buffer_delete (dialog->buffer, &start, &end);
	gtk_text_buffer_insert_at_cursor (dialog->buffer,
					  "static void blah ();", 20);*/
}

static void
options_set_toggle (GtkTreeViewColumn *tree_column,
		    GtkCellRenderer *cell,
		    GtkTreeModel *model,
		    GtkTreeIter *iter,
		    gpointer data)
{
	gboolean visible = !gtk_tree_model_iter_has_child (model, iter);
	g_object_set (G_OBJECT (cell), "visible", visible, NULL);

	if (visible) {
		gboolean active;
		gtk_tree_model_get (model, iter, COLUMN_ACTIVE, &active, -1);
		g_object_set (G_OBJECT (cell), "active", active, NULL);
	}
}

static void
options_set_pixbuf (GtkTreeViewColumn *tree_column,
		    GtkCellRenderer *cell,
		    GtkTreeModel *model,
		    GtkTreeIter *iter,
		    gpointer data)
{
	gboolean visible = gtk_tree_model_iter_has_child (model, iter);
	g_object_set (G_OBJECT (cell), "visible", visible, NULL);

	if (visible) {
		GdkPixbuf *pixbuf;
		gtk_tree_model_get (model, iter, COLUMN_PIXBUF, &pixbuf, -1);
		g_object_set (G_OBJECT (cell), "pixbuf", pixbuf, NULL);
		g_object_unref (G_OBJECT (pixbuf));
	}
}

static void
options_set_text (GtkTreeViewColumn *tree_column,
		  GtkCellRenderer *cell,
		  GtkTreeModel *model,
		  GtkTreeIter *iter,
		  gpointer data)
{
	char *name;

	gtk_tree_model_get (model, iter, COLUMN_NAME, &name, -1);
	g_object_set (GTK_CELL_RENDERER (cell), "text", name, NULL);
	g_free (name);
}

static void
option_toggled_cb (GtkCellRendererToggle *cell,
		   gchar *path_str,
		   gpointer data)
{
	GlimmerDialogPrefs *dialog = (GlimmerDialogPrefs *)data;
	GtkTreePath *path = gtk_tree_path_new_from_string (path_str);
	GtkTreeIter iter;
	gboolean active;
	GlimmerOptionCallback cb;
	gpointer user_data;

	gtk_tree_model_get_iter (GTK_TREE_MODEL (dialog->options_model), &iter, path);
	gtk_tree_model_get (GTK_TREE_MODEL (dialog->options_model), &iter,
			    COLUMN_ACTIVE, &active, COLUMN_CALLBACK, &cb,
			    COLUMN_USERDATA, &user_data, -1);

	active ^= 1;

	if (cb != NULL)
		(*cb) (dialog, active, user_data);

	gtk_tree_store_set (GTK_TREE_STORE (dialog->options_model), &iter,
			    COLUMN_ACTIVE, active, -1);
	gtk_tree_path_free (path);
}

static void
show_line_numbers_cb (GlimmerDialogPrefs *dialog,
		      gboolean active,
		      gpointer user_data)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dialog->dialog), "GlimmerView");

	gtk_source_view_set_show_line_numbers (GTK_SOURCE_VIEW (view), active);
}

static void
show_line_markers_cb (GlimmerDialogPrefs *dialog,
		      gboolean active,
		      gpointer user_data)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dialog->dialog), "GlimmerView");

	gtk_source_view_set_show_line_markers (GTK_SOURCE_VIEW (view), active);
}

static void
auto_indent_cb (GlimmerDialogPrefs *dialog,
		gboolean active,
		gpointer user_data)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dialog->dialog), "GlimmerView");

	gtk_source_view_set_auto_indent (GTK_SOURCE_VIEW (view), active);
}

static void
setup_options_tree (GlimmerDialogPrefs *dialog)
{
	GlimmerView *view;
	GtkSourceView *source_view;
	GtkTreeModel *model;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeIter iter, child;
	GdkPixbuf *pixbuf;

	view = g_object_get_data (G_OBJECT (dialog->dialog), "GlimmerView");
	source_view = GTK_SOURCE_VIEW (view);

	dialog->options_model = gtk_tree_store_new (LAST_COL,
						    G_TYPE_STRING,
						    GDK_TYPE_PIXBUF,
						    G_TYPE_BOOLEAN,
						    G_TYPE_POINTER,
						    G_TYPE_POINTER);
	model = GTK_TREE_MODEL (dialog->options_model);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
					      COLUMN_NAME, GTK_SORT_ASCENDING);
	gtk_tree_view_set_model (GTK_TREE_VIEW (dialog->options), model);

	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, "Options");
	renderer = gtk_cell_renderer_toggle_new ();
	g_signal_connect (G_OBJECT (renderer), "toggled",
			  G_CALLBACK (option_toggled_cb), dialog);
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 options_set_toggle,
						 NULL, NULL);
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 options_set_pixbuf,
						 NULL, NULL);
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 options_set_text,
						 NULL, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (dialog->options), column);

	/* Add default items. */
	gtk_tree_store_append (dialog->options_model, &iter, NULL);
	pixbuf = gdk_pixbuf_new_from_file (ICONDIR "stock_list_enum-16.png", NULL);
	gtk_tree_store_set (dialog->options_model, &iter,
			    COLUMN_NAME, "Margin",
			    COLUMN_PIXBUF, pixbuf, -1);
	g_object_unref (G_OBJECT (pixbuf));
	gtk_tree_store_append (dialog->options_model, &child, &iter);
	gtk_tree_store_set (dialog->options_model, &child,
			    COLUMN_NAME, "Show line numbers",
			    COLUMN_ACTIVE, gtk_source_view_get_show_line_numbers (source_view),
			    COLUMN_CALLBACK, show_line_numbers_cb, -1);
	gtk_tree_store_append (dialog->options_model, &child, &iter);
	gtk_tree_store_set (dialog->options_model, &child,
			    COLUMN_NAME, "Show line markers",
			    COLUMN_ACTIVE, gtk_source_view_get_show_line_markers (source_view),
			    COLUMN_CALLBACK, show_line_markers_cb, -1);

	gtk_tree_store_append (dialog->options_model, &iter, NULL);
	pixbuf = gdk_pixbuf_new_from_file (ICONDIR "stock_text_indent-16.png", NULL);
	gtk_tree_store_set (dialog->options_model, &iter,
			    COLUMN_NAME, "Indent",
			    COLUMN_PIXBUF, pixbuf, -1);
	g_object_unref (G_OBJECT (pixbuf));
	gtk_tree_store_append (dialog->options_model, &child, &iter);
	gtk_tree_store_set (dialog->options_model, &child,
			    COLUMN_NAME, "Auto indent",
			    COLUMN_ACTIVE, gtk_source_view_get_auto_indent (source_view),
			    COLUMN_CALLBACK, auto_indent_cb, -1);
#if 0
	gtk_tree_store_append (dialog->options_model, &child, &iter);
	gtk_tree_store_set (dialog->options_model, &child,
			    COLUMN_NAME, "Smart indent",
			    COLUMN_ACTIVE, TRUE, -1);
	gtk_tree_store_append (dialog->options_model, &child, &iter);
	gtk_tree_store_set (dialog->options_model, &child,
			    COLUMN_NAME, "Block indent",
			    COLUMN_ACTIVE, FALSE, -1);
	gtk_tree_store_append (dialog->options_model, &child, &iter);
	gtk_tree_store_set (dialog->options_model, &child,
			    COLUMN_NAME, "Smart paste",
			    COLUMN_ACTIVE, TRUE, -1);

	gtk_tree_store_append (dialog->options_model, &iter, NULL);
	pixbuf = gtk_widget_render_icon (GTK_WIDGET (dialog->dialog),
					 GTK_STOCK_FIND, GTK_ICON_SIZE_MENU,
					 NULL);
	gtk_tree_store_set (dialog->options_model, &iter,
			    COLUMN_NAME, "Search",
			    COLUMN_PIXBUF, pixbuf, -1);
	g_object_unref (G_OBJECT (pixbuf));
	gtk_tree_store_append (dialog->options_model, &child, &iter);
	gtk_tree_store_set (dialog->options_model, &child,
			    COLUMN_NAME, "Show dialog when search fails",
			    COLUMN_ACTIVE, TRUE, -1);
	gtk_tree_store_append (dialog->options_model, &child, &iter);
	gtk_tree_store_set (dialog->options_model, &child,
			    COLUMN_NAME, "Search word at cursor",
			    COLUMN_ACTIVE, FALSE, -1);

	gtk_tree_store_append (dialog->options_model, &iter, NULL);
	pixbuf = gdk_pixbuf_new_from_file (ICONDIR "stock_interaction-16.png", NULL);
	gtk_tree_store_set (dialog->options_model, &iter,
			    COLUMN_NAME, "Brace match",
			    COLUMN_PIXBUF, pixbuf, -1);
	g_object_unref (G_OBJECT (pixbuf));
	gtk_tree_store_append (dialog->options_model, &child, &iter);
	gtk_tree_store_set (dialog->options_model, &child,
			    COLUMN_NAME, "Generate closing brace",
			    COLUMN_ACTIVE, TRUE, -1);
	gtk_tree_store_append (dialog->options_model, &child, &iter);
	gtk_tree_store_set (dialog->options_model, &child,
			    COLUMN_NAME, "Enable brace match highlighting",
			    COLUMN_ACTIVE, TRUE, -1);
	gtk_tree_store_append (dialog->options_model, &child, &iter);
	gtk_tree_store_set (dialog->options_model, &child,
			    COLUMN_NAME, "Highlight opposing brace only",
			    COLUMN_ACTIVE, FALSE, -1);
	gtk_tree_store_append (dialog->options_model, &child, &iter);
	gtk_tree_store_set (dialog->options_model, &child,
			    COLUMN_NAME, "Ignore neighbouring braces",
			    COLUMN_ACTIVE, TRUE, -1);

	gtk_tree_store_append (dialog->options_model, &iter, NULL);
	pixbuf = gdk_pixbuf_new_from_file (ICONDIR "stock_stop-16.png", NULL);
	gtk_tree_store_set (dialog->options_model, &iter,
			    COLUMN_NAME, "Error",
			    COLUMN_PIXBUF, pixbuf, -1);
	g_object_unref (G_OBJECT (pixbuf));
	gtk_tree_store_append (dialog->options_model, &child, &iter);
	gtk_tree_store_set (dialog->options_model, &child,
			    COLUMN_NAME, "Underline errors",
			    COLUMN_ACTIVE, TRUE, -1);
	gtk_tree_store_append (dialog->options_model, &child, &iter);
	gtk_tree_store_set (dialog->options_model, &child,
			    COLUMN_NAME, "Show tooltip",
			    COLUMN_ACTIVE, TRUE, -1);
	gtk_tree_store_append (dialog->options_model, &child, &iter);
	gtk_tree_store_set (dialog->options_model, &child,
			    COLUMN_NAME, "Show in gutter",
			    COLUMN_ACTIVE, TRUE, -1);
#endif
}

static GlimmerDialogPrefs *
dialog_prefs_get_dialog (GlimmerView *view)
{
	GlimmerDialogPrefs *dialog;
	GladeXML *gui;
	GtkWindow *window;
	GtkWidget *notebook, *toplevel, *menu;
	GtkWidget *menuitem;
	GSList *languages, *themes, *l;
	GtkSourceLanguage *current_lang;
	GtkTreeViewColumn *column;
	GtkCellRenderer *renderer;

	gui = glade_xml_new (GLADEDIR "dialogs.glade", "prefs-notebook", NULL);
	if (!gui) {
		g_warning ("Could not find dialogs.glade, reinstall glimmer");
		return NULL;
	}

	/* Unable to get toplevel GtkWindow if part of out-of-process Bonobo component. */
	toplevel = gtk_widget_get_toplevel (GTK_WIDGET (view));
	if (!GTK_WIDGET_TOPLEVEL (toplevel))
		window = NULL;
	else
		window = GTK_WINDOW (toplevel);

	dialog = g_new0 (GlimmerDialogPrefs, 1);
	dialog->dialog = gtk_dialog_new_with_buttons (_("Editor Preferences"),
						      window,
						      GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_NO_SEPARATOR,
						      GTK_STOCK_CLOSE,
						      GTK_RESPONSE_CLOSE,
						      GTK_STOCK_HELP,
						      GTK_RESPONSE_HELP,
						      NULL);
	g_return_val_if_fail (dialog->dialog != NULL, NULL);
	gtk_container_set_border_width (GTK_CONTAINER (dialog->dialog), 5);
	gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dialog->dialog)->vbox), 2);

	g_object_set_data (G_OBJECT (dialog->dialog), "GlimmerView", view);

	notebook                       = glade_xml_get_widget (gui, "prefs-notebook");
	dialog->keymap                 = glade_xml_get_widget (gui, "keymap-optionmenu");
	dialog->customize              = glade_xml_get_widget (gui, "customize-button");
	dialog->limit_undo             = glade_xml_get_widget (gui, "limitundo-checkbutton");
	dialog->undo_levels            = glade_xml_get_widget (gui, "limitundo-spinbutton");
	dialog->options                = glade_xml_get_widget (gui, "options-treeview");
	
	dialog->default_font           = glade_xml_get_widget (gui, "samefont-checkbutton");
	dialog->fontpicker             = glade_xml_get_widget (gui, "editor-fontpicker");
	dialog->right_margin           = glade_xml_get_widget (gui, "rightmargin-checkbutton");
	dialog->margin_pos             = glade_xml_get_widget (gui, "rightmargin-spinbutton");
	dialog->tab_width              = glade_xml_get_widget (gui, "tabwidth-spinbutton");
	dialog->wrap_text              = glade_xml_get_widget (gui, "wraptext-checkbutton");
	dialog->word_wrap              = glade_xml_get_widget (gui, "wordwrap-checkbutton");
	
	dialog->themes                 = glade_xml_get_widget (gui, "colorscheme-optionmenu");
	dialog->languages              = glade_xml_get_widget (gui, "language-optionmenu");
	dialog->styles                 = glade_xml_get_widget (gui, "element-treeview");
	dialog->style_bold             = glade_xml_get_widget (gui, "bold-checkbutton");
	dialog->style_italic           = glade_xml_get_widget (gui, "italic-checkbutton");
	dialog->use_foreground         = glade_xml_get_widget (gui, "foreground-checkbutton");
	dialog->style_foreground       = glade_xml_get_widget (gui, "foreground-colorpicker");
	dialog->use_background         = glade_xml_get_widget (gui, "background-checkbutton");
	dialog->style_background       = glade_xml_get_widget (gui, "background-colorpicker");
	dialog->defaults               = glade_xml_get_widget (gui, "reset-button");
	g_object_unref (gui);

	if (!notebook || !dialog->customize || !dialog->limit_undo ||
	    !dialog->undo_levels || !dialog->options || !dialog->default_font ||
	    !dialog->fontpicker || !dialog->right_margin || !dialog->margin_pos ||
	    !dialog->tab_width || !dialog->wrap_text || !dialog->word_wrap ||
	    !dialog->themes || !dialog->styles || !dialog->style_bold || 
	    !dialog->style_italic || !dialog->style_foreground ||
	    !dialog->style_background || !dialog->defaults) {
		g_error ("Could not find the required widgets inside dialogs.glade");
		return NULL;
	}

	/* Setup the options treeview. */
	setup_options_tree (dialog);

	/* Create GtkListStore for styles & setup treeview. */
	dialog->styles_model = gtk_list_store_new (1, G_TYPE_STRING);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (dialog->styles_model),
					      0, GTK_SORT_ASCENDING);
	gtk_tree_view_set_model (GTK_TREE_VIEW (dialog->styles),
				 GTK_TREE_MODEL (dialog->styles_model));

	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes ("Styles", renderer,
							   "text", 0, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (dialog->styles), column);

	/* Create GlimmerView for the preview text. */
	/*dialog->preview = glimmer_view_new ();
	dialog->buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (dialog->preview));
	gtk_source_buffer_set_highlight (GTK_SOURCE_BUFFER (dialog->buffer), TRUE);
	gtk_container_add (GTK_CONTAINER (sw), dialog->preview);*/

	/* Connect signals. */
	g_signal_connect (G_OBJECT (dialog->limit_undo), "toggled",
			  G_CALLBACK (limit_undo_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->undo_levels), "value-changed",
			  G_CALLBACK (undo_levels_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->default_font), "toggled",
			  G_CALLBACK (default_font_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->fontpicker), "font-set",
			  G_CALLBACK (fontpicker_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->right_margin), "toggled",
			  G_CALLBACK (right_margin_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->margin_pos), "value-changed",
			  G_CALLBACK (margin_pos_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->tab_width), "value-changed",
			  G_CALLBACK (tab_width_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->wrap_text), "toggled",
			  G_CALLBACK (wrap_text_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->word_wrap), "toggled",
			  G_CALLBACK (word_wrap_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->themes), "changed",
			  G_CALLBACK (theme_changed_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->languages), "changed",
			  G_CALLBACK (language_changed_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->styles), "cursor-changed",
			  G_CALLBACK (styles_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->style_bold), "toggled",
			  G_CALLBACK (style_bold_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->style_italic), "toggled",
			  G_CALLBACK (style_italic_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->use_foreground), "toggled",
			  G_CALLBACK (use_foreground_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->style_foreground), "color-set",
			  G_CALLBACK (style_foreground_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->use_background), "toggled",
			  G_CALLBACK (use_background_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->style_background), "color-set",
			  G_CALLBACK (style_background_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->defaults), "clicked",
			  G_CALLBACK (style_defaults_cb), dialog);

	/* Add languages to optionmenu. */
	menu = gtk_menu_new ();
	/*menuitem = gtk_menu_item_new_with_label (_("None"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);*/	
	
	languages = (GSList *)glimmer_view_get_available_languages (view);
	for (l = languages; l; l = l->next) {
		GtkSourceLanguage *lang = l->data;
		char *name;

		name = gtk_source_language_get_name (lang);
		menuitem = gtk_menu_item_new_with_label (name);
		g_free (name);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	}
	gtk_widget_show_all (menu);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (dialog->languages), menu);
	
	/* Select current language if any in use. */
	current_lang = gtk_source_buffer_get_language (GTK_SOURCE_BUFFER (
		gtk_text_view_get_buffer (GTK_TEXT_VIEW (view))));
	if (current_lang != NULL) {
		gtk_option_menu_set_history (GTK_OPTION_MENU (dialog->languages),
					     g_slist_index (languages, current_lang));
	} else {
		gtk_option_menu_set_history (GTK_OPTION_MENU (dialog->languages), 0);
	}

	/* Add color schemes to optionmenu. */
	menu = gtk_menu_new ();
	themes = (GSList *)glimmer_view_get_available_themes (view);
	for (l = themes; l; l = l->next) {
		const char *theme = l->data;
		GtkWidget *menuitem;

		menuitem = gtk_menu_item_new_with_label (theme);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	}
	gtk_widget_show_all (menu);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (dialog->themes), menu);

	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog->dialog)->vbox),
			    notebook, FALSE, FALSE, 0);

	gtk_dialog_set_default_response (GTK_DIALOG (dialog->dialog),
					 GTK_RESPONSE_CLOSE);

	/*g_signal_connect (G_OBJECT (dialog->dialog), "destroy",
			  G_CALLBACK (dialog_destroyed), &dialog);*/
	g_signal_connect (G_OBJECT (dialog->dialog), "response",
			  G_CALLBACK (dialog_response_handler), dialog);

	gtk_window_set_resizable (GTK_WINDOW (dialog->dialog), FALSE);

	gtk_widget_show_all (notebook);

	return dialog;
}

void
glimmer_dialog_preferences (GlimmerView *view)
{
	GlimmerDialogPrefs *dialog;
	GtkWidget *window;

	dialog = g_object_get_data (G_OBJECT (view), "prefs-dialog");
	if (dialog == NULL) {
		dialog = dialog_prefs_get_dialog (view);
		if (dialog == NULL) {
			g_warning ("Could not create the Preferences dialog");
			return;
		}
	}

	dialog_prefs_load_settings (view, dialog);

	window = gtk_widget_get_toplevel (GTK_WIDGET (view));
	if (GTK_WIDGET_TOPLEVEL (window))
		gtk_window_set_transient_for (GTK_WINDOW (dialog->dialog),
					      GTK_WINDOW (window));

	gtk_window_present (GTK_WINDOW (dialog->dialog));
}
