/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  glimmer-dialog-goto-line.c
 *
 *  Copyright (C) 2002, 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glade/glade-xml.h>
#include <libgnome/libgnome.h>
#include "glimmer-utils.h"
#include "glimmer-dialogs.h"

typedef struct _GlimmerDialogGotoLine GlimmerDialogGotoLine;

struct _GlimmerDialogGotoLine {
	GtkWidget *dialog;
	GtkWidget *entry;
};

static void dialog_response_handler (GtkDialog             *dlg,
				     gint                   res_id,
				     GlimmerDialogGotoLine *dialog);
static void goto_button_pressed (GlimmerView           *view,
				 GlimmerDialogGotoLine *dialog);
static GlimmerDialogGotoLine *dialog_goto_line_get_dialog (GlimmerView *view);

static void
dialog_response_handler (GtkDialog             *dlg,
			 gint                   res_id,
			 GlimmerDialogGotoLine *dialog)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dlg), "GlimmerView");

	switch (res_id) {
		case GTK_RESPONSE_OK:
			goto_button_pressed (view, dialog);
			break;
		default:
			gtk_widget_hide (dialog->dialog);
	}
}

static void
destroy_dialog (gpointer data)
{
	GlimmerDialogGotoLine *dialog = data;

	gtk_widget_destroy (dialog->dialog);
	g_free (dialog);
}

static GlimmerDialogGotoLine *
dialog_goto_line_get_dialog (GlimmerView *view)
{
	GlimmerDialogGotoLine *dialog;
	GladeXML *gui;
	GtkWidget *vbox, *toplevel, *button;
	GtkWindow *window;

	gui = glade_xml_new (GLADEDIR "dialogs.glade", "gotoline-vbox", NULL);
	if (!gui) {
		g_warning ("Could not find dialogs.glade, reinstall glimmer");
		return NULL;
	}

	/* Unable to get toplevel GtkWindow if part of out-of-process Bonobo component. */
	toplevel = gtk_widget_get_toplevel (GTK_WIDGET (view));
	if (!GTK_WIDGET_TOPLEVEL (toplevel))
		window = NULL;
	else
		window = GTK_WINDOW (toplevel);

	dialog = g_new0 (GlimmerDialogGotoLine, 1);
	dialog->dialog = gtk_dialog_new_with_buttons (_("Go to Line"),
						      window,
						      GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_NO_SEPARATOR,
						      GTK_STOCK_CLOSE,
						      GTK_RESPONSE_CANCEL,
						      NULL);
	g_return_val_if_fail (dialog->dialog != NULL, NULL);

	g_object_set_data (G_OBJECT (dialog->dialog), "GlimmerView", view);
	g_object_set_data_full (G_OBJECT (view), "gotoline-dialog",
				dialog, destroy_dialog);

	/* Add "Go to Line" button */
	button = glimmer_button_new_with_stock_image (_("_Go to Line"),
						      GTK_STOCK_JUMP_TO);
	g_return_val_if_fail (button != NULL, NULL);

	GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
	gtk_widget_show (button);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog->dialog), 
				      button, GTK_RESPONSE_OK);

	vbox = glade_xml_get_widget (gui, "gotoline-vbox");
	dialog->entry = glade_xml_get_widget (gui, "entry");
	g_object_unref (gui);

	if (!dialog->entry) {
		g_warning (_("Could not find the required widgets inside dialogs.glade"));
		return NULL;
	}

	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog->dialog)->vbox),
			    vbox, FALSE, FALSE, 0);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog->dialog),
					 GTK_RESPONSE_OK);
	g_signal_connect (G_OBJECT (dialog->dialog), "delete-event",
			  G_CALLBACK (gtk_widget_hide), NULL);
	g_signal_connect(G_OBJECT (dialog->dialog), "response",
			 G_CALLBACK (dialog_response_handler), dialog);
	gtk_window_set_resizable (GTK_WINDOW (dialog->dialog), FALSE);

	return dialog;
}

void
glimmer_dialog_goto_line (GlimmerView *view)
{
	GlimmerDialogGotoLine *dialog;
	GtkWidget *window;

	dialog = g_object_get_data (G_OBJECT (view), "gotoline-dialog");
	if (dialog == NULL) {
		dialog = dialog_goto_line_get_dialog (view);
		if (dialog == NULL) {
			g_warning ("Could not create the Goto Line dialog");
			return;
		}
	}

	window = gtk_widget_get_toplevel (GTK_WIDGET (view));
	if (GTK_WIDGET_TOPLEVEL (window))
		gtk_window_set_transient_for (GTK_WINDOW (dialog->dialog),
					      GTK_WINDOW (window));

	gtk_window_present (GTK_WINDOW (dialog->dialog));
	gtk_widget_grab_focus (dialog->entry);
}

static void
goto_button_pressed (GlimmerView *view,
		     GlimmerDialogGotoLine *dialog)
{
	GtkTextBuffer *buffer;
	const gchar* text;
	guint line, line_count;
	GtkTextIter iter;

	buffer = GTK_TEXT_VIEW (view)->buffer;

	text = gtk_entry_get_text (GTK_ENTRY (dialog->entry));

	if (text != NULL && text[0] != 0) {
		line = MAX (atoi (text) - 1, 0);
		line_count = gtk_text_buffer_get_line_count (buffer);

		if (line > line_count)
			line = line_count;

		gtk_widget_grab_focus (GTK_WIDGET (view));
		gtk_text_buffer_get_iter_at_line (buffer, &iter, line);
		gtk_text_buffer_place_cursor (buffer, &iter);
		gtk_text_view_scroll_to_mark (GTK_TEXT_VIEW (view),
					      gtk_text_buffer_get_mark (buffer,
									"insert"),
					      0.0, TRUE, 0.0, 0.5);
		gtk_widget_hide (dialog->dialog);
	}
}
