/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  glimmer-dialog-page-setup.c
 *
 *  Copyright (C) 2003 - Paolo Maggi 
 *  Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * Copied from the gedit project. Original author Paolo Maggi.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <glade/glade-xml.h>
#include <gconf/gconf-client.h>
#include <libgnome/libgnome.h>
#include "glimmer-dialogs.h"
#include "glimmer-settings.h"
#include "glimmer-utils.h"
#include "gnome-print-font-picker.h"

#define DEFAULT_PRINT_FONT_BODY 	(const gchar *) "Monospace Regular 9"
#define DEFAULT_PRINT_FONT_HEADER	(const gchar *) "Sans Regular 11"
#define DEFAULT_PRINT_FONT_NUMBERS	(const gchar *) "Sans Regular 8"

typedef struct _GlimmerDialogPageSetup GlimmerDialogPageSetup;

struct _GlimmerDialogPageSetup {
	GtkWidget *dialog;

	GtkWidget *print_syntax;
	GtkWidget *print_header;
	
	GtkWidget *print_line_numbers;
	GtkWidget *print_line_every;

	GtkWidget *wrap_text;
	GtkWidget *word_wrap;

	GtkWidget *fonts_table;
	GtkWidget *body_fontpicker;
	GtkWidget *header_fontpicker;
	GtkWidget *numbers_fontpicker;

	GtkWidget *restore_fonts;
};

#if 0
static void
dialog_destroyed (GtkObject *obj, void **dialog_pointer)
{
	if (dialog_pointer != NULL) {
		g_free (*dialog_pointer);
		*dialog_pointer = NULL;
	}	
}
#endif

static void
dialog_page_setup_load_settings (GlimmerView *view,
				 GlimmerDialogPageSetup *dialog)
{
	GConfClient *client;
	gchar *str;
	GtkWrapMode wrap_mode;
	gboolean bval;

	client = gconf_client_get_default ();

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->print_syntax),
		gconf_client_get_bool (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
				       GLIMMER_SETTING_PRINT_SYNTAX, NULL));

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->print_header),
		gconf_client_get_bool (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
				       GLIMMER_SETTING_PRINT_HEADER, NULL));

	bval = gconf_client_get_bool (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
				      GLIMMER_SETTING_PRINT_LINE_NUMBERS, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->print_line_numbers),
				      bval);
	gtk_widget_set_sensitive (dialog->print_line_every, bval);

	gtk_spin_button_set_value (GTK_SPIN_BUTTON (dialog->print_line_every),
		gconf_client_get_int (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
				      GLIMMER_SETTING_PRINT_LINE_EVERY, NULL));

	str = gconf_client_get_string (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
				       GLIMMER_SETTING_PRINT_WRAP_MODE, NULL);
		wrap_mode = glimmer_util_get_wrap_mode_from_string (str);
	if (wrap_mode == GTK_WRAP_NONE) {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->wrap_text),
					      FALSE);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->word_wrap),
					      FALSE);
	} else if (wrap_mode == GTK_WRAP_WORD) {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->wrap_text),
					      TRUE);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->word_wrap),
					      TRUE);
	} else {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->wrap_text),
					      TRUE);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->word_wrap),
					      FALSE);
	}
	g_free (str);

	str = gconf_client_get_string (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
				       GLIMMER_SETTING_PRINT_FONT_BODY, NULL);
	gnome_print_font_picker_set_font_name (
		GNOME_PRINT_FONT_PICKER (dialog->body_fontpicker), str);
	g_free (str);
	
	str = gconf_client_get_string (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
				       GLIMMER_SETTING_PRINT_FONT_HEADER, NULL);
	gnome_print_font_picker_set_font_name (
		GNOME_PRINT_FONT_PICKER (dialog->header_fontpicker), str);
	g_free (str);
	
	str = gconf_client_get_string (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
				       GLIMMER_SETTING_PRINT_FONT_NUMBERS, NULL);
	gnome_print_font_picker_set_font_name (
		GNOME_PRINT_FONT_PICKER (dialog->numbers_fontpicker), str);
	g_free (str);

	g_object_unref (G_OBJECT (client));
}

static void
dialog_page_setup_save_settings (GlimmerView *view,
				 GlimmerDialogPageSetup *dialog)
{
	GConfClient *client;
	gboolean bval;
	gint ival;
	const gchar *str;

	client = gconf_client_get_default ();

	bval = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->print_syntax));
	gconf_client_set_bool (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
			       GLIMMER_SETTING_PRINT_SYNTAX, bval, NULL);

	bval = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->print_header));
	gconf_client_set_bool (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
			       GLIMMER_SETTING_PRINT_HEADER, bval, NULL);

	bval = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->print_line_numbers));
	gconf_client_set_bool (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
			       GLIMMER_SETTING_PRINT_LINE_NUMBERS, bval, NULL);

	ival = gtk_spin_button_get_value (GTK_SPIN_BUTTON (dialog->print_line_every));
	gconf_client_set_int (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
			      GLIMMER_SETTING_PRINT_LINE_EVERY, ival, NULL);

	bval = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->wrap_text));
	if (!bval) {
		str = "GTK_WRAP_NONE";
	} else {
		bval = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->word_wrap));
		if (bval)
			str = "GTK_WRAP_WORD";
		else
			str = "GTK_WRAP_CHAR";
	}
	gconf_client_set_string (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
				 GLIMMER_SETTING_PRINT_WRAP_MODE, str, NULL);

	str = gnome_print_font_picker_get_font_name (
		GNOME_PRINT_FONT_PICKER (dialog->body_fontpicker));
	gconf_client_set_string (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
				 GLIMMER_SETTING_PRINT_FONT_BODY, str, NULL);
	
	str = gnome_print_font_picker_get_font_name (
		GNOME_PRINT_FONT_PICKER (dialog->header_fontpicker));
	gconf_client_set_string (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
				 GLIMMER_SETTING_PRINT_FONT_HEADER, str, NULL);	

	str = gnome_print_font_picker_get_font_name (
		GNOME_PRINT_FONT_PICKER (dialog->numbers_fontpicker));
	gconf_client_set_string (client, GLIMMER_BASE_KEY GLIMMER_PRINT_KEY
				 GLIMMER_SETTING_PRINT_FONT_NUMBERS, str, NULL);

	g_object_unref (G_OBJECT (client));
}

static void
dialog_response_handler (GtkDialog *dlg,
			 gint res_id,
			 GlimmerDialogPageSetup *dialog)
{
	GlimmerView *view = g_object_get_data (G_OBJECT (dlg), "GlimmerView");

	switch (res_id) {
		case GTK_RESPONSE_HELP:
			/* TODO */;
			break;
		default:
			dialog_page_setup_save_settings (view, dialog);
			gtk_widget_destroy (dialog->dialog);
			break;
	}
}

static void
print_line_numbers_cb (GtkToggleButton *button,
		       GlimmerDialogPageSetup *dialog)
{
	gtk_widget_set_sensitive (dialog->print_line_every,
				  gtk_toggle_button_get_active (button));
}

static void
wrap_text_cb (GtkToggleButton *button,
	      GlimmerDialogPageSetup *dialog)
{
	if (gtk_toggle_button_get_active (button)) {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->word_wrap),
					      TRUE);
		gtk_widget_set_sensitive (dialog->word_wrap, TRUE);
	} else {
		gtk_widget_set_sensitive (dialog->word_wrap, FALSE);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->word_wrap),
					      FALSE);
	}
}

static void
restore_fonts_cb (GtkButton *button,
		  GlimmerDialogPageSetup *dialog)
{
	gnome_print_font_picker_set_font_name (
		GNOME_PRINT_FONT_PICKER (dialog->body_fontpicker),
		DEFAULT_PRINT_FONT_BODY);
	gnome_print_font_picker_set_font_name (
		GNOME_PRINT_FONT_PICKER (dialog->header_fontpicker),
		DEFAULT_PRINT_FONT_HEADER);
	gnome_print_font_picker_set_font_name (
		GNOME_PRINT_FONT_PICKER (dialog->numbers_fontpicker),
		DEFAULT_PRINT_FONT_NUMBERS);
}

static GlimmerDialogPageSetup *
dialog_page_setup_get_dialog (GlimmerView *view)
{
	GlimmerDialogPageSetup *dialog;
	GladeXML *gui;
	GtkWindow *window;
	GtkWidget *notebook, *toplevel;

	gui = glade_xml_new (GLADEDIR "dialogs.glade", "pagesetup-notebook", NULL);
	if (!gui) {
		g_warning ("Could not find dialogs.glade, reinstall glimmer");
		return NULL;
	}

	/* Unable to get toplevel GtkWindow if part of out-of-process Bonobo component. */
	toplevel = gtk_widget_get_toplevel (GTK_WIDGET (view));
	if (!GTK_WIDGET_TOPLEVEL (toplevel))
		window = NULL;
	else
		window = GTK_WINDOW (toplevel);

	dialog = g_new0 (GlimmerDialogPageSetup, 1);
	dialog->dialog = gtk_dialog_new_with_buttons (_("Page Setup"),
						      window,
						      GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_NO_SEPARATOR,
						      GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
						      NULL);
	g_return_val_if_fail (dialog->dialog != NULL, NULL);
	gtk_container_set_border_width (GTK_CONTAINER (dialog->dialog), 5);
	gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dialog->dialog)->vbox), 2);

	g_object_set_data (G_OBJECT (dialog->dialog), "GlimmerView", view);

	notebook                       = glade_xml_get_widget (gui, "pagesetup-notebook");
	dialog->print_syntax           = glade_xml_get_widget (gui, "highlighted-checkbutton");
	dialog->print_header           = glade_xml_get_widget (gui, "headers-checkbutton");
	dialog->print_line_numbers     = glade_xml_get_widget (gui, "linenumbers-checkbutton");
	dialog->print_line_every       = glade_xml_get_widget (gui, "everyline-spinbutton");
	dialog->wrap_text              = glade_xml_get_widget (gui, "printwraptext-checkbutton");
	dialog->word_wrap              = glade_xml_get_widget (gui, "printwrapword-checkbutton");
	dialog->fonts_table            = glade_xml_get_widget (gui, "fonts-table");
	dialog->restore_fonts          = glade_xml_get_widget (gui, "restorefonts-button");

	if (!notebook || !dialog->print_syntax || !dialog->print_header ||
	    !dialog->print_line_numbers || !dialog->print_line_every ||
	    !dialog->wrap_text || !dialog->word_wrap || !dialog->fonts_table ||
	    !dialog->restore_fonts) {	
		g_error ("Could not find the required widgets inside dialogs.glade");
		return NULL;
	}	

	dialog->body_fontpicker = gnome_print_font_picker_new ();
	gnome_print_font_picker_set_mode (GNOME_PRINT_FONT_PICKER (dialog->body_fontpicker),
					  GNOME_PRINT_FONT_PICKER_MODE_FONT_INFO);
	gtk_table_attach_defaults (GTK_TABLE (dialog->fonts_table), 
				   dialog->body_fontpicker, 1, 2, 0, 1);
					  
	dialog->header_fontpicker = gnome_print_font_picker_new ();
	gnome_print_font_picker_set_mode (GNOME_PRINT_FONT_PICKER (dialog->header_fontpicker),
					  GNOME_PRINT_FONT_PICKER_MODE_FONT_INFO);
	gtk_table_attach_defaults (GTK_TABLE (dialog->fonts_table), 
				   dialog->header_fontpicker, 1, 2, 1, 2);

	dialog->numbers_fontpicker = gnome_print_font_picker_new ();
	gnome_print_font_picker_set_mode (GNOME_PRINT_FONT_PICKER (dialog->numbers_fontpicker),
					  GNOME_PRINT_FONT_PICKER_MODE_FONT_INFO);
	gtk_table_attach_defaults (GTK_TABLE (dialog->fonts_table), 
				   dialog->numbers_fontpicker, 1, 2, 2, 3);

	/* Connect signals. */
	g_signal_connect (G_OBJECT (dialog->print_line_numbers), "clicked",
			  G_CALLBACK (print_line_numbers_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->wrap_text), "clicked",
			  G_CALLBACK (wrap_text_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->restore_fonts), "clicked",
			  G_CALLBACK (restore_fonts_cb), dialog);

	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog->dialog)->vbox),
			    notebook, FALSE, FALSE, 0);

	gtk_dialog_set_default_response (GTK_DIALOG (dialog->dialog),
					 GTK_RESPONSE_CLOSE);

	/*g_signal_connect (G_OBJECT (dialog->dialog), "destroy",
			  G_CALLBACK (dialog_destroyed), &dialog);*/
	g_signal_connect (G_OBJECT (dialog->dialog), "response",
			  G_CALLBACK (dialog_response_handler), dialog);

	gtk_window_set_resizable (GTK_WINDOW (dialog->dialog), FALSE);

	gtk_widget_show_all (notebook);

	return dialog;
}

void
glimmer_dialog_page_setup (GlimmerView *view)
{
	GlimmerDialogPageSetup *dialog;
	GtkWidget *window;

	dialog = g_object_get_data (G_OBJECT (view), "page-setup-dialog");
	if (dialog == NULL) {
		dialog = dialog_page_setup_get_dialog (view);
		if (dialog == NULL) {
			g_warning ("Could not create the Page Setup dialog");
			return;
		}
		/*g_object_set_data (G_OBJECT (view), "page-setup-dialog", dialog);*/
	}

	dialog_page_setup_load_settings (view, dialog);

	window = gtk_widget_get_toplevel (GTK_WIDGET (view));
	if (GTK_WIDGET_TOPLEVEL (window))
		gtk_window_set_transient_for (GTK_WINDOW (dialog->dialog),
					      GTK_WINDOW (window));

	gtk_window_present (GTK_WINDOW (dialog->dialog));
}
