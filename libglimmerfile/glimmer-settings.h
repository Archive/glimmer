/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * glimmer-settings.h
 *
 * Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GLIMMER_SETTINGS_H__
#define __GLIMMER_SETTINGS_H__

#define GLIMMER_BASE_KEY			"/apps/glimmer"
#define GLIMMER_PRINT_KEY			"/print"
#define GLIMMER_THEMES_KEY			"/themes"

#define GLIMMER_SETTING_WRAP_MODE		"/text_wrap_mode"
#define GLIMMER_SETTING_LINE_NUMBERS		"/show_line_numbers"
#define GLIMMER_SETTING_LINE_PIXMAPS		"/show_line_pixmaps"
#define GLIMMER_SETTING_LIMIT_UNDO		"/limit_undo"
#define GLIMMER_SETTING_UNDO_LEVELS		"/undo_levels"
#define GLIMMER_SETTING_INSERT_SPACES		"/insert_spaces"
#define GLIMMER_SETTING_TAB_WIDTH		"/tab_width"
#define GLIMMER_SETTING_DEFAULT_FONT		"/default_font"
#define GLIMMER_SETTING_FONT			"/editor_font"
#define GLIMMER_SETTING_AUTO_INDENT		"/auto_indent"
#define GLIMMER_SETTING_AUTO_INSERT_BRACES	"/auto_insert_braces"
#define GLIMMER_SETTING_SMART_HOME		"/smart_home"
#define GLIMMER_SETTING_SMART_INDENT		"/smart_indent"
#define GLIMMER_SETTING_COLOR_THEME		"/color_theme"
#define GLIMMER_SETTING_SHOW_MARGIN		"/show_margin"
#define GLIMMER_SETTING_MARGIN			"/margin"

#define GLIMMER_SETTING_FIND_MATCH_CASE		"/find_match_case"
#define GLIMMER_SETTING_FIND_MATCH_WORD		"/find_match_word"
#define GLIMMER_SETTING_FIND_WRAP_AROUND	"/find_wrap_around"
#define GLIMMER_SETTING_FIND_DIRECTION		"/find_direction"
#define GLIMMER_SETTING_FIND_HISTORY		"/find_history"
#define GLIMMER_SETTING_REPLACE_HISTORY		"/replace_history"
#define GLIMMER_SETTING_HISTORY_ITEMS		"/history_items"

#define GLIMMER_SETTING_PRINT_SYNTAX		"/syntax_highlighted"
#define GLIMMER_SETTING_PRINT_HEADER		"/page_header"
#define GLIMMER_SETTING_PRINT_LINE_NUMBERS	"/line_numbers"
#define GLIMMER_SETTING_PRINT_LINE_EVERY	"/line_every"
#define GLIMMER_SETTING_PRINT_WRAP_MODE		"/text_wrap_mode"
#define GLIMMER_SETTING_PRINT_FONT_BODY		"/font_body"
#define GLIMMER_SETTING_PRINT_FONT_HEADER	"/font_header"
#define GLIMMER_SETTING_PRINT_FONT_NUMBERS	"/font_numbers"

#define GLIMMER_SETTING_FILE_ENCODINGS		"/encodings"

#endif /* __GLIMMER_SETTINGS_H__ */
