/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * glimmer-view.c
 *
 * Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <gconf/gconf-client.h>
#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include <gtksourceview/gtksourcelanguagesmanager.h>
#include <libgnome/gnome-macros.h>
#include <libxml/tree.h>
#include "glimmer-view.h"
#include "glimmer-settings.h"
#include "glimmer-style-scheme.h"
#include "glimmer-themes.h"
#include "glimmer-utils.h"

#define MONOSPACE_FONT_NAME "/desktop/gnome/interface/monospace_font_name"

struct _GlimmerViewPrivate {
	GlimmerStyleScheme *default_scheme;
	GtkSourceLanguagesManager *lm;
	GlimmerThemes *themes;
	gchar *mime_type;
	gboolean use_default_font;
	gchar *font;
	gboolean overwrite;
	gint undo_levels;
	gboolean limit_undo;
	gchar *theme;
};

/* Boilerplate */
GNOME_CLASS_BOILERPLATE (GlimmerView, glimmer_view, GtkSourceView,
			 GTK_TYPE_SOURCE_VIEW);

/* Private functions. */
static void
toggle_overwrite_cb (GtkTextView *view,
		     gpointer data)
{
	GlimmerViewPrivate *priv = GLIMMER_VIEW (view)->priv;

	priv->overwrite = !priv->overwrite;
}

static void
set_style_color (const gchar *value,
		 GdkColor    *color)
{
	unsigned int r = 0, g = 0, b = 0;

	g_return_if_fail (color != NULL);

	if (value && (strlen (value) == 8) &&
	    (sscanf (value, "0x%02X%02X%02X", &r, &g, &b) == 3)) {
		color->red = r * 256;
		color->green = g * 256;
		color->blue = b * 256;
	}
}

static void
update_theme_style (GlimmerView *view,
		    GConfClient *client,
		    GConfEntry *entry,
		    const char *key)
{
	GtkSourceLanguage *language;
	char *style_name, *style_class, *style_pref;

	/* If no language is in use, there is no use continueing. */
	language = gtk_source_buffer_get_language (GTK_SOURCE_BUFFER (
		gtk_text_view_get_buffer (GTK_TEXT_VIEW (view))));
	if (!language)
		return;

	/* Do some string magic to decode the style class and preference. */
	style_name = g_strdup (key + strlen (GLIMMER_THEMES_KEY) +
			       strlen (view->priv->theme) + 2);
	style_class = strchr (style_name, '_');
	style_pref = style_class + 1;
	style_class = g_strndup (style_name, style_class - style_name);

	/* Is the style a default or language-specific style? */
	if (strchr (style_name, '/')) {
		char *lang, *lang_id, *style_id;

		/* Decompose the language and style id. */
		lang = g_strndup (style_class,
				  strchr (style_class, '/') - style_class);
		style_id = g_strdup (strchr (style_class, '/') + 1);

		lang_id = gtk_source_language_get_id (language);
		if (!strcmp (lang_id, lang)) {
			GtkSourceTagStyle *style;

			style = gtk_source_language_get_tag_style (language, style_id);
			if (!style) {
				g_warning ("Unknown style '%s'", style_id);
				g_free (style_id);
				g_free (lang_id);
				g_free (style_name);
				g_free (style_class);
				return;
			}

			if (!strcmp (style_pref, "background")) {
				set_style_color (gconf_value_get_string (entry->value),
						 &style->background);
			} else if (!strcmp (style_pref, "usebackground")) {
				if (gconf_value_get_bool (entry->value))
					style->mask |= GTK_SOURCE_TAG_STYLE_USE_BACKGROUND;
				else
					style->mask &= ~GTK_SOURCE_TAG_STYLE_USE_BACKGROUND;
			} else if (!strcmp (style_pref, "foreground")) {
				set_style_color (gconf_value_get_string (entry->value),
						 &style->foreground);
			} else if (!strcmp (style_pref, "useforeground")) {
				if (gconf_value_get_bool (entry->value))
					style->mask |= GTK_SOURCE_TAG_STYLE_USE_FOREGROUND;
				else
					style->mask &= ~GTK_SOURCE_TAG_STYLE_USE_FOREGROUND;
			} else if (!strcmp (style_pref, "bold")) {
				style->bold = gconf_value_get_bool (entry->value);
			} else if (!strcmp (style_pref, "italic")) {
				style->italic = gconf_value_get_bool (entry->value);
			} else {
				g_warning ("Unknown style preference '%s'", style_pref);
			}
				
			gtk_source_language_set_tag_style (language, style_id, style);
		}
		g_free (style_id);
		g_free (lang_id);
		g_free (lang);
	} else {
	}

	g_free (style_name);
	g_free (style_class);
}

static void 
prefs_notify_cb (GConfClient *client,
		 guint        cnxn_id,
		 GConfEntry  *entry,
		 gpointer     data)
{
	GlimmerView *view = GLIMMER_VIEW (data);
	gchar *key, *val;
	gint prefix;

	prefix = strlen (GLIMMER_BASE_KEY);
	key = g_malloc (strlen (entry->key) - prefix + 1);
	strcpy (key, entry->key + prefix);
	key[strlen (entry->key) - prefix] = '\0';

	if (!strcmp (key, GLIMMER_SETTING_WRAP_MODE)) {
		val = gconf_client_get_string (client, entry->key, NULL);
		gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (view),
			glimmer_util_get_wrap_mode_from_string (val));
		g_free (val);
	} else if (!strcmp (key, GLIMMER_SETTING_LINE_NUMBERS)) {
		gtk_source_view_set_show_line_numbers (GTK_SOURCE_VIEW (view),
			gconf_client_get_bool (client, entry->key, NULL));
	} else if (!strcmp (key, GLIMMER_SETTING_LINE_PIXMAPS)) {
		gtk_source_view_set_show_line_markers (GTK_SOURCE_VIEW (view),
			gconf_client_get_bool (client, entry->key, NULL));
	} else if (!strcmp (key, GLIMMER_SETTING_LIMIT_UNDO )) {
		glimmer_view_set_limit_undo (view,
			gconf_client_get_bool (client, entry->key, NULL));
	} else if (!strcmp (key, GLIMMER_SETTING_UNDO_LEVELS)) {
		glimmer_view_set_max_undo_levels (view,
			gconf_client_get_int (client, entry->key, NULL));
	} else if (!strcmp (key, GLIMMER_SETTING_INSERT_SPACES )) {
		gtk_source_view_set_insert_spaces_instead_of_tabs (GTK_SOURCE_VIEW (view),
			gconf_client_get_bool (client, entry->key, NULL));
	} else if (!strcmp (key, GLIMMER_SETTING_TAB_WIDTH)) {
		gtk_source_view_set_tabs_width (GTK_SOURCE_VIEW (view),
			gconf_client_get_int (client, entry->key, NULL));
	} else if (!strcmp (key, GLIMMER_SETTING_DEFAULT_FONT)) {
		glimmer_view_set_use_default_font (view,
			gconf_client_get_bool (client, entry->key, NULL));
	} else if (!strcmp (key, GLIMMER_SETTING_FONT)) {
		val = gconf_client_get_string (client, entry->key, NULL);
		glimmer_view_set_font (view, val);
		g_free (val);
	} else if (!strcmp (key, GLIMMER_SETTING_SHOW_MARGIN)) {
		gtk_source_view_set_show_margin (GTK_SOURCE_VIEW (view),
			gconf_client_get_bool (client, entry->key, NULL));
	} else if (!strcmp (key, GLIMMER_SETTING_MARGIN)) {
		gtk_source_view_set_margin (GTK_SOURCE_VIEW (view),
			gconf_client_get_int (client, entry->key, NULL));
	} else if (!strcmp (key, GLIMMER_SETTING_AUTO_INDENT)) {
		gtk_source_view_set_auto_indent (GTK_SOURCE_VIEW (view),
			gconf_client_get_bool (client, entry->key, NULL));
	} else if (!strcmp (key, GLIMMER_SETTING_COLOR_THEME)) {
		val = gconf_client_get_string (client, entry->key, NULL);
		glimmer_view_set_theme (view, val);
		g_free (val);
	} else if (strstr (key, GLIMMER_THEMES_KEY)) {
		update_theme_style (view, client, entry, key);
	}

	g_free (key);
}

static void
load_settings (GlimmerView *view)
{
	GConfClient *client;
	guint cnxn;
	char *val;

	client = gconf_client_get_default ();

	val = gconf_client_get_string (client,
				       GLIMMER_BASE_KEY GLIMMER_SETTING_WRAP_MODE,
				       NULL);
	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (view),
		glimmer_util_get_wrap_mode_from_string (val));
	g_free (val);
	gtk_source_view_set_show_line_numbers (GTK_SOURCE_VIEW (view),
		gconf_client_get_bool (client,
				       GLIMMER_BASE_KEY GLIMMER_SETTING_LINE_NUMBERS,
				       NULL));
	gtk_source_view_set_show_line_markers (GTK_SOURCE_VIEW (view),
		gconf_client_get_bool (client,
				       GLIMMER_BASE_KEY GLIMMER_SETTING_LINE_PIXMAPS,
				       NULL));
	glimmer_view_set_max_undo_levels (view,
		gconf_client_get_int (client,
				      GLIMMER_BASE_KEY GLIMMER_SETTING_UNDO_LEVELS,
				      NULL));
	glimmer_view_set_limit_undo (view,
		gconf_client_get_bool (client,
				       GLIMMER_BASE_KEY GLIMMER_SETTING_LIMIT_UNDO,
				       NULL));
	gtk_source_view_set_insert_spaces_instead_of_tabs (GTK_SOURCE_VIEW (view),
		gconf_client_get_bool (client,
				       GLIMMER_BASE_KEY GLIMMER_SETTING_INSERT_SPACES,
				       NULL));
	gtk_source_view_set_tabs_width (GTK_SOURCE_VIEW (view),
		gconf_client_get_int (client,
				      GLIMMER_BASE_KEY GLIMMER_SETTING_TAB_WIDTH,
				      NULL));
	glimmer_view_set_use_default_font (view,
		gconf_client_get_bool (client,
				       GLIMMER_BASE_KEY GLIMMER_SETTING_DEFAULT_FONT,
				       NULL));
	val = gconf_client_get_string (client,
				       GLIMMER_BASE_KEY GLIMMER_SETTING_FONT,
				       NULL);
	glimmer_view_set_font (view, val);
	g_free (val);
	gtk_source_view_set_show_margin (GTK_SOURCE_VIEW (view),
		gconf_client_get_bool (client,
				       GLIMMER_BASE_KEY GLIMMER_SETTING_SHOW_MARGIN,
				       NULL));
	gtk_source_view_set_margin (GTK_SOURCE_VIEW (view),
		gconf_client_get_int (client,
				      GLIMMER_BASE_KEY GLIMMER_SETTING_MARGIN,
				      NULL));
	gtk_source_view_set_auto_indent (GTK_SOURCE_VIEW (view),
		gconf_client_get_bool (client,
				       GLIMMER_BASE_KEY GLIMMER_SETTING_AUTO_INDENT,
				       NULL));
	val = gconf_client_get_string (client,
				       GLIMMER_BASE_KEY GLIMMER_SETTING_COLOR_THEME,
				       NULL);
	glimmer_view_set_theme (view, val);
	g_free (val);

	/* Register notify callback for preference changes. */
	gconf_client_add_dir (client, GLIMMER_BASE_KEY,
			      GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);

	cnxn = gconf_client_notify_add (client, GLIMMER_BASE_KEY,
					prefs_notify_cb, view, NULL, NULL);

	g_object_set_data (G_OBJECT (view), "NotifyConnection",
			   GINT_TO_POINTER (cnxn));

	g_object_unref (G_OBJECT (client));
}

static void
glimmer_view_finalize (GObject *object)
{
	GConfClient *client;
	guint cnxn;

	client = gconf_client_get_default ();

	cnxn = GPOINTER_TO_INT (g_object_get_data (object, "NotifyConnection"));
	gconf_client_notify_remove (client, cnxn);

	if (G_OBJECT_CLASS (parent_class)->finalize)
		G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
glimmer_view_class_init (GlimmerViewClass *klass)
{
	GObjectClass *object_class;

	object_class = (GObjectClass *) klass;
	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = glimmer_view_finalize;
}

static void
glimmer_view_instance_init (GlimmerView *view)
{
	GlimmerViewPrivate *priv;

	priv = g_new0 (GlimmerViewPrivate, 1);
	view->priv = priv;
	priv->themes = glimmer_themes_new ();
	priv->default_scheme = glimmer_style_scheme_new_default (priv->themes);
	
	/* Set the default style scheme to the GlimmerStyleScheme before
	 * creating a language manager. */
	/*gtk_source_style_scheme_set_default (GTK_SOURCE_STYLE_SCHEME (priv->default_scheme));*/
	
	priv->lm = gtk_source_languages_manager_new ();
	priv->overwrite = FALSE;
	priv->theme = g_strdup ("Default");

	g_signal_connect (G_OBJECT (view), "toggle-overwrite",
			  G_CALLBACK (toggle_overwrite_cb), view);
}

#if 0
static void
glimmer_view_install_popup_menu (GlimmerView *view)
{
	BonoboUIComponent *popup_component;

	g_return_if_fail (file != NULL);

	popup_component = bonobo_control_get_popup_ui_component (file->priv->control);
	bonobo_ui_component_set (popup_component, "/", "<popups/>", NULL);
	bonobo_ui_component_set_translate (popup_component,
					   "/popups",
					   glimmer_view_popup_menu_xml,
					   NULL);
	bonobo_ui_component_add_verb_list_with_data (popup_component,
						     glimmer_view_popup_menu_verbs,
						     file);

	g_signal_connect (G_OBJECT (file->view),
			  "button_press_event",
			  G_CALLBACK (button_press_cb),
			  file);
}

static void
glimmer_view_insert_cb (GtkTextBuffer *buffer,
			GtkTextIter   *iter,
			const gchar   *text,
			gint           len,
			GlimmerView   *view)
{
	static gboolean tab_insert = FALSE;

	g_return_if_fail (file != NULL);
	g_return_if_fail (GLIMMER_IS_GLIMMER_FILE (file));

	/* Return immediately if file is being loaded. */
	if (file->priv->is_loading)
		return;

	/* Return immediately if this callback was called because of a
	   convert-tab-to-spaces insert. */
	if (tab_insert)
		return;

	if (GTK_TEXT_VIEW (file->view)->editable)
		glimmer_view_set_modified (file, TRUE);

	if (!strcmp (text, "\t") && file->priv->insert_spaces) {
		GtkTextIter *start;
		GtkTextIter new_iter;
		gchar *spaces;
		int i;

		/* Get the GtkTextIter before the tab character, remove the tab
		   character, and insert spaces instead. */
		start = gtk_text_iter_copy (iter);
		gtk_text_iter_backward_char (start);
		gtk_text_buffer_delete (buffer, start, iter);
		tab_insert = TRUE;
		spaces = g_malloc (file->priv->tab_width + 1);
		for (i=0; i<file->priv->tab_width; i++)
			spaces[i] = ' ';
		spaces[file->priv->tab_width] = '\0';
		gtk_text_buffer_insert (buffer, start, spaces, file->priv->tab_width);
		g_free (spaces);
		tab_insert = FALSE;
		gtk_text_buffer_get_iter_at_offset (buffer,
						    &new_iter,
						    gtk_text_iter_get_offset (start) + file->priv->tab_width);
		gtk_text_iter_free (start);

		/* Call update_ui with the new GtkTextIter. */
		glimmer_view_update_ui (buffer,
					&new_iter,
					gtk_text_buffer_get_insert (buffer),
					file);
	} else
		glimmer_view_update_ui (buffer,
					iter,
					gtk_text_buffer_get_insert (buffer),
					file);
}

static void
glimmer_view_delete_cb (GtkTextBuffer *buffer,
			GtkTextIter   *start,
			GtkTextIter   *end,
			GlimmerView   *view)
{
	g_return_if_fail (file != NULL);
	g_return_if_fail (GLIMMER_IS_GLIMMER_FILE (file));

	/* Return immediately if file is being loaded. */
	if (file->priv->is_loading)
		return;

	if (GTK_TEXT_VIEW (file->view)->editable)
		glimmer_view_set_modified (file, TRUE);

	glimmer_view_update_ui (buffer,
				start,
				gtk_text_buffer_get_insert (buffer),
				file);
}

static void
can_undo_cb (GtkSourceBuffer *buffer,
	     gboolean         can_undo,
	     GlimmerView     *view)
{
	BonoboUIComponent *component = bonobo_control_get_ui_component (file->priv->control);

	bonobo_ui_component_set_prop (component,
				      "/commands/EditUndo",
				      "sensitive",
				      can_undo ? "1" : "0",
				      NULL);
}

static void
can_redo_cb (GtkSourceBuffer *buffer,
	     gboolean         can_redo,
	     GlimmerView     *view)
{
	BonoboUIComponent *component = bonobo_control_get_ui_component (file->priv->control);

	bonobo_ui_component_set_prop (component,
				      "/commands/EditRedo",
				      "sensitive",
				      can_redo ? "1" : "0",
				      NULL);
}

static int
calculate_line_indentation (GlimmerView *view, int line)
{
	GtkTextBuffer *buffer;
	GtkTextIter start;
	gboolean use_spaces;
	char c;
	int indent = 0;
	int spaces = 0;

	g_return_val_if_fail (file != NULL, 0);
	g_return_val_if_fail (GLIMMER_IS_GLIMMER_FILE (file), 0);

	buffer = GTK_TEXT_VIEW (file->view)->buffer;
	use_spaces = file->priv->insert_spaces;

	gtk_text_buffer_get_iter_at_line (buffer, &start, line);
	while (!gtk_text_iter_ends_line (&start)) {
		c = gtk_text_iter_get_char (&start);
		if (c == '\t') {
			indent++;
		} else if (c == ' ' && use_spaces) {
			spaces++;
			if (spaces == file->priv->tab_width) {
				indent++;
				spaces = 0;
			}
		} else {
			break;
		}

		gtk_text_iter_forward_char (&start);
	}

	return indent;
}

static gboolean
find_opening_parens (gunichar ch, gpointer user_data)
{
	return ch == '(';
}

static gint
key_press_cb (GtkWidget   *widget,
	      GdkEventKey *event,
	      GlimmerView *view)
{
	GtkTextBuffer *buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget));
	gunichar prevchar;
	GtkTextIter cur;
	GtkTextIter iter;
	GtkTextIter iter2;
	GtkTextIter lineiter;
	GtkTextMark *mark;
	gboolean shift_state;
	gboolean control_state;
	int key;

	shift_state = event->state & GDK_SHIFT_MASK;
	control_state = event->state & GDK_CONTROL_MASK;
	key = event->keyval;

	mark = gtk_text_buffer_get_mark (buf, "insert");
	gtk_text_buffer_get_iter_at_mark (buf, &cur, mark);
	iter = iter2 = lineiter = cur;

	gtk_text_iter_backward_char (&cur);
	prevchar = gtk_text_iter_get_char (&cur);

	/* we set it back to cur pos */
	cur = iter;

	/* Auto insert braces. */
	if (key == GDK_braceleft && file->priv->auto_insert_braces &&
	    gtk_text_iter_ends_line (&cur)) {
		/* Auto-insert-braces means that when you press the { key to
		 * start a code block (method, if, for or while loop etc) that
		 * it automatically inserts the closing brace at the correct
		 * indentation. It also adds a new line between the braces,
		 * indented one level higher and positions the cursor there so
		 * you can start writing the code block immediately. */
		int line_indent;
		char *indent, *new_indent;

		gtk_text_iter_backward_find_char (&iter,
						  find_opening_parens,
						  NULL,
						  NULL);
		line_indent = calculate_line_indentation (file, gtk_text_iter_get_line (&iter));

		/* Calculate line indentation and create indent string. */
		if (file->priv->insert_spaces) {
			indent = make_spaces_string (line_indent * file->priv->tab_width);
			new_indent = make_spaces_string ((line_indent + 1) * file->priv->tab_width);
		} else {
			int i;

			indent = g_malloc (line_indent + 1);
			for (i = 0; i<line_indent; i++)
				indent[i] = '\t';
			indent[line_indent] = '\0';

			new_indent = g_malloc (line_indent + 2);
			for (i = 0; i<line_indent + 1; i++)
				new_indent[i] = '\t';
			new_indent[line_indent + 1] = '\0';
		}

		/* Insert opening brace, new line and closing brace. */
		gtk_text_buffer_begin_user_action (buf);
		gtk_text_buffer_insert (buf, &cur, "{\n", 2);
		gtk_text_buffer_insert (buf, &cur, new_indent, strlen (new_indent));
		gtk_text_buffer_insert (buf, &cur, "\n", 1);
		gtk_text_buffer_insert (buf, &cur, indent, strlen (indent));
		gtk_text_buffer_insert (buf, &cur, "}", 1);
		gtk_text_buffer_end_user_action (buf);

		/* Position cursor on the new line. */
		gtk_text_buffer_get_iter_at_mark (buf, &iter, mark);
		gtk_text_buffer_get_iter_at_offset (buf,
						    &iter,
						    gtk_text_iter_get_offset (&iter) - (2 + strlen (indent)));
		gtk_text_buffer_place_cursor (buf, &iter);
		gtk_text_view_scroll_mark_onscreen (GTK_TEXT_VIEW (widget),
						    gtk_text_buffer_get_insert (buf));
		g_free (indent);
		g_free (new_indent);
		return TRUE;
	} else if (key == GDK_Return && file->priv->smart_indent &&
		   prevchar == ',') {
		/* Smart indent means that it indents text "smartly" (duh).
		 * Variables in a method call/definition for example will be
		 * indented at the same level as the location of the opening
		 * parenthesis. Furthermore, when you press TAB on a new line,
		 * it will indent to the same level as the previous line (this
		 * is not necessary if you've already got auto-indent enabled).
		 */
		if (gtk_text_iter_backward_find_char (&iter,
						      find_opening_parens,
						      NULL,
						      NULL)) {
			char *indent;
			int level = 1;

			while (!gtk_text_iter_starts_line (&iter)) {
				char c = gtk_text_iter_get_char (&iter);
				if (c == '\t')
					level += file->priv->tab_width;
				else
					level++;
				gtk_text_iter_backward_char (&iter);
			}

			if (file->priv->insert_spaces) {
				indent = make_spaces_string (level);
			} else {
				int i, tabs, spaces;

				tabs = level / file->priv->tab_width;
				spaces = level % file->priv->tab_width;

				indent = g_malloc (tabs + spaces + 1);
				for (i = 0; i < tabs; i++)
					indent[i] = '\t';
				for (i = 0; i < spaces; i++)
					indent[tabs + i] = ' ';
				indent[tabs + spaces] = '\0';
			}

			gtk_text_buffer_begin_user_action (buf);
			gtk_text_buffer_insert (buf, &cur, "\n", 1);
			gtk_text_buffer_insert (buf, &cur, indent, strlen (indent));
			g_free (indent);
			gtk_text_buffer_end_user_action (buf);
			return TRUE;
		}
	} else if (key == GDK_Return && file->priv->auto_indent &&
		   gtk_text_iter_ends_line (&cur)) {
		/* Auto-indent means that when you press ENTER at the end of a
		 * line, the new line is automatically indented at the same
		 * level as the previous line.
		 */
		int line_indent;
		char *indent;

		/* Calculate line indentation and create indent string. */
		line_indent = calculate_line_indentation (file, gtk_text_iter_get_line (&cur));
		if (prevchar == '{')
			line_indent++;

		if (file->priv->insert_spaces) {
			indent = make_spaces_string (line_indent * file->priv->tab_width);
		} else {
			int i;

			indent = g_malloc (line_indent + 1);
			for (i = 0; i<line_indent; i++)
				indent[i] = '\t';
			indent[line_indent] = '\0';
		}

		/* Insert new line and auto-indent. */
		gtk_text_buffer_begin_user_action (buf);
		gtk_text_buffer_insert (buf, &cur, "\n", 1);
		gtk_text_buffer_insert (buf, &cur, indent, strlen (indent));
		g_free (indent);
		gtk_text_buffer_end_user_action (buf);
		gtk_text_view_scroll_mark_onscreen (GTK_TEXT_VIEW (widget),
						    gtk_text_buffer_get_insert (buf));
		return TRUE;
	} else if (key == GDK_Home && file->priv->smart_home) {
		/* Smart home means that when you press the HOME key, the cursor
		 * will jump to the first character on the line. The second time
		 * you type the HOME key, it will jump to the start of the line.
		 */
		if (!gtk_text_iter_starts_sentence (&cur)) {
			gtk_text_iter_backward_chars (&cur, gtk_text_iter_get_line_offset (&cur));
			while (!gtk_text_iter_ends_line (&cur)) {
				char c = gtk_text_iter_get_char (&cur);
				if (c == '\t' || c == ' ')
					gtk_text_iter_forward_char (&cur);
				else
					break;
			}
			if (!gtk_text_iter_equal (&cur, &iter)) {
				gtk_text_buffer_place_cursor (buf, &cur);
				return TRUE;
			}
		}
	} else if (key == GDK_Tab) {
		/* Replace the default GtkTextView behavior of replacing
		 * several lines of text with a tab character when you press TAB
		 * with the functionality where those lines are automatically
		 * indented one level more (or shift-TAB to unindent one level).
		 * This functionality only works when you select an entire line,
		 * or at least text starting from a line offset of 0.
		 */
		GtkTextIter start, end;
		if (gtk_text_buffer_get_selection_bounds (buf, &start, &end) &&
		    gtk_text_iter_starts_line (&start)) {
			int i, start_line, end_line;

			gtk_text_buffer_begin_user_action (buf);

			start_line = gtk_text_iter_get_line (&start);
			end_line = gtk_text_iter_get_line (&end);

			for (i = start_line; i < end_line; i++) {
				gtk_text_buffer_get_iter_at_line (buf, &iter, i);

				/* FIXME: SHIFT-TAB currently focuses the previous
				 * widget. This should probably not happen (CTRL-TAB
				 * and CTRL-SHIFT-TAB are reserved for tabbing
				 * out of a GtkTextView). Bug in GTK probably.
				 */
				if (shift_state) {
					if (gtk_text_iter_get_char (&iter) == '\t') {
						iter2 = iter;
						gtk_text_iter_forward_char (&iter2);
						gtk_text_buffer_delete (buf, &iter, &iter2);
					} else if (gtk_text_iter_get_char (&iter) == ' ') {
						int spaces = 0;
						while (!gtk_text_iter_ends_line (&iter)) {
							if (gtk_text_iter_get_char (&iter) == ' ')
								spaces++;
							else
								break;
							gtk_text_iter_forward_char (&iter);
						}
						iter2 = iter;
						gtk_text_iter_forward_chars (&iter2, spaces);
						gtk_text_buffer_delete (buf, &iter, &iter2);
					}
				} else {
					if (file->priv->insert_spaces) {
						char *indent = make_spaces_string (file->priv->tab_width);
						gtk_text_buffer_insert (buf, &iter, indent, strlen (indent));
						g_free (indent);
					} else {
						gtk_text_buffer_insert (buf, &iter, "\t", 1);
					}
				}
			}

			/* Adjust the selection bound to the new indented line. */
			gtk_text_buffer_get_iter_at_line (buf, &iter, start_line);
			gtk_text_buffer_move_mark_by_name (buf,
							   "selection_bound",
							   &iter);

			gtk_text_buffer_end_user_action (buf);

			return TRUE;
		}
	} else if (key == GDK_parenleft && file->priv->auto_insert_parens &&
		   gtk_text_iter_ends_line (&cur)) {
		/* FIXME: implement CodeInsight functionality. */
	}

	return GTK_WIDGET_CLASS (parent_class)->key_press_event (widget, event);
}

static gint
button_press_cb (GtkWidget      *widget,
		 GdkEventButton *event,
		 GlimmerView    *view)
{
	gboolean handled = FALSE;

	if (event->type == GDK_BUTTON_PRESS && event->button == 3) {
		if (!GTK_WIDGET_HAS_FOCUS (widget))
			gtk_widget_grab_focus (widget);
		undo_check (file);
		redo_check (file);
		cut_check (file);
		copy_check (file);
		paste_check (file);

		bonobo_control_do_popup (file->priv->control,
					 event->button,
					 event->time);
		handled = TRUE;
	}

	if (handled)
		g_signal_stop_emission_by_name (widget, "button_press_event");

	return handled;
}

static gchar *
make_spaces_string (gint spaces)
{
	gchar *string = NULL;

	if (spaces > 10)
		spaces = 10;
	else if (spaces < 0)
		spaces = 1;
	string = g_new (char, spaces + 1);

	if (string) {
		memset (string, ' ', spaces);
		string[spaces] = '\0';
	}
	return (string);
}

static void
undo_check (GlimmerView *view)
{
	BonoboUIComponent *component;
	GtkSourceBuffer *buffer;
	gchar *value;

	component = bonobo_control_get_popup_ui_component (file->priv->control);
	buffer = GTK_SOURCE_BUFFER (GTK_TEXT_VIEW (file->view)->buffer);
	value = gtk_source_buffer_can_undo (buffer) ? "1" : "0";
	bonobo_ui_component_set_prop (component, "/commands/undo",
				      "sensitive", value, NULL);
}

static void
redo_check (GlimmerView *view)
{
	BonoboUIComponent *component;
	GtkSourceBuffer *buffer;
	gchar *value;

	component = bonobo_control_get_popup_ui_component (file->priv->control);
	buffer = GTK_SOURCE_BUFFER (GTK_TEXT_VIEW (file->view)->buffer);
	value = gtk_source_buffer_can_redo (buffer) ? "1" : "0";
	bonobo_ui_component_set_prop (component, "/commands/redo",
				      "sensitive", value, NULL);
}

static void
cut_check (GlimmerView *view)
{
	BonoboUIComponent *component;
	GtkTextBuffer *buffer;
	GtkTextIter start;
	GtkTextIter end;
	gchar *value;

	component = bonobo_control_get_popup_ui_component (file->priv->control);
	buffer = GTK_TEXT_VIEW (file->view)->buffer;
	value = gtk_text_buffer_get_selection_bounds (buffer, &start, &end) ?
		"1" : "0";
	bonobo_ui_component_set_prop (component, "/commands/cut",
				      "sensitive", value, NULL);
}

static void
copy_check (GlimmerView *view)
{
	BonoboUIComponent *component;
	GtkTextBuffer *buffer;
	GtkTextIter start;
	GtkTextIter end;
	gchar *value;

	component = bonobo_control_get_popup_ui_component (file->priv->control);
	buffer = GTK_TEXT_VIEW (file->view)->buffer;
	value = gtk_text_buffer_get_selection_bounds (buffer, &start, &end) ?
		"1" : "0";
	bonobo_ui_component_set_prop (component, "/commands/copy",
				      "sensitive", value, NULL);
}

static void
paste_check (GlimmerView *view)
{
}
#endif

/* ----------------------------------------------------------------------
 * Public interface 
 * ---------------------------------------------------------------------- */

GtkWidget *
glimmer_view_new (void)
{
	GtkWidget *view;
	GtkSourceBuffer *buffer;

	buffer = gtk_source_buffer_new (NULL);
	view = glimmer_view_new_with_buffer (buffer);
	g_object_unref (buffer);

	return view;
}

GtkWidget *
glimmer_view_new_with_buffer (GtkSourceBuffer *buffer)
{
	GtkWidget *view;

	g_return_val_if_fail (buffer != NULL, NULL);
	g_return_val_if_fail (GTK_IS_SOURCE_BUFFER (buffer), NULL);

	view = g_object_new (GLIMMER_TYPE_VIEW, NULL);
	gtk_text_view_set_buffer (GTK_TEXT_VIEW (view), GTK_TEXT_BUFFER (buffer));

	/* Call load_settings only _after_ the GtkTextView has a buffer set.
	 * Otherwise you'll have problems setting the undo-levels since
	 * the buffer is still NULL at that point. NOTE: if the user changes the
	 * buffer after creation, the settings won't be reloaded (isn't a
	 * problem except for the undo-levels which get set on the buffer).
	 * Can't be fixed until GTK+ 2.4 when we have a "buffer" property on the
	 * GtkTextView.
	 */
	load_settings (GLIMMER_VIEW (view));

	return view;
}

gchar *
glimmer_view_get_mime_type (GlimmerView *view)
{
	g_return_val_if_fail (view != NULL, NULL);
	g_return_val_if_fail (GLIMMER_IS_VIEW (view), NULL);

	if (view->priv->mime_type)
		return g_strdup (view->priv->mime_type);

	return NULL;
}

void
glimmer_view_set_mime_type (GlimmerView *view,
			    const gchar *mime_type)
{
	GtkSourceLanguage *language = NULL;
	GlimmerStyleScheme *scheme;

	g_return_if_fail (view != NULL);
	g_return_if_fail (GLIMMER_IS_VIEW (view));
	g_return_if_fail (mime_type != NULL);

	if (view->priv->mime_type)
		g_free (view->priv->mime_type);

	view->priv->mime_type = g_strdup (mime_type);

	language = gtk_source_languages_manager_get_language_from_mime_type (view->priv->lm,
									     mime_type);

	if (language == NULL) {
		g_warning ("No language found for mime type `%s'\n", mime_type);
		g_object_set (G_OBJECT (GTK_TEXT_VIEW (view)->buffer), "highlight", FALSE, NULL);
		gtk_source_buffer_set_language (GTK_SOURCE_BUFFER (GTK_TEXT_VIEW (view)->buffer),
						NULL);
	} else {
		scheme = glimmer_style_scheme_new (view->priv->themes, language,
						   view->priv->theme);
		gtk_source_language_set_style_scheme (language,
						      GTK_SOURCE_STYLE_SCHEME (scheme));

		g_object_set (G_OBJECT (GTK_TEXT_VIEW (view)->buffer), "highlight", TRUE, NULL);
		gtk_source_buffer_set_language (GTK_SOURCE_BUFFER (GTK_TEXT_VIEW (view)->buffer),
						language);
	}
}

gint
glimmer_view_get_max_undo_levels (GlimmerView *view)
{
	g_return_val_if_fail (view != NULL, -1);
	g_return_val_if_fail (GLIMMER_IS_VIEW (view), -1);

	return gtk_source_buffer_get_max_undo_levels (
		GTK_SOURCE_BUFFER (GTK_TEXT_VIEW (view)->buffer));
}

void
glimmer_view_set_max_undo_levels (GlimmerView *view,
				  gint undo_levels)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (GLIMMER_IS_VIEW (view));

	view->priv->undo_levels = undo_levels;
	gtk_source_buffer_set_max_undo_levels (
		GTK_SOURCE_BUFFER (GTK_TEXT_VIEW (view)->buffer), undo_levels);
}

gboolean
glimmer_view_get_limit_undo (GlimmerView *view)
{
	g_return_val_if_fail (view != NULL, FALSE);
	g_return_val_if_fail (GLIMMER_IS_VIEW (view), FALSE);

	return view->priv->limit_undo;
}

void
glimmer_view_set_limit_undo (GlimmerView *view,
			     gboolean limit_undo)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (GLIMMER_IS_VIEW (view));

	view->priv->limit_undo = limit_undo;
	if (limit_undo)
		glimmer_view_set_max_undo_levels (view, -1);
	else
		glimmer_view_set_max_undo_levels (view, view->priv->undo_levels);
}

gboolean
glimmer_view_get_use_default_font (GlimmerView *view)
{
	g_return_val_if_fail (view != NULL, FALSE);
	g_return_val_if_fail (GLIMMER_IS_VIEW (view), FALSE);

	return view->priv->use_default_font;
}

void
glimmer_view_set_use_default_font (GlimmerView *view,
				   gboolean     use_default)
{
	GConfClient *client;
	PangoFontDescription *font_desc;
	gchar *font;

	g_return_if_fail (view != NULL);
	g_return_if_fail (GLIMMER_IS_VIEW (view));

	if (use_default) {
		client = gconf_client_get_default ();
		font = gconf_client_get_string (client,
						MONOSPACE_FONT_NAME,
						NULL);
		if (font) {
			font_desc = pango_font_description_from_string (font);
			if (font_desc != NULL) {
				gtk_widget_modify_font (GTK_WIDGET (view), font_desc);
				pango_font_description_free (font_desc);
			}
			g_free (font);
		}

		g_object_unref (G_OBJECT (client));
	} else if (view->priv->font != NULL) {
		font = g_strdup (view->priv->font);
		glimmer_view_set_font (view, font);
		g_free (font);
	}

	view->priv->use_default_font = use_default;
}

gchar *
glimmer_view_get_font (GlimmerView *view)
{
	g_return_val_if_fail (view != NULL, NULL);
	g_return_val_if_fail (GLIMMER_IS_VIEW (view), NULL);

	return g_strdup (view->priv->font);
}

void
glimmer_view_set_font (GlimmerView *view,
		       const gchar *font)
{
	PangoFontDescription *font_desc = NULL;

	g_return_if_fail (view != NULL);
	g_return_if_fail (GLIMMER_IS_VIEW (view));
	g_return_if_fail (font != NULL);

	font_desc = pango_font_description_from_string (font);
	g_return_if_fail (font_desc != NULL);

	gtk_widget_modify_font (GTK_WIDGET (view), font_desc);

	pango_font_description_free (font_desc);

	if (view->priv->font)
		g_free (view->priv->font);
	view->priv->font = g_strdup (font);

	/* We need to update the tab width when the font changes. */
	/*glimmer_view_set_tab_width (file, glimmer_view_get_tab_width (file));*/
}

const GSList *
glimmer_view_get_available_languages (GlimmerView *view)
{
	g_return_val_if_fail (view != NULL, NULL);
	g_return_val_if_fail (GLIMMER_IS_VIEW (view), NULL);

	return gtk_source_languages_manager_get_available_languages (view->priv->lm);
}

const GSList *
glimmer_view_get_available_themes (GlimmerView *view)
{
	g_return_val_if_fail (view != NULL, NULL);
	g_return_val_if_fail (GLIMMER_IS_VIEW (view), NULL);

	return glimmer_themes_get_available_themes (view->priv->themes);
}

gchar *
glimmer_view_get_theme (GlimmerView *view)
{
	g_return_val_if_fail (view != NULL, FALSE);
	g_return_val_if_fail (GLIMMER_IS_VIEW (view), FALSE);

	return g_strdup (view->priv->theme);
}

void
glimmer_view_set_theme (GlimmerView *view,
			const gchar *theme)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (GLIMMER_IS_VIEW (view));
	g_return_if_fail (theme != NULL);	
}

gboolean
glimmer_view_get_overwrite_mode (GlimmerView *view)
{
	g_return_val_if_fail (view != NULL, FALSE);
	g_return_val_if_fail (GLIMMER_IS_VIEW (view), FALSE);

	return view->priv->overwrite;
}

#if 0
void
glimmer_view_update_styles (GlimmerView *view)
{
	GtkSourceBuffer *buffer = GTK_SOURCE_BUFFER (GTK_TEXT_VIEW (file->view)->buffer);
	GSList *tags;
	GtkTextTag *tag;
	GlimmerViewStyle *style;

	g_return_if_fail (file != NULL);
	g_return_if_fail (GLIMMER_IS_GLIMMER_FILE (file));

	/* Update all tags. */
	for (tags = gtk_source_buffer_get_regex_tags (buffer); tags; tags = tags->next) {
		tag = GTK_TEXT_TAG (tags->data);
		glimmer_view_style_cache_update_tag (tag);
	}

	/* Update non-tag styles. */
	style = glimmer_view_style_cache_get_style_from_class ("plain-text");
	gtk_widget_modify_base (file->view, GTK_STATE_NORMAL, &style->background);
	gtk_widget_modify_text (file->view, GTK_STATE_NORMAL, &style->foreground);
	style = glimmer_view_style_cache_get_style_from_class ("gutter");
	gtk_widget_modify_bg (file->view, GTK_STATE_NORMAL, &style->background);
	gtk_widget_modify_fg (file->view, GTK_STATE_NORMAL, &style->foreground);

	/* Redraw widget with new styles. */
	gtk_widget_queue_draw (GTK_WIDGET (file->view));
}

#endif

GlimmerThemes *
_glimmer_view_get_themes (GlimmerView *view)
{
	g_return_val_if_fail (GLIMMER_IS_VIEW (view), NULL);

	return view->priv->themes;
}
