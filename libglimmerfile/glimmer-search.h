/*  glimmer-file-search.h
 *
 *  Search routines for Glimmer
 *  Chris Phelps, 2001
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GLIMMER_FILE_SEARCH_H_
#define _GLIMMER_FILE_SEARCH_H_

#include "glimmer-file.h"

G_BEGIN_DECLS

void glimmer_file_search_load_config (void);
void glimmer_file_search_save_config (void);

void glimmer_file_find_cb (BonoboUIComponent *component, GlimmerFile *file, const gchar *verb);
void glimmer_file_find_next_cb (BonoboUIComponent *component, GlimmerFile *file, const gchar *verb);
void glimmer_file_replace_cb (BonoboUIComponent *component, GlimmerFile *file, const gchar *verb);
void glimmer_file_goto_line_cb (BonoboUIComponent *component, GlimmerFile *file, const gchar *verb);
void glimmer_file_match_bracket_cb (BonoboUIComponent *component, GlimmerFile *file, const gchar *verb);

G_END_DECLS

#endif
