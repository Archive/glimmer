/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  glimmer-themes.h
 *
 *  Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GLIMMER_THEMES_H__
#define __GLIMMER_THEMES_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define GLIMMER_TYPE_THEMES		(glimmer_themes_get_type ())
#define GLIMMER_THEMES(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), GLIMMER_TYPE_THEMES, GlimmerThemes))
#define GLIMMER_THEMES_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), GLIMMER_TYPE_THEMES, GlimmerThemesClass))
#define GLIMMER_IS_THEMES(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), GLIMMER_TYPE_THEMES))
#define GLIMMER_IS_THEMES_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), GLIMMER_TYPE_THEMES))

typedef struct _GlimmerThemes        GlimmerThemes;
typedef struct _GlimmerThemesPrivate GlimmerThemesPrivate;
typedef struct _GlimmerThemesClass   GlimmerThemesClass;

struct _GlimmerThemes {
	GObject parent;

	GlimmerThemesPrivate *priv;
};

struct _GlimmerThemesClass {
	GObjectClass parent_class;
};

GType          glimmer_themes_get_type			(void) G_GNUC_CONST;

GlimmerThemes *glimmer_themes_new			(void);

GSList        *glimmer_themes_get_available_themes	(GlimmerThemes *themes);

GHashTable    *glimmer_themes_get_style_tags            (GlimmerThemes *themes,
							 const gchar   *theme,
							 const gchar   *language_id);

G_END_DECLS

#endif /* __GLIMMER_THEMES_H__ */
