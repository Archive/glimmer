/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  glimmer-view.h
 *
 *  Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GLIMMER_VIEW_H__
#define __GLIMMER_VIEW_H__

#include <gtksourceview/gtksourceview.h>
#include "glimmer-themes.h"

G_BEGIN_DECLS

#define GLIMMER_TYPE_VIEW		(glimmer_view_get_type ())
#define GLIMMER_VIEW(o)			(G_TYPE_CHECK_INSTANCE_CAST ((o), GLIMMER_TYPE_VIEW, GlimmerView))
#define GLIMMER_VIEW_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), GLIMMER_TYPE_VIEW, GlimmerViewClass))
#define GLIMMER_IS_VIEW(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), GLIMMER_TYPE_VIEW))
#define GLIMMER_IS_VIEW_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), GLIMMER_TYPE_VIEW))

typedef struct _GlimmerView        GlimmerView;
typedef struct _GlimmerViewPrivate GlimmerViewPrivate;
typedef struct _GlimmerViewClass   GlimmerViewClass;

struct _GlimmerView {
	GtkSourceView parent;

	GlimmerViewPrivate *priv;
};

struct _GlimmerViewClass {
	GtkSourceViewClass parent_class;
};

GType          glimmer_view_get_type			(void) G_GNUC_CONST;

GtkWidget     *glimmer_view_new				(void);
GtkWidget     *glimmer_view_new_with_buffer		(GtkSourceBuffer *buffer);

gchar         *glimmer_view_get_mime_type		(GlimmerView *view);
void           glimmer_view_set_mime_type		(GlimmerView *view,
							 const gchar *mime_type);

gint           glimmer_view_get_max_undo_levels		(GlimmerView *view);
void           glimmer_view_set_max_undo_levels		(GlimmerView *view,
							 gint         undo_levels);

gboolean       glimmer_view_get_limit_undo		(GlimmerView *view);
void           glimmer_view_set_limit_undo		(GlimmerView *view,
							 gboolean     limit_undo);

gboolean       glimmer_view_get_use_default_font	(GlimmerView *view);
void           glimmer_view_set_use_default_font	(GlimmerView *view,
							 gboolean     use_default);

gchar         *glimmer_view_get_font			(GlimmerView *view);
void           glimmer_view_set_font			(GlimmerView *view,
							 const gchar *font);

const GSList  *glimmer_view_get_available_languages     (GlimmerView *view);

const GSList  *glimmer_view_get_available_themes        (GlimmerView *view);
gchar         *glimmer_view_get_theme                   (GlimmerView *view);
void           glimmer_view_set_theme                   (GlimmerView *view,
							 const gchar *theme);

/* FIXME: GtkTextView needs to have a property/API to do this (bug #110241) */
gboolean       glimmer_view_get_overwrite_mode		(GlimmerView *view);

/* INTERNAL private stuff - not even exported from the library on many platforms. */
GlimmerThemes *_glimmer_view_get_themes                 (GlimmerView *view);

G_END_DECLS

#endif /* __GLIMMER_VIEW_H__ */
