/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 *  glimmer-commands.h
 *
 *  Copyright (C) 2003 - Jeroen Zwartepoorte
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GLIMMER_COMMANDS_H__
#define __GLIMMER_COMMANDS_H__

#include <bonobo/bonobo-ui-component.h>
#include "glimmer-view.h"

G_BEGIN_DECLS

void undo_cb		(BonoboUIComponent *component,
			 GlimmerView       *view,
			 const gchar       *verb);
void redo_cb		(BonoboUIComponent *component,
			 GlimmerView       *view,
			 const gchar       *verb);
void cut_cb		(BonoboUIComponent *component,
			 GlimmerView       *view,
			 const gchar       *verb);
void copy_cb		(BonoboUIComponent *component,
			 GlimmerView       *view,
			 const gchar       *verb);
void paste_cb		(BonoboUIComponent *component,
			 GlimmerView       *view,
			 const gchar       *verb);
void select_all_cb	(BonoboUIComponent *component,
			 GlimmerView       *view,
			 const gchar       *verb);
void preferences_cb	(BonoboUIComponent *component,
			 GlimmerView       *view,
			 const gchar       *verb);
void page_setup_cb	(BonoboUIComponent *component,
			 GlimmerView       *view,
			 const gchar       *verb);
void print_preview_cb	(BonoboUIComponent *component,
			 GlimmerView       *view,
			 const gchar       *verb);
void print_cb		(BonoboUIComponent *component,
			 GlimmerView       *view,
			 const gchar       *verb);
void find_cb		(BonoboUIComponent *component,
			 GlimmerView       *view,
			 const gchar       *verb);
void find_next_cb	(BonoboUIComponent *component,
			 GlimmerView       *view,
			 const gchar       *verb);
void find_previous_cb	(BonoboUIComponent *component,
			 GlimmerView       *view,
			 const gchar       *verb);
void replace_cb		(BonoboUIComponent *component,
			 GlimmerView       *view,
			 const gchar       *verb);
void goto_line_cb	(BonoboUIComponent *component,
			 GlimmerView       *view,
			 const gchar       *verb);

G_END_DECLS

#endif /* __GLIMMER_COMMANDS_H__ */
