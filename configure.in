dnl Process this file with autoconf to produce a configure script.

AC_PREREQ(2.52)
AC_INIT(glimmer, 1.99.0)

AM_CONFIG_HEADER(config.h)
AC_CONFIG_SRCDIR(libglimmerfile/glimmer-view.c)

AM_INIT_AUTOMAKE(AC_PACKAGE_NAME, AC_PACKAGE_VERSION)

AM_MAINTAINER_MODE

AC_PROG_INTLTOOL

GNOME_COMPILE_WARNINGS(maximum)

dnl ==========================================================================
dnl Before making a release, the LTVERSION string should be modified.
dnl The string is of the form C:R:A.
dnl - If interfaces have been changed or added, but binary compatibility has
dnl   been preserved, change to C+1:0:A+1
dnl - If binary compatibility has been broken (eg removed or changed
dnl   interfaces) change to C+1:0:0
dnl - If the interface is the same as the previous version, change to C:R+1:A
dnl ==========================================================================

LTVERSION=0:1:0
AC_SUBST(LTVERSION)

dnl ==========================================================================
dnl
dnl If you add a version number here, you *must* add an AC_SUBST line for
dnl it too, or it will never make it into the spec file!
dnl
dnl ==========================================================================

GTK_REQUIRED=2.2.2
XML_REQUIRED=2.5.8
SRCVIEW_REQUIRED=0.5.0
ACTIVATION_REQUIRED=2.2.4
VFS_REQUIRED=2.2.5
BONOBO_REQUIRED=2.2.4
GNOME_REQUIRED=2.2.2
PRINT_REQUIRED=2.2.1
LIBGLADE_REQUIRED=2.0.1
EEL_REQUIRED=2.2.4

AC_SUBST(GTK_REQUIRED)
AC_SUBST(XML_REQUIRED)
AC_SUBST(SRCVIEW_REQUIRED)
AC_SUBST(ACTIVATION_REQUIRED)
AC_SUBST(VFS_REQUIRED)
AC_SUBST(BONOBO_REQUIRED)
AC_SUBST(GNOME_REQUIRED)
AC_SUBST(PRINT_REQUIRED)
AC_SUBST(LIBGLADE_REQUIRED)
AC_SUBST(EEL_REQUIRED)

PKG_CHECK_MODULES(GLIMMER,
	gtk+-2.0 >= $GTK_REQUIRED
	libxml-2.0 >= $XML_REQUIRED
	gtksourceview-1.0 >= $SRCVIEW_REQUIRED
	bonobo-activation-2.0 >= $ACTIVATION_REQUIRED
	gnome-vfs-2.0 >= $VFS_REQUIRED
	gnome-vfs-module-2.0 >= $VFS_REQUIRED
	libbonoboui-2.0 >= $BONOBO_REQUIRED
	libgnome-2.0 >= $GNOME_REQUIRED
	libgnomeui-2.0 >= $GNOME_REQUIRED
	libgnomeprintui-2.2 >= $PRINT_REQUIRED
	libglade-2.0 >= $LIBGLADE_REQUIRED
	eel-2.0 >= $EEL_REQUIRED)
AC_SUBST(GLIMMER_CFLAGS)
AC_SUBST(GLIMMER_LIBS)

AC_ISC_POSIX
AC_PROG_CC
AC_HEADER_STDC
AM_PROG_LIBTOOL
AM_GCONF_SOURCE_2

# orbit-idl
ORBIT_IDL="`$PKG_CONFIG --variable=orbit_idl ORBit-2.0`"
AC_SUBST(ORBIT_IDL)

# idl directories
LIBBONOBO_IDL_DIR="`$PKG_CONFIG --variable=idldir libbonobo-2.0`"
AC_SUBST(LIBBONOBO_IDL_DIR)
BONOBO_ACTIVATION_IDL_DIR="`$PKG_CONFIG --variable=idldir bonobo-activation-2.0`"
AC_SUBST(BONOBO_ACTIVATION_IDL_DIR)

# marshal and enum generators
GLIB_GENMARSHAL="`$PKG_CONFIG --variable=glib_genmarshal glib-2.0`"
AC_SUBST(GLIB_GENMARSHAL)
GLIB_MKENUMS="`$PKG_CONFIG --variable=glib_mkenums glib-2.0`"
AC_SUBST(GLIB_MKENUMS)

# i18n support
ALL_LINGUAS="am az ca cs da de el en_CA en_GB es fi fr ga hr hu id ja lv ms nl no pa pt pt_BR ru sk sq sr sr@Latn sv tr uk vi zh_CN zh_TW"
GETTEXT_PACKAGE=glimmer-too
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE")
AC_DEFINE_UNQUOTED(GNOME_EXPLICIT_TRANSLATION_DOMAIN, "$GETTEXT_PACKAGE")
AM_GLIB_GNU_GETTEXT

AC_PATH_PROG(GCONFTOOL, gconftool-2)

CFLAGS="$CFLAGS -Wall -Werror"

AC_OUTPUT([
Makefile
idl/Makefile
libglimmerfile/Makefile
libglimmerfile/dialogs/Makefile
data/Makefile
data/themes/Makefile
tests/Makefile
po/Makefile.in
])
